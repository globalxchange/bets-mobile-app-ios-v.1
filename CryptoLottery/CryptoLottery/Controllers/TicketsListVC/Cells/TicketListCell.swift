//
//  TicketListCell.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-05-06.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class TicketListCell: UITableViewCell {

    @IBOutlet weak var Mainview: UIView!
    
    @IBOutlet weak var CoinTicketView: UIView!
    @IBOutlet weak var TicketView: CustomView!
    @IBOutlet weak var SubTicketView: CustomView!
    @IBOutlet var LinesLBL: [CustomLBL]!
    @IBOutlet weak var linelbl: CustomLBL!
    @IBOutlet weak var TicketIconView: CustomView!
    @IBOutlet weak var TicketIconBTN: CustomBTN!
    
    @IBOutlet weak var PinVIew: UIStackView!
    @IBOutlet var PinValueView: [CustomView]!
    @IBOutlet var PinValueLBL: [UILabel]!
    
    @IBOutlet weak var TicketNoLBL: UILabel!
    
    @IBOutlet weak var StackBTN: UIStackView!
    @IBOutlet weak var EnterBTN: UIButton!
    @IBOutlet weak var SpinBTN: UIButton!
    
    func EnteractionHandler(action:(() -> Void)? = nil) {
        struct __ { static var action :(() -> Void)? }
        if action != nil { __.action = action }
        else { __.action?() }
    }
    
    func SpinactionHandler(action:(() -> Void)? = nil) {
        struct __ { static var action :(() -> Void)? }
        if action != nil { __.action = action }
        else { __.action?() }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.TicketNoLBL.text = "000-000"
        
        self.CoinTicketView.backgroundColor = .clear
        self.TicketView.BGColor = .clear
        self.SubTicketView.BGColor = .clear
        
        self.SubTicketView.BorderColor = AppDarkColor
        self.SubTicketView.BorderWidth = 3
        for n in 0...self.LinesLBL.count - 1 {
            self.LinesLBL[n].LBL_BGColor = self.LinesLBL[n].tag == 100 ? .white : AppDarkColor
            self.LinesLBL[n].LBL_BorderColor = AppDarkColor
        }
        
        for n in 0...self.PinValueView.count - 1 {
            let view: CustomView = self.PinValueView[n]
            view.BGColor = AppDarkColor
            view.BorderColor = AppDarkColor
            view.BorderWidth = 1
            view.isCircle = true
            view.clickBound = true
            view.maskBound = true
            
            let lbl = self.PinValueLBL[n]
            lbl.textColor = .white
            lbl.textAlignment = .center
            lbl.text = ""
        }
        
        self.TicketIconView.BGColor = .clear
        self.TicketIconView.BorderColor = AppDarkColor
        self.TicketIconView.BorderWidth = 3
        
        self.TicketIconBTN.setTitle("0", for: .normal)
        self.TicketIconBTN.theme_setTitleColor(GlobalPicker.textColor, forState: .normal)
        self.TicketIconBTN.backgroundColor = .clear
        self.TicketIconBTN.BTN_BorderColor = AppDarkColor
        self.TicketIconBTN.BTN_BorderWidth = 3
        self.TicketIconBTN.tintColor = AppDarkColor
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func TappedEnterBTN(_ sender: Any) {
        self.EnteractionHandler()
    }
    
    @IBAction func TappedSpinBTN(_ sender: Any) {
        self.SpinactionHandler()
    }
    
}
