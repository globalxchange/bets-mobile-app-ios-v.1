//
//  TicketsListVC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-05-06.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class TicketsListVC: BaseVC {
    
    //    MARK:- IBOutlet
    
    @IBOutlet weak var NavBGView: UIView!
    @IBOutlet weak var NavIMG: UIImageView!
    
    @IBOutlet weak var Headerview: UIView!
    @IBOutlet weak var TitleLBL: UILabel!
    @IBOutlet weak var DetailLBL: UILabel!
    
    @IBOutlet weak var TicketTBL: UITableView!
    
    //    MARK:- Variable Define
    
    var TicketCount: Int = 0
    var SelectedTicket_Dict: NSMutableDictionary!
    var LuckyNo_arry: NSMutableArray!
    var tickets = [String]()
    var LuckyEntery: Int = 0
    
    //    MARK:- View Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.SetupUI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NavBGView.backgroundColor = AppDarkColor
    }
    
    //    MARK:- User Define Methods
    
    func SetupUI() {
        
        self.Headerview.backgroundColor = .white
        self.TitleLBL.theme_textColor = GlobalPicker.textColor
        self.DetailLBL.theme_textColor = GlobalPicker.textColor
        
        self.TitleLBL.text = "Let’s Test Your Luck"
        self.DetailLBL.text = "You Can Simply Enter Your Favourite Six Numbers Or Spin The Cyclone To Randomly Generate The Numbers For Your Ticket"
        
        self.LuckyNo_arry = NSMutableArray.init()
        self.LuckyNo_arry = self.SelectedTicket_Dict.object(forKey: Buy_Tickets_Count) as? NSMutableArray
        self.TicketCount = self.LuckyNo_arry.count
        
        self.TicketTBL.register(UINib.init(nibName: "TicketListCell", bundle: nil), forCellReuseIdentifier: "TicketListCell")
        
        let footer = UIView.init(frame: CGRect(x: 0, y: 0, width: Screen_width, height: 30))
        footer.backgroundColor = .white
        
        self.TicketTBL.tableFooterView = footer
        
        self.TicketTBL.delegate = self
        self.TicketTBL.dataSource = self
        self.TicketTBL.reloadData()
    
    }
    
    func checkLuckyobje() {
        if self.LuckyEntery == self.LuckyNo_arry.count {
            print("All ticket's lucky no are done")
            
            let formatter = NumberFormatter()
            formatter.minimumIntegerDigits = 6
            formatter.maximumIntegerDigits = 6
            formatter.numberStyle = .decimal
            
            for item in self.LuckyNo_arry {
//                if let number = NumberFormatter().number(from: item as! String) {
//                    let ticket = Double(truncating: number)
                self.tickets.append(item as! String)
                    debugPrint(item as Any)
//                }
            }
            print(tickets)
            self.SelectedTicket_Dict.setValue(tickets, forKey: Buy_Tickets_Count)
            DispatchQueue.main.asyncAfter(deadline: .now() + 3 , execute: {
                let loader = TicketPaymentVC.init(nibName: "TicketPaymentVC", bundle: nil)
                loader.SelectedTicket_Dict = self.SelectedTicket_Dict
                self.navigationController?.pushViewController(loader, animated: true)
            })
        }
        else {
            print("Some ticket's lucky no remains \(self.LuckyNo_arry.count - self.LuckyEntery)")
        }
    }
    //    MARK:- Api Calling
    
    //    MARK:- IBAction Methods
    
}

//MARK:- UItableVIew Delegate
extension TicketsListVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.TicketCount
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: TicketListCell = tableView.dequeueReusableCell(withIdentifier: "TicketListCell") as! TicketListCell
        
        cell.TicketIconBTN.setTitle("\(indexPath.row + 1)", for: .normal)
        cell.EnterBTN.tag = indexPath.row
        cell.SpinBTN.tag = indexPath.row
        cell.tag = indexPath.row
        if self.LuckyEntery == indexPath.row {
            cell.Mainview.backgroundColor = AppDarkColor.withAlphaComponent(0.5)
            cell.linelbl.LBL_BGColor = AppDarkColor.withAlphaComponent(0.5)
            cell.StackBTN.isHidden = false
            cell.TicketNoLBL.isHidden = true
            cell.EnteractionHandler {
                let pay = PinNumberVC.init(nibName: "PinNumberVC", bundle: nil)
                pay.modalPresentationStyle = .overCurrentContext
                pay.modalTransitionStyle = .coverVertical
                pay.NumberDismissCallBack = { (pinvalue) in
                    cell.PinVIew.isHidden = false
                    self.SetupPinvalue(pinvalue: pinvalue, cell: cell, indexPath: indexPath, ismulti: false)
                }
                self.present(pay, animated: true, completion: nil)
            }
            cell.SpinactionHandler {
                let spin = SpinLuckVC.init(nibName: "SpinLuckVC", bundle: nil)
                spin.modalPresentationStyle = .overCurrentContext
                spin.modalTransitionStyle = .coverVertical
                spin.NumberDismissCallBack = { (pinvalue) in
                    cell.PinVIew.isHidden = false
                    if pinvalue.count == 0 {
                        for i in self.LuckyEntery...self.TicketCount - 1 {
                            self.SetupPinvalue(pinvalue: self.random(digits: 5), cell: cell, indexPath: IndexPath(row: i, section: 0), ismulti: true)
                        }
                    }
                    else {
                        self.SetupPinvalue(pinvalue: pinvalue, cell: cell, indexPath: indexPath, ismulti: false)
                    }
                    
                }
                self.present(spin, animated: true, completion: nil)
            }
        }
        else {
            cell.Mainview.backgroundColor = .white
            cell.StackBTN.isHidden = true
            cell.TicketNoLBL.isHidden = false
            cell.linelbl.LBL_BGColor = .white
            let value:String = self.LuckyNo_arry.object(at: indexPath.row) as! String
            if value.isZero {
                cell.TicketNoLBL.isHidden = false
                cell.PinVIew.isHidden = true
            }
            else {
                cell.TicketNoLBL.isHidden = true
                cell.PinVIew.isHidden = false
                let pinset = value.inserting(separator: "-", every: 1)
                let pinarry = pinset.components(separatedBy: "-")
                for n in 0...cell.PinValueView.count - 1 {
                    let lbl = cell.PinValueLBL[n]
                    lbl.text = pinarry[n]
                }
            }
            
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func SetupPinvalue(pinvalue: String, cell: TicketListCell, indexPath: IndexPath, ismulti: Bool) {
        if ismulti {
            self.LuckyNo_arry.replaceObject(at: indexPath.row, with: pinvalue)
            self.LuckyEntery += 1
            if self.LuckyEntery == self.LuckyNo_arry.count {
                self.TicketTBL.reloadData {
                    self.checkLuckyobje()
                }
            }
        }
        else {
            let pinset = pinvalue.inserting(separator: "-", every: 1)
            let pinarry = pinset.components(separatedBy: "-")
            for n in 0...cell.PinValueView.count - 1 {
                let lbl = cell.PinValueLBL[n]
                lbl.text = pinarry[n]
            }
            
            self.LuckyNo_arry.replaceObject(at: indexPath.row, with: pinvalue)
            self.LuckyEntery += 1
            self.TicketTBL.reloadData {
                self.checkLuckyobje()
            }
        }
    }
    
    func random(digits:Int) -> String {
        var number = String()
        for _ in 0...digits {
           number += "\(Int.random(in: 0...9))"
        }
        return number
    }
    
}
