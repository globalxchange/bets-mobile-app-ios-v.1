//
//  ValutCurrencyCell.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-05-02.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import SkeletonView

class ValutCurrencyCell: UICollectionViewCell {

    @IBOutlet weak var ValutIMG: UIImageView!
    @IBOutlet weak var ValutTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ValutIMG.tintColor = AppDarkColor
        ValutTitle.theme_textColor = GlobalPicker.textColor
        
        ValutIMG.showGradientSkeleton()
        ValutTitle.showGradientSkeleton()
        
    }

}
