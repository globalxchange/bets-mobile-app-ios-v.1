//
//  IncomingCallVC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-07-07.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class IncomingCallVC: UIViewController {

    @IBOutlet weak var TitleLBL: UILabel!
    @IBOutlet weak var ProfilePic: UIImageView!
    
    @IBOutlet var IconView: [UIView]!
    @IBOutlet weak var MainStack: UIStackView!
    @IBOutlet weak var Stack1: UIStackView!
    @IBOutlet weak var AudioView: UIView!
    @IBOutlet weak var AudioIMG: UIImageView!
    @IBOutlet weak var AudioLBL: UILabel!
    @IBOutlet weak var AudioBTN: UIButton!
    
    @IBOutlet weak var MicroView: UIView!
    @IBOutlet weak var MicroIMG: UIImageView!
    @IBOutlet weak var MicroLBL: UILabel!
    @IBOutlet weak var MicroBTN: UIButton!
    
    @IBOutlet weak var VideoView: UIView!
    @IBOutlet weak var VideoIMG: UIImageView!
    @IBOutlet weak var VideoLBL: UILabel!
    @IBOutlet weak var VideoBTN: UIButton!
    
    @IBOutlet weak var Stack2: UIStackView!
    @IBOutlet weak var MessageView: UIView!
    @IBOutlet weak var MessageIMG: UIImageView!
    @IBOutlet weak var MessageLBL: UILabel!
    @IBOutlet weak var MessageBTN: UIButton!
    
    @IBOutlet weak var ContactView: UIView!
    @IBOutlet weak var ContactIMG: UIImageView!
    @IBOutlet weak var ContactLBL: UILabel!
    @IBOutlet weak var ContactBTN: UIButton!
    
    @IBOutlet weak var VoiceView: UIView!
    @IBOutlet weak var VoiceIMG: UIImageView!
    @IBOutlet weak var VoiceLBL: UILabel!
    @IBOutlet weak var VoiceBTN: UIButton!
    
    @IBOutlet weak var Stack3: UIStackView!
    @IBOutlet weak var RecieveView: UIView!
    @IBOutlet weak var RecieveSub: UIView!
    @IBOutlet weak var RecieveIMG: UIImageView!
    @IBOutlet weak var RecieveBTN: UIButton!
    
    @IBOutlet weak var DummyView: UIView!
    
    @IBOutlet weak var HangupView: UIView!
    @IBOutlet weak var HangupSub: UIView!
    @IBOutlet weak var HangupIMG: UIImageView!
    @IBOutlet weak var HangupBTN: UIButton!
    
    // MARK:- Variable Define
    
    var userName: String = ""
    
    // MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    // MARK:- User Define Methods
    func setupUI() {
        self.view.backgroundColor = AppDarkColor
        
        for view in self.IconView {
            view.backgroundColor = .clear
            view.clipsToBounds = true
            view.layer.cornerRadius = view.frame.height / 2
        }
        
        let string1: String = "Anna williams"
        let string2: String = "\nCalling..." as String
        let mainstring: String = string1 + string2
        let myMutableString = mainstring.Attributestring(attribute: [(string1, Font().MontserratRegularFont(font: 32.0), UIColor.white), (string2, Font().MontserratRegularFont(font: 15.0), UIColor.white)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [string1, string2])))
        self.TitleLBL.attributedText = myMutableString
        
        self.ProfilePic.image = UIImage.init(named: "Oval")
        self.ProfilePic.clipsToBounds = true
        self.ProfilePic.layer.cornerRadius = self.ProfilePic.frame.height / 2
        
        self.AudioLBL.font = Font().MontserratLightFont(font: 13)
        self.AudioLBL.textColor = .white
        self.AudioLBL.text = "Audio"
        
        self.MicroLBL.font = Font().MontserratLightFont(font: 13)
        self.MicroLBL.textColor = .white
        self.MicroLBL.text = "Microphone"
        
        self.VideoLBL.font = Font().MontserratLightFont(font: 13)
        self.VideoLBL.textColor = .white
        self.VideoLBL.text = "Video"
        
        self.MessageLBL.font = Font().MontserratLightFont(font: 13)
        self.MessageLBL.textColor = .white
        self.MessageLBL.text = "Message"
        
        self.ContactLBL.font = Font().MontserratLightFont(font: 13)
        self.ContactLBL.textColor = .white
        self.ContactLBL.text = "Add Contact"
        
        self.VoiceLBL.font = Font().MontserratLightFont(font: 13)
        self.VoiceLBL.textColor = .white
        self.VoiceLBL.text = "Voice Mail"
        
        self.AudioIMG.tintColor = .white
        self.AudioIMG.image = UIImage.init(named: "IconAudio")
        
        self.MicroIMG.tintColor = .white
        self.MicroIMG.image = UIImage.init(named: "IconMicro")
        
        self.VideoIMG.tintColor = .white
        self.VideoIMG.image = UIImage.init(named: "IconVideo")
        
        self.MessageIMG.tintColor = .white
        self.MessageIMG.image = UIImage.init(named: "IconMessage")
        
        self.ContactIMG.tintColor = .white
        self.ContactIMG.image = UIImage.init(named: "IconContact")
        
        self.VoiceIMG.tintColor = .white
        self.VoiceIMG.image = UIImage.init(named: "IconVoice")
        
        self.RecieveIMG.tintColor = .white
        self.RecieveIMG.image = UIImage.init(named: "IconReceive")
        
        self.HangupIMG.tintColor = .white
        self.HangupIMG.image = UIImage.init(named: "IconHangUp")
        
        self.RecieveSub.backgroundColor = .systemGreen
        self.RecieveSub.clipsToBounds = true
        self.RecieveSub.layer.cornerRadius = self.RecieveSub.frame.height / 2
        
        self.HangupSub.backgroundColor = .systemRed
        self.HangupSub.clipsToBounds = true
        self.HangupSub.layer.cornerRadius = self.HangupSub.frame.height / 2
        
        self.Stack1.isHidden = true
//        self.AudioView.isHidden = true
//        self.MicroView.isHidden = true
//        self.VideoView.isHidden = true
        self.Stack2.isHidden = true
//        self.MessageView.isHidden = true
//        self.ContactView.isHidden = true
//        self.VoiceView.isHidden = true
        self.Stack3.isHidden = false
        self.RecieveView.isHidden = false
        self.HangupView.isHidden = false
    }

    // MARK:- IBAction Methods
    @IBAction func TappedButtons(_ sender: UIButton) {
        switch sender {
        case self.AudioBTN:
            break
            
        case self.MicroBTN:
            break
            
        case self.VideoBTN:
            break
            
        case self.MessageBTN:
            break
            
        case self.ContactBTN:
            break
            
        case self.VoiceBTN:
            break
            
        case self.RecieveBTN:
            self.Stack1.isHidden = false
            self.AudioView.isHidden = false
            self.MicroView.isHidden = false
            self.VideoView.isHidden = false
            self.Stack2.isHidden = false
            self.MessageView.isHidden = false
            self.ContactView.isHidden = false
            self.VoiceView.isHidden = false
            self.Stack3.isHidden = false
            self.RecieveView.isHidden = true
            self.DummyView.isHidden = true
            self.HangupView.isHidden = false
            break
            
        case self.HangupBTN:
            self.navigationController?.popViewController(animated: true)
            break
            
        default:
            self.navigationController?.popViewController(animated: true)
            break
        }
    }
    
}
