//
//  SupportOptionCell.swift
//  CryptoLottery
//
//  Created by Hiren on 14/07/20.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class SupportOptionCell: UITableViewCell {

    @IBOutlet weak var OptionBTN1: UIButton!
    @IBOutlet weak var OptionBTN2: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.OptionBTN1.backgroundColor = .white
        self.OptionBTN1.setTitleColor(.lightGray, for: .normal)
        self.OptionBTN1.setTitle("Upload Screenshort", for: .normal)
        self.OptionBTN1.titleLabel?.font = Font().MontserratRegularFont(font: 17)
        
        self.OptionBTN2.backgroundColor = .white
        self.OptionBTN2.setTitleColor(.lightGray, for: .normal)
        self.OptionBTN2.setTitle("Contact Preference", for: .normal)
        self.OptionBTN2.titleLabel?.font = Font().MontserratRegularFont(font: 17)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK:- IBAction Methods
    
    @IBAction func TappedOptionBTN1(sender: UIButton!) {
        
    }
    
    @IBAction func TappedOptionBTN2(sender: UIButton!) {
        
    }
}
