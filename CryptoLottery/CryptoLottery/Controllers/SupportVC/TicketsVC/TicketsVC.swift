//
//  TicketsVC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-07-06.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class TicketsVC: UIViewController {

    // MARK:- IBOutlet Defines
    
    @IBOutlet weak var BotomView: UIView!
    @IBOutlet weak var Bottom_Height: NSLayoutConstraint!
    @IBOutlet weak var BottomTitle: UILabel!
    @IBOutlet weak var Tocket_TBL: UITableView!
    @IBOutlet weak var OptionStack: UIStackView!
    @IBOutlet weak var DoneBTN: UIButton!
    @IBOutlet weak var TypeMoreBTN: UIButton!
    @IBOutlet var Corners: [UILabel]!
    
    // MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .OnHoldView, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    // MARK:- User Define Methods
    @objc func onDidReceiveData(_ notification:Notification) {
        self.setupUI()
    }

    func setupUI() {
        
        for lbl in self.Corners {
            lbl.backgroundColor = .white
        }
        self.BotomView.layer.cornerRadius = 10
        self.BotomView.backgroundColor = .white
        
        self.Bottom_Height.constant = Screen_height / 2 > ((100 * 3) + 120) ? Screen_height / 2 : ((100 * 3) + 120)
        
        self.view.backgroundColor = AppDarkColor
        self.BottomTitle.text = "Submit Ticket"
        self.BottomTitle.textColor = AppDarkColor
        self.BottomTitle.font = Font().MontserratBoldFont(font: 20)
        
        self.Tocket_TBL.register(UINib.init(nibName: "SupportTicketOptionCell", bundle: nil), forCellReuseIdentifier: "SupportTicketOptionCell")
        self.Tocket_TBL.register(UINib.init(nibName: "SupportOptionCell", bundle: nil), forCellReuseIdentifier: "SupportOptionCell")
        self.Tocket_TBL.backgroundColor = .clear
        let footer = UIView.init(frame: CGRect(x: 0, y: 0, width: Screen_width, height: 50))
        footer.backgroundColor = UIColor.clear
        self.Tocket_TBL.tableFooterView = footer
        self.Tocket_TBL.delegate = self
        self.Tocket_TBL.dataSource = self
        self.Tocket_TBL.reloadData()
        
        self.DoneBTN.backgroundColor = AppDarkColor
        self.DoneBTN.setTitle("I'm Done", for: .normal)
        self.DoneBTN.setTitleColor(.white, for: .normal)
        self.DoneBTN.titleLabel?.font = Font().MontserratRegularFont(font: 17)
        self.DoneBTN.layer.cornerRadius = 10
        
        self.TypeMoreBTN.backgroundColor = AppDarkColor
        self.TypeMoreBTN.setTitle("Type More", for: .normal)
        self.TypeMoreBTN.setTitleColor(.white, for: .normal)
        self.TypeMoreBTN.titleLabel?.font = Font().MontserratRegularFont(font: 17)
        self.TypeMoreBTN.layer.cornerRadius = 10
    }
    
    // MARK:- IBAction Methods
    
    @IBAction func TappedDone(sender: UIButton!) {
        
    }
    
    @IBAction func TappedTypeMore(sender: UIButton!) {
        
    }

}

//MARK:- Tableview Delegate and datasource
extension TicketsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: Screen_width, height: 30))
        view.backgroundColor = .white
        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 2 {
            let cell: SupportOptionCell = tableView.dequeueReusableCell(withIdentifier: "SupportOptionCell") as! SupportOptionCell
            
            return cell
        }
        else {
            let cell: SupportTicketOptionCell = tableView.dequeueReusableCell(withIdentifier: "SupportTicketOptionCell") as! SupportTicketOptionCell
            if indexPath.section == 0 {
                cell.Titlelbl.text = "Type Of Issue"
                cell.Noteslbl.text = "Select Issue Type"
                cell.Images.image = UIImage.init(named: "SupportUser")
            }
            else {
                cell.Titlelbl.text = "Sub Issue Type"
                cell.Noteslbl.text = "Select Issue Type"
                cell.Images.image = UIImage.init(named: "SupportMail")
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
