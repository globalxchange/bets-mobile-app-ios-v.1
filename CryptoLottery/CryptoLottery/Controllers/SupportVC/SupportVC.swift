//
//  SupportVC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-07-04.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class SupportVC: BaseVC {

    // MARK:- IBOutlets Defines
    @IBOutlet weak var NavView: UIView!
    @IBOutlet weak var NavIMG: UIImageView!
    @IBOutlet weak var BackBTN: UIButton!
    
    @IBOutlet weak var HeaderTabStack: UIStackView!
    @IBOutlet var OptionView: [UIView]!
    @IBOutlet var SubOptionView: [CustomView]!
    @IBOutlet var OptionIMG: [UIImageView]!
    @IBOutlet var OptionTitleLBL: [UILabel]!
    @IBOutlet var OptionSectionLBL: [UILabel]!
    @IBOutlet var OptionBTN: [UIButton]!
    
    // MARK:- Variables Define

    var SelectedOption: Int = 0
    
    // MARK:- View Life Cycles
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.SetupUI()
    }

    // MARK:- User Define Methods
    
    func SetupUI() {
        for view in self.OptionView {
            view.backgroundColor = .white
        }
        self.SetupSelectedOption()
        
        SocketConnect.shared.Connect()
        SocketConnect.shared.socket.on(clientEvent: .error) { (data, ack) in
            print(data)
        }
        SocketConnect.shared.socket.onAny {print("Got event: \($0.event), with items: \($0.items!)")}
        SocketConnect.shared.socket.on(clientEvent: .connect) { (data, ack) in
            print("connected")
        }
    }
    
    func SetupSelectedOption() {
        for img in self.OptionIMG {
            if img.tag == self.SelectedOption {
                img.tintColor = AppDarkColor
                img.alpha = 1.0
            }
            else {
                img.tintColor = AppDarkColor
                img.alpha = 0.5
            }
        }
        for item in self.OptionTitleLBL {
            if item.tag == self.SelectedOption {
                item.textColor = AppDarkColor
                item.alpha = 1.0
                item.font = Font().MontserratBoldFont(font: 12)
            }
            else {
                item.textColor = AppDarkColor
                item.alpha = 0.5
                item.font = Font().MontserratLightFont(font: 12)
            }
            item.text = item.tag == 0 ? "OnHold" : item.tag == 1 ? "Chats" : item.tag == 2 ? "Ticket" : "FAQ"
        }
        for select in self.OptionSectionLBL {
            if select.tag == self.SelectedOption {
                select.textColor = AppDarkColor
            }
            else {
                select.textColor = .clear
            }
        }
        for view in self.SubOptionView {
            view.BGColor = .white
            view.ShadowColor = .black
            if view.tag == self.SelectedOption {
                view.ShadowOpacity = 0.5
                view.ShadowRadius = 1
                view.ShadowOffset = CGSize(width: 0, height: 0.75)
            }
            else {
                view.ShadowOpacity = 0.5
                view.ShadowRadius = 0
                view.ShadowOffset = CGSize(width: 0, height: 0)
            }
        }
        NotificationCenter.default.post(name: self.SelectedOption == 0 ? .OnHoldView : self.SelectedOption == 1 ? .ChatsView : self.SelectedOption == 2 ? .TicketView : .FAQView, object: nil)
        self.containerController.selectController(atIndex: self.SelectedOption, animated: true)
    }
    
    func setupContainerController(_ controller: SwiftyPageController) {
        // assign variable
        self.containerController = controller
        //        containerController.isEnabledInteractive = false
        
        // set delegate
        self.containerController.delegate = self
        
        // set animation type
        self.containerController.animator = .default
        
        // set view controllers
        let v1 = OnHoldVC.init(nibName: "OnHoldVC", bundle: nil)
        let v2 = ChatsVC.init(nibName: "ChatsVC", bundle: nil)
        let v3 = TicketsVC.init(nibName: "TicketsVC", bundle: nil)
        let v4 = FAQVC.init(nibName: "FAQVC", bundle: nil)
        
        self.containerController.viewControllers = [v1, v2, v3, v4]
        
        // select needed controller
        self.SetupSelectedOption()
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let containerController = segue.destination as? SwiftyPageController {
            setupContainerController(containerController)
        }
    }
    
    // MARK:- API Calling
    
    // MARK:- IBAction Methods
    
    @IBAction func TappedBackBTN(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func TappedOptionBTN(_ sender: UIButton) {
        if sender.tag == 0 {
            self.SelectedOption = 0
        }
        else if sender.tag == 1 {
            self.SelectedOption = 1
        }
        else if sender.tag == 2 {
            self.SelectedOption = 2
        }
        else {
            self.SelectedOption = 3
        }
        self.SetupSelectedOption()
    }
    
}

// MARK: - PagesViewControllerDelegate

extension SupportVC: SwiftyPageControllerDelegate {
    
    func swiftyPageController(_ controller: SwiftyPageController, alongSideTransitionToController toController: UIViewController) {
        
    }
    
    func swiftyPageController(_ controller: SwiftyPageController, didMoveToController toController: UIViewController) {
        
    }
    
    func swiftyPageController(_ controller: SwiftyPageController, willMoveToController toController: UIViewController) {
        
    }
    
}
