//
//  FAQVC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-07-06.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class FAQVC: UIViewController {

    @IBOutlet weak var SearchBar: UISearchBar!
    @IBOutlet weak var CategoryList: TPKeyboardAvoidingCollectionView!
    
    // MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .OnHoldView, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    // MARK:- User Define Methods
    @objc func onDidReceiveData(_ notification:Notification) {
        self.setupUI()
    }

    func setupUI() {
        self.view.backgroundColor = AppDarkColor
        
        self.SearchBar.placeholder = "Search Category"
        
        self.CategoryList.register(UINib.init(nibName: "CategoryCell", bundle: nil), forCellWithReuseIdentifier: "CategoryCell")
        self.CategoryList.translatesAutoresizingMaskIntoConstraints = false
        self.CategoryList.delegate = self
        self.CategoryList.dataSource = self
        self.CategoryList.reloadData()
    }

}

extension FAQVC: UICollectionViewDelegate,UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell",for: indexPath) as? CategoryCell else { fatalError() }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: Screen_width / 2.3, height: 120)
    }
    
}
