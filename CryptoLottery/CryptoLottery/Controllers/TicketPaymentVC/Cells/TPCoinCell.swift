//
//  TPCoinCell.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-05-21.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class TPCoinCell: UITableViewCell {

    @IBOutlet weak var CoinSelectionView: UIView!
    @IBOutlet weak var CoinSTitle: UILabel!
    @IBOutlet weak var IMGCoin: UIImageView!
    @IBOutlet weak var CoinName: UILabel!
    @IBOutlet weak var Seprator: UILabel!
    @IBOutlet weak var CoinDetail: UILabel!
    
    @IBOutlet weak var CoinConverterView: UIView!
    @IBOutlet weak var SeletedCoinBTN: UIButton!
    @IBOutlet weak var LineSeprator: UILabel!
    @IBOutlet weak var UnseletedCoinBTN: UIButton!
    
    var didConverterActionBlock: ((_ index: Int)->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.theme_backgroundColor = GlobalPicker.backgroundColor
        self.CoinSelectionView.theme_backgroundColor = GlobalPicker.backgroundColor
        
        self.IMGCoin.backgroundColor = AppDarkColor
        self.IMGCoin.clipsToBounds = true
        self.IMGCoin.layer.cornerRadius = self.IMGCoin.frame.height / 2
        self.IMGCoin.tintColor = UIColor.white
        
        self.CoinSTitle.theme_textColor = GlobalPicker.notesColor
        self.CoinName.theme_textColor = GlobalPicker.textColor
        self.CoinDetail.theme_textColor = GlobalPicker.notesColor
        
        self.UnseletedCoinBTN.setTitleColor(UIColor.lightGray, for: .normal)
        self.LineSeprator.backgroundColor = AppDarkColor
        self.SeletedCoinBTN.setTitleColor(AppDarkColor, for: .normal)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func TappedSeletedCoinBTN(_ sender: UIButton) {
        if self.didConverterActionBlock != nil {
            if sender == self.SeletedCoinBTN {
                self.didConverterActionBlock!(0)
            }
            else {
                self.didConverterActionBlock!(1)
            }
        }
//        if self.CoinData.coin.uppercased() != "USD" {
//            let obj = self.Currenydata[self.getcoinIndex(coin: self.CoinName.text!)]
//            if sender == self.SeletedCoinBTN {
//                if sender.isSelected {
//                    self.isUSDFormat = false
//                    self.SeletedCoinBTN.isSelected = self.isUSDFormat
//                    self.SeletedCoinBTN.setTitle("USD", for: .normal)
//                    self.SeletedCoinBTN.setTitleColor(AppDarkColor, for: .normal)
//
//                    self.PriceLBL.text = String.init(format: "%f", self.activeLotteries.ticketPrice!).CoinPriceThumbRules(Coin: obj.coin)
//                    self.CoinDetail.text = String.init(format: "%f", obj.nativeField).CoinPriceThumbRules(Coin: obj.coin)
//                    if let n = NumberFormatter().number(from: self.TXTValue1.text! as String) {
//                        let f = Double(truncating: n)
//                        self.TXTValue2.text = String.init(format: "%f", (self.activeLotteries.ticketPrice! * f)).CoinPriceThumbRules(Coin: obj.coin)
//                    }
//                }
//                else {
//                    self.isUSDFormat = true
//                    self.SeletedCoinBTN.isSelected = self.isUSDFormat
//                    self.SeletedCoinBTN.setTitle(self.CoinData.coin.uppercased(), for: .normal)
//                    self.SeletedCoinBTN.setTitleColor(AppDarkColor, for: .selected)
//                    CommanApiCall().ForexConversion(Buy: obj.coin, From: "USD") { (keyvalue1, keyvalue2) in
//                        let price = self.activeLotteries.ticketPrice! * keyvalue1
//                        self.PriceLBL.text = String.init(format: "%f", price).ConvertIntoUSDFormat()
//                        self.CoinDetail.text = String.init(format: "%f", (obj.nativeField * keyvalue1)).ConvertIntoUSDFormat()
//                        if let n = NumberFormatter().number(from: self.TXTValue1.text! as String) {
//                            let f = Double(truncating: n)
//                            self.TXTValue2.text = String.init(format: "%f", (price * f)).ConvertIntoUSDFormat()
//                        }
//                    }
//                }
//            }
//            else {
//                self.isUSDFormat = false
//                self.SeletedCoinBTN.isSelected = self.isUSDFormat
//                self.SeletedCoinBTN.setTitle("USD", for: .normal)
//                self.UnseletedCoinBTN.setTitle(self.CoinData.coin.uppercased(), for: .normal)
//                self.PriceLBL.text = String.init(format: "%f", self.activeLotteries.ticketPrice!).CoinPriceThumbRules(Coin: obj.coin)
//                self.CoinDetail.text = String.init(format: "%f", obj.nativeField).CoinPriceThumbRules(Coin: obj.coin)
//                if let n = NumberFormatter().number(from: self.TXTValue1.text! as String) {
//                    let f = Double(truncating: n)
//                    self.TXTValue2.text = String.init(format: "%f", (self.activeLotteries.ticketPrice! * f)).CoinPriceThumbRules(Coin: obj.coin)
//                }
//            }
//        }
    }
    
}
