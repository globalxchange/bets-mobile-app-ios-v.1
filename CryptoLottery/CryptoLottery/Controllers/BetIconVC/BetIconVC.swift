//
//  BetIconVC.swift
//  ClassRoom
//
//  Created by Shorupan Pirakaspathy on 2020-04-09.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class BetIconVC: BaseVC {
    
    //    MARK:- IBOutlet
    
    @IBOutlet weak var NavBGView: UIView!
    @IBOutlet weak var NavIMG: UIImageView!
    
    @IBOutlet weak var BetStack: UIStackView!
    
    //    MARK:- Variable Define
    
    //    MARK:- View Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.NavBGView.backgroundColor = AppDarkColor
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //    MARK:- User Define Methods
    
    //    MARK:- Api Calling
    
    //    MARK:- IBAction Methods
    
}
