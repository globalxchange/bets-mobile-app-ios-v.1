//
//  StreamingVC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-06-06.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import AVFoundation
import CoreLocation

class StreamingVC: UIViewController {
    
    // MARK: - Constants
    
    let cameraManager = CameraManager()
    
    // MARK: - @IBOutlets
    
    @IBOutlet var headerView: UIView!
    @IBOutlet var flashModeImageView: UIButton!
    @IBOutlet var outputImageView: UIButton!
    @IBOutlet var cameraTypeImageView: UIButton!
    @IBOutlet var qualityLabel: UIButton!
    
    @IBOutlet var cameraView: UIView!
    @IBOutlet var askForPermissionsLabel: UILabel!
    
    @IBOutlet var footerView: UIView!
    @IBOutlet var cameraButton: UIButton!
    @IBOutlet weak var CMback: CustomLBL!
    @IBOutlet var locationButton: UIButton!
    @IBOutlet var captView: UIImageView!
    
    let redColor = UIColor(red: 229 / 255, green: 77 / 255, blue: 67 / 255, alpha: 1)
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.captView.clipsToBounds = true
        self.captView.layer.cornerRadius = 10
        
        self.cameraButton.clipsToBounds = true
        self.cameraButton.layer.cornerRadius = self.cameraButton.frame.height / 2
        self.cameraButton.layer.borderWidth = 5.0
        self.cameraButton.layer.borderColor = UIColor.white.cgColor
        self.cameraButton.backgroundColor = .clear
        
        setupCameraManager()
        
        navigationController?.navigationBar.isHidden = true
        
        askForPermissionsLabel.isHidden = true
        askForPermissionsLabel.backgroundColor = AppDarkColor.withAlphaComponent(0.5)
        askForPermissionsLabel.textColor = .white
        askForPermissionsLabel.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(askForCameraPermissions))
        askForPermissionsLabel.addGestureRecognizer(tapGesture)
        
        footerView.backgroundColor = AppDarkColor
        headerView.backgroundColor = AppDarkColor
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                case .authorizedAlways, .authorizedWhenInUse:
                    cameraManager.shouldUseLocationServices = true
                    locationButton.isHidden = true
                default:
                    cameraManager.shouldUseLocationServices = false
            }
        }
        
        let currentCameraState = cameraManager.currentCameraStatus()
        
        if currentCameraState == .notDetermined {
            askForPermissionsLabel.isHidden = false
        } else if currentCameraState == .ready {
            addCameraToView()
        } else {
            askForPermissionsLabel.isHidden = false
        }
        
        flashModeImageView.setImage(UIImage(named: "flash_off"), for: .normal)
        if !cameraManager.hasFlash {
            flashModeImageView.isEnabled = false
            flashModeImageView.isUserInteractionEnabled = false
        }
        
        outputImageView.setImage(UIImage(named: "output_video"), for: .normal)
        
        cameraTypeImageView.setImage(UIImage(named: "switch_camera"), for: .normal)
        
        qualityLabel.isUserInteractionEnabled = true
        let qualityGesture = UITapGestureRecognizer(target: self, action: #selector(changeCameraQuality))
        qualityLabel.addGestureRecognizer(qualityGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = true
        cameraManager.resumeCaptureSession()
        cameraManager.startQRCodeDetection { result in
            switch result {
                case .success(let value):
                    print(value)
                case .failure(let error):
                    print(error.localizedDescription)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cameraManager.stopQRCodeDetection()
        cameraManager.stopCaptureSession()
    }
    
    // MARK: - ViewController
    fileprivate func setupCameraManager() {
        cameraManager.shouldEnableExposure = true
        cameraManager.shouldEnableTapToFocus = true
        cameraManager.shouldEnablePinchToZoom = true
        cameraManager.shouldEnableExposure = true
        cameraManager.writeFilesToPhoneLibrary = true
        cameraManager.shouldFlipFrontCameraImage = true
        cameraManager.showAccessPermissionPopupAutomatically = true
    }
    
    
    fileprivate func addCameraToView() {
        cameraManager.addPreviewLayerToView(cameraView, newCameraOutputMode: CameraOutputMode.videoWithMic)
        cameraManager.showErrorBlock = { [weak self] (erTitle: String, erMessage: String) -> Void in
            
            let alertController = UIAlertController(title: erTitle, message: erMessage, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) -> Void in }))
            
            self?.present(alertController, animated: true, completion: nil)
        }
    }
    
    // MARK: - @IBActions
    
    @IBAction func BAckBTN(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func changeFlashMode(_ sender: UIButton) {
        switch cameraManager.changeFlashMode() {
            case .off:
                flashModeImageView.setImage(UIImage(named: "flash_off"), for: .normal)
            case .on:
                flashModeImageView.setImage(UIImage(named: "flash_on"), for: .normal)
            case .auto:
                flashModeImageView.setImage(UIImage(named: "flash_auto"), for: .normal)
        }
    }
    
    @IBAction func outputModeButtonTapped(_ sender: UIButton) {
        cameraManager.cameraOutputMode = cameraManager.cameraOutputMode == CameraOutputMode.videoWithMic ? CameraOutputMode.stillImage : CameraOutputMode.videoWithMic
        switch cameraManager.cameraOutputMode {
            case .stillImage:
                cameraButton.isSelected = false
                self.cameraButton.backgroundColor = .clear
                cameraButton.layer.borderColor = UIColor.white.cgColor
                CMback.backgroundColor = UIColor.white
                CMback.LBL_BGColor = UIColor.white
                outputImageView.setImage(UIImage(named: "output_image"), for: .normal)
            case .videoWithMic, .videoOnly:
                self.cameraButton.backgroundColor = .clear
                cameraButton.layer.borderColor = UIColor.white.cgColor
                CMback.backgroundColor = redColor
                CMback.LBL_BGColor = redColor
                outputImageView.setImage(UIImage(named: "output_video"), for: .normal)
        }
    }
    
    @IBAction func changeCameraDevice() {
        cameraManager.cameraDevice = cameraManager.cameraDevice == CameraDevice.front ? CameraDevice.back : CameraDevice.front
    }
    
    @IBAction func changeCameraQuality() {
        switch cameraManager.cameraOutputQuality {
            case .high:
                qualityLabel.setTitle("Medium", for: .normal)
                cameraManager.cameraOutputQuality = .medium
            break
            
            case .medium:
                qualityLabel.setTitle("Low", for: .normal)
                cameraManager.cameraOutputQuality = .low
            break
            
            case .low:
                qualityLabel.setTitle("High", for: .normal)
                cameraManager.cameraOutputQuality = .high
            break
            
            default:
                qualityLabel.setTitle("High", for: .normal)
                cameraManager.cameraOutputQuality = .high
            break
            
        }
    }
    
    @IBAction func askForCameraPermissions() {
        cameraManager.askUserForCameraPermission { permissionGranted in
            
            if permissionGranted {
                self.askForPermissionsLabel.isHidden = true
                self.askForPermissionsLabel.alpha = 0
                self.addCameraToView()
            } else {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                } else {
                    // Fallback on earlier versions
                }
            }
        }
    }
    
    @IBAction func recordButtonTapped(_ sender: UIButton) {
        switch cameraManager.cameraOutputMode {
            case .stillImage:
                cameraManager.capturePictureWithCompletion { result in
                    switch result {
                        case .failure:
                            self.cameraManager.showErrorBlock("Error occurred", "Cannot save picture.")
                        case .success(let content):
                            
                            guard let validImage = UIImage(data: content.asData!) else {
                                return
                            }
                            self.captView.image = validImage
                            if self.cameraManager.cameraDevice == .front {
                                switch validImage.imageOrientation {
                                case .up, .down:
                                    self.captView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                                default:
                                    break
                                }
                            }
                    }
                }
            case .videoWithMic, .videoOnly:
                cameraButton.isSelected = !cameraButton.isSelected
                cameraButton.setTitle("", for: UIControl.State.selected)
                
                cameraButton.layer.borderColor = UIColor.white.cgColor
                self.cameraButton.backgroundColor = .clear
                CMback.backgroundColor = redColor
                CMback.LBL_BGColor = redColor
                if sender.isSelected {
                    cameraButton.layer.borderColor = UIColor.clear.cgColor
                    self.cameraButton.backgroundColor = .clear
                    CMback.backgroundColor = redColor
                    CMback.LBL_BGColor = redColor
                    cameraManager.startRecordingVideo()
                    cameraManager.stopVideoRecording({ (videoURL, recordError) -> Void in
                        guard let videoURL = videoURL else {
                            //Handle error of no recorded video URL
                            return
                        }
                        do {
                            try FileManager.default.copyItem(at: videoURL, to: URL.init(string: "")!)
                        }
                        catch {
                            //Handle error occured during copy
                        }
                    })
                } else {
                    cameraButton.layer.borderColor = UIColor.white.cgColor
                    self.cameraButton.backgroundColor = .clear
                    CMback.backgroundColor = redColor
                    CMback.LBL_BGColor = redColor
                    cameraManager.stopVideoRecording { (_, error) -> Void in
                        if error != nil {
                            self.cameraManager.showErrorBlock("Error occurred", "Cannot save video.")
                        }
                    }
                }
        }
    }
    
    @IBAction func locateMeButtonTapped(_ sender: Any) {
        cameraManager.shouldUseLocationServices = true
        locationButton.isHidden = true
    }
    
}

public extension Data {
    func printExifData() {
        let cfdata: CFData = self as CFData
        let imageSourceRef = CGImageSourceCreateWithData(cfdata, nil)
        let imageProperties = CGImageSourceCopyMetadataAtIndex(imageSourceRef!, 0, nil)!
        
        let mutableMetadata = CGImageMetadataCreateMutableCopy(imageProperties)!
        
        CGImageMetadataEnumerateTagsUsingBlock(mutableMetadata, nil, nil) { _, tag in
            print(CGImageMetadataTagCopyName(tag)!, ":", CGImageMetadataTagCopyValue(tag)!)
            return true
        }
    }
}

