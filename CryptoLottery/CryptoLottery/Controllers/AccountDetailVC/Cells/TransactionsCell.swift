//
//  TransactionsCell.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-04-23.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class TransactionsCell: UITableViewCell {

    @IBOutlet weak var view: CustomView!
    @IBOutlet weak var TitleCenter: NSLayoutConstraint!
    @IBOutlet weak var TitleLBL: UILabel!
    @IBOutlet weak var LogoIMG: UIImageView!
    @IBOutlet weak var PriceLBL: UILabel!
    @IBOutlet weak var NotesLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        TitleLBL.theme_textColor = GlobalPicker.textColor
        self.TitleLBL.font = Font().MontserratBoldFont(font: 17)
        PriceLBL.theme_textColor = GlobalPicker.textColor
        self.PriceLBL.font = Font().MontserratBoldFont(font: 17)
        NotesLBL.textColor = UIColor.colorWithHexString(hexStr: "5A6876")
        self.NotesLBL.font = Font().MontserratRegularFont(font: 10)
        LogoIMG.tintColor = AppDarkColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
