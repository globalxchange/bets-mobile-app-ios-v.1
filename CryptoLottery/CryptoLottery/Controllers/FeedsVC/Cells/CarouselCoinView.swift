//
//  CarouselCoinView.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-04-29.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class CarouselCoinView: UIView {

    @IBOutlet weak var Carousel: UIView!
    @IBOutlet weak var subView: CustomView!
    @IBOutlet weak var IMG: UIImageView!
    
    var CoinIMG: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
    
    func loadViewFromNib() {
        
        let view = UINib(nibName: "CarouselCoinView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
        
//        self.CoinIMG = UIImageView.init(frame: CGRect)
        
        self.IMG.clipsToBounds = true
        self.IMG.layer.cornerRadius = self.IMG.frame.height / 2
    }
    
    func setupView(image: UIImage? = nil, imageURL: String, imgTin: UIColor, bgcolor: UIColor) {
        self.Carousel.backgroundColor = .clear
        self.subView.BGColor = bgcolor
        self.subView.backgroundColor = bgcolor
        if image == nil {
            self.IMG.downloadedFrom(url: URL.init(string: imageURL)!)
        }
        else {
            self.IMG.image = image
        }
        self.IMG.tintColor = imgTin
    }
    
    func HighlightView(imgTin: UIColor, bgcolor: UIColor) {
        self.Carousel.backgroundColor = .clear
        self.subView.BGColor = bgcolor
        self.subView.backgroundColor = bgcolor
        self.IMG.tintColor = imgTin
    }

}
