//
//  LotteryListCell.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-06-19.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import SwipeCellKit

class LotteryListCell: SwipeTableViewCell {

    @IBOutlet weak var FeedCell: CustomView!
    @IBOutlet weak var SubView: CustomView!
    @IBOutlet weak var ColorLBL: UILabel!
    @IBOutlet weak var CellMSG: UILabel!
    
    @IBOutlet weak var DetailsStack: UIStackView!
    
    @IBOutlet weak var JackpotStack: UIStackView!
    @IBOutlet weak var AmountLBL: UILabel!
    
    @IBOutlet weak var JKstack: UIStackView!
    @IBOutlet weak var JackpotLBL: UILabel!
    
    @IBOutlet weak var UpDownStack: UIStackView!
    
    @IBOutlet weak var IntervalStack: UIStackView!
    @IBOutlet weak var Dayview: UIView!
    @IBOutlet weak var DayValueLBL: UILabel!
    @IBOutlet weak var DayLBL: UILabel!
    
    @IBOutlet weak var Houreview: UIView!
    @IBOutlet weak var HoureValueLBL: UILabel!
    @IBOutlet weak var HoureLBL: UILabel!
    
    @IBOutlet weak var Minuteview: UIView!
    @IBOutlet weak var MinuteValueLBL: UILabel!
    @IBOutlet weak var MinuteLBL: UILabel!
    
    @IBOutlet weak var Secondview: UIView!
    @IBOutlet weak var SecondValueLBL: UILabel!
    @IBOutlet weak var SecondLBL: UILabel!
    
    @IBOutlet weak var MilliSecondview: UIView!
    @IBOutlet weak var MilliSecondValueLBL: UILabel!
    @IBOutlet weak var MilliSecondLBL: UILabel!
    
    var timestamp: Double!
    lazy var currentTime: Double = {
        let time = NSDate().timeIntervalSinceNow
        return time
    }()
    var timer: Timer!
    var MLtimer: Timer!
    var MLstartTime: Double = 0
    var MLtime: Double = 0
    
    func UPactionHandler(action:(() -> Void)? = nil) {
        struct __ { static var action :(() -> Void)? }
        if action != nil { __.action = action }
        else { __.action?() }
    }
    
    func DOWNactionHandler(action:(() -> Void)? = nil) {
        struct __ { static var action :(() -> Void)? }
        if action != nil { __.action = action }
        else { __.action?() }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.JackpotLBL.theme_textColor = GlobalPicker.textColor
        self.AmountLBL.theme_textColor = GlobalPicker.textColor
        
        self.JackpotLBL.isSkeletonable = true
        self.AmountLBL.isSkeletonable = true
        
        self.ColorLBL.backgroundColor = AppDarkColor
        let maskPath = UIBezierPath(roundedRect: CGRect.init(x: 0, y: 0, width: self.ColorLBL.frame.width, height: self.ColorLBL.frame.height),
                                    byRoundingCorners: [.bottomLeft, .topLeft],
                                    cornerRadii: CGSize(width: 100.0, height: 100.0))
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = maskPath.cgPath
        self.ColorLBL.layer.mask = maskLayer

        self.CellMSG.textColor = UIColor.white
        
        self.DetailsStack.isHidden = false
        self.IntervalStack.addBackground(color: AppDarkColor)
        self.IntervalStack.isHidden = true
        
        self.Dayview.backgroundColor = AppDarkColor
        self.DayValueLBL.textColor = .white
        self.DayLBL.textColor = .white
        self.DayLBL.text = "Days"
        self.DayValueLBL.font = Font().MontserratBoldFont(font: 17)
        self.DayLBL.font = Font().MontserratRegularFont(font: 11)
        
        self.Houreview.backgroundColor = AppDarkColor
        self.HoureValueLBL.textColor = .white
        self.HoureLBL.textColor = .white
        self.HoureLBL.text = "Hours"
        self.HoureValueLBL.font = Font().MontserratBoldFont(font: 17)
        self.HoureLBL.font = Font().MontserratRegularFont(font: 11)
        
        self.Minuteview.backgroundColor = AppDarkColor
        self.MinuteValueLBL.textColor = .white
        self.MinuteLBL.textColor = .white
        self.MinuteLBL.text = "Minutes"
        self.MinuteValueLBL.font = Font().MontserratBoldFont(font: 17)
        self.MinuteLBL.font = Font().MontserratRegularFont(font: 11)
        
        self.Secondview.backgroundColor = AppDarkColor
        self.SecondValueLBL.textColor = .white
        self.SecondLBL.textColor = .white
        self.SecondLBL.text = "Seconds"
        self.SecondValueLBL.font = Font().MontserratBoldFont(font: 17)
        self.SecondLBL.font = Font().MontserratRegularFont(font: 11)
        
        self.MilliSecondview.backgroundColor = AppDarkColor
        self.MilliSecondValueLBL.textColor = .white
        self.MilliSecondLBL.textColor = .white
        self.MilliSecondLBL.text = "Milliseconds"
        self.MilliSecondValueLBL.font = Font().MontserratBoldFont(font: 17)
        self.MilliSecondLBL.font = Font().MontserratRegularFont(font: 11)
        
        if self.timer != nil {
            self.timer.invalidate()
            self.timer = nil
        }
        if self.MLtimer != nil {
            self.MLtimer.invalidate()
            self.MLtimer = nil
        }
        self.currentTime = NSDate().timeIntervalSince1970
        
        self.SubView.backgroundColor = UIColor.clear
        self.SubView.BGColor = UIColor.clear
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .FeedTimer, object: nil)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func showPrice(price: String) {
        self.FeedCell.backgroundColor = AppDarkColor
        self.FeedCell.BGColor = AppDarkColor
        self.DetailsStack.isHidden = true
        self.IntervalStack.isHidden = true
        self.UpDownStack.isHidden = true
        self.CellMSG.text = String.init(format: "%@ Per Ticket", price)
        self.CellMSG.font = Font().MontserratBoldFont(font: 17)
    }
    
    @IBAction func TappedUP(_ sender: Any) {
        self.UPactionHandler()
    }
    
    @IBAction func TappedDown(_ sender: Any) {
        self.DOWNactionHandler()
    }
    
}

extension LotteryListCell {
    
    func stopTimer() {
        DispatchQueue.main.async {
            self.FeedCell.backgroundColor = UIColor.white
            self.FeedCell.BGColor = UIColor.white
            self.DetailsStack.isHidden = false
            self.UpDownStack.isHidden = false
            self.IntervalStack.isHidden = true
            self.CellMSG.text = ""
            if self.timer != nil {
                self.timer.invalidate()
                self.timer = nil
            }
            if self.MLtimer != nil {
                self.MLtimer.invalidate()
                self.MLtimer = nil
            }
            self.MilliSecondValueLBL.text = String.init(format: "00")
        }
    }
    
    @objc func onDidReceiveData(_ notification:Notification) {
        self.stopTimer()
    }
    
    func UpdateTimer(stamp: Double) {
        DispatchQueue.main.async {
            self.FeedCell.backgroundColor = AppDarkColor
            self.CellMSG.text = ""
            self.FeedCell.BGColor = AppDarkColor
            self.DetailsStack.isHidden = true
            self.UpDownStack.isHidden = true
            self.IntervalStack.isHidden = false
            
            self.timestamp = stamp / 1000
            let final:Double = self.timestamp - self.currentTime
            if final != 0 || final < 0 || self.timestamp > self.currentTime  {
                self.timer = Timer.scheduledTimer(timeInterval: 1.0,
                                                  target: self,
                                                  selector: #selector(self.Timerupdate),
                                                  userInfo: nil,
                                                  repeats: true)
                
                RunLoop.current.add(self.timer, forMode: RunLoop.Mode.common)
                self.timer.fire()
            }
            
            print(Date().offset(from: Date(timeIntervalSinceNow: self.timestamp)))
        }
    }
    
    @objc func advanceTimer() {
        DispatchQueue.main.async {
            if self.timestamp == 0 || self.timestamp <= 0 || self.timestamp < self.currentTime {
                self.stopTimer()
                self.DayValueLBL.text = String.init(format: "00")
                self.HoureValueLBL.text = String.init(format: "00")
                self.MinuteValueLBL.text = String.init(format: "00")
                self.SecondValueLBL.text = String.init(format: "00")
                self.MilliSecondValueLBL.text = String.init(format: "00")
            }
            else {
                self.MLtime = Date().timeIntervalSinceReferenceDate - self.MLstartTime
                let timeString = String(format: "%2.2f", self.MLtime)
                let arry = timeString.components(separatedBy: ".")
                let intArray = arry.map { Int($0)!}
                let decimal = String(format: "%02i", Int(intArray.last!))
                
                self.MilliSecondValueLBL.text = decimal
            }
        }
    }
    
    @objc func Timerupdate() {
        DispatchQueue.main.async {
            let final:Double = self.timestamp - self.currentTime
            if self.timestamp == 0 || self.timestamp <= 0 || self.timestamp < self.currentTime {
                self.stopTimer()
                self.DayValueLBL.text = String.init(format: "00")
                self.HoureValueLBL.text = String.init(format: "00")
                self.MinuteValueLBL.text = String.init(format: "00")
                self.SecondValueLBL.text = String.init(format: "00")
                self.MilliSecondValueLBL.text = String.init(format: "00")
            }
            else {
                self.timestamp -= 1
                
                let time = final.secondsToHoursMinutesSeconds()
                
                self.MLstartTime = Date().timeIntervalSinceReferenceDate
                self.MLtimer = Timer.scheduledTimer(timeInterval: 0.005,
                                                    target: self,
                                                    selector: #selector(self.advanceTimer),
                                                    userInfo: nil,
                                                    repeats: true)
                
                self.DayValueLBL.text = String.init(format: "%02i", time.0!)
                self.HoureValueLBL.text = String.init(format: "%02i", time.1!)
                self.MinuteValueLBL.text = String.init(format: "%02i", time.2!)
                self.SecondValueLBL.text = String.init(format: "%02i", time.3!)
                self.MilliSecondValueLBL.text = String.init(format: "%02i", time.4!)
            }
        }
    }
    
    func getReadableDate(timeStamp: TimeInterval) -> String? {
        let date = Date(timeIntervalSince1970: timeStamp)
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss Z"
        return dateFormatter.string(from: date)
    }
    
    func dateFallsInCurrentWeek(date: Date) -> Bool {
        let currentWeek = Calendar.current.component(Calendar.Component.weekOfYear, from: Date())
        let datesWeek = Calendar.current.component(Calendar.Component.weekOfYear, from: date)
        return (currentWeek == datesWeek)
    }
    
}
