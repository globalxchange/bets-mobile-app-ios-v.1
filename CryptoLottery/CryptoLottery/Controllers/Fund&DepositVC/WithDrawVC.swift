//
//  FD_Step1VC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-06-24.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import CRNotifications

class WithDrawVC: BaseVC {
    
    // MARK:- IBOutlets
    @IBOutlet weak var DF_MainView: CustomView!
    @IBOutlet var DF_bgLBL: [UILabel]!
    
    @IBOutlet weak var DF_MainView_Height: NSLayoutConstraint!
    @IBOutlet weak var DF_NautchLBL: CustomLBL!
    @IBOutlet weak var DF_LogoIMG: UIImageView!
    @IBOutlet weak var DF_MainTitleLBL: UILabel!
    @IBOutlet weak var DF_MainDismissBTN: UIButton!
    
    // TODO:- Step 1 Betvault Selection
    @IBOutlet weak var S1_ShowView: UIView!
    @IBOutlet weak var S1_SegmentView: UIView!
    @IBOutlet var S1_SegmentsBTN: [UIButton]!
    @IBOutlet weak var S1_TBL: UITableView!
    @IBOutlet var AllBottomview: UIView!
    @IBOutlet weak var S1_ProcessBTN: UIButton!
    
    // TODO:- Step 1 Variables
    var S1_SelectedSegment: Int = 0
    var S1_CoinsVault: BetsVaultVault!
    var SectionCoin = [CoinSectionArray]()
    var BetsVaultLoad: BetsVaultPayload!
    
    // MARK:- Variable Define
    var FromCome: String!
    
    // MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        self.DF_MainView.backgroundColor = .white
        self.DF_MainView.BGColor = .white
        self.DF_LogoIMG.tintColor = AppDarkColor
        self.DF_MainTitleLBL.theme_textColor = GlobalPicker.textColor
        
        self.AllBottomview.backgroundColor = AppDarkColor
        self.FromCome = "Withdraw"
        self.cornerColor()
        self.GetFundVault()
        self.GetVaultCoinlist()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.SetupStep_1()
    }
    
    // MARK:- User Define Methods

    func cornerColor(color: UIColor = AppDarkColor) {
        for lbl in self.DF_bgLBL {
            lbl.backgroundColor = color
        }
    }
    
    func Sectionmanage(CoinsVaults: [CoinVaultCoin]) {
        // fiat Section Create
        let fiat = CoinSectionArray.init(fromDictionary: [:])
        fiat.sectionkey = "Fiat"
        self.SectionCoin.append(fiat)
        
        // crypto Section Create
        let crypto = CoinSectionArray.init(fromDictionary: [:])
        crypto.sectionkey = "Crypto"
        self.SectionCoin.append(crypto)
        
        for items in CoinsVaults {
            if items.assetType.uppercased() == "crypto".uppercased() {
                crypto.sectionArray.append(items)
            }
            else {
                fiat.sectionArray.append(items)
            }
        }
    }
    
    func SetupStep_1() {
        
        self.DF_MainDismissBTN.tag = 0
        
        self.cornerColor()
        
        self.DF_MainView_Height.constant = (Screen_height / 2) + (Screen_height / 4)
        
        self.DF_MainTitleLBL.text = "Select The Bets Vault That You Want To Withdraw"
        
        if self.S1_SelectedSegment == 0 {
            self.S1_SegmentsBTN[0].titleLabel?.font = Font().MontserratBoldFont(font: 17)
            self.S1_SegmentsBTN[1].titleLabel?.font = Font().MontserratLightFont(font: 17)
        }
        else {
            self.S1_SelectedSegment = 1
            self.S1_SegmentsBTN[1].titleLabel?.font = Font().MontserratBoldFont(font: 17)
            self.S1_SegmentsBTN[0].titleLabel?.font = Font().MontserratLightFont(font: 17)
        }
        
        self.S1_SegmentView.clipsToBounds = true
        self.S1_SegmentView.backgroundColor = UIColor.colorWithHexString(hexStr: "E4E9F2")
        self.S1_SegmentView.layer.borderWidth = 1.0
        self.S1_SegmentView.layer.borderColor = UIColor.colorWithHexString(hexStr: "E4E9F2").cgColor
        self.S1_SegmentView.layer.cornerRadius = 15
        
        self.S1_SegmentsBTN[0].clipsToBounds = true
        self.S1_SegmentsBTN[1].clipsToBounds = true
        
        self.DF_bgLBL[0].backgroundColor = AppDarkColor
        self.DF_bgLBL[1].backgroundColor = AppDarkColor
        
        self.S1_ShowView.backgroundColor = .white
        self.S1_TBL.backgroundColor = .white
        
        self.S1_ProcessBTN.setTitle("Procced To Withdraw ___ Bets Vault", for: .normal)
        self.S1_ProcessBTN.isUserInteractionEnabled = false
        self.S1_ProcessBTN.alpha = 0.5
        self.S1_ProcessBTN.tintColor = .clear
        self.S1_ProcessBTN.backgroundColor = .clear
        self.S1_ProcessBTN.setTitleColor(.white, for: .normal)
        
        self.S1_TBL.register(UINib.init(nibName: "VaultSelectionCell", bundle: nil), forCellReuseIdentifier: "VaultSelectionCell")
    }
    
    // TODO:- S1_Table Config Methods
    func S1_TableviewCell(indexPath: IndexPath) -> VaultSelectionCell {
        let cell: VaultSelectionCell = self.S1_TBL.dequeueReusableCell(withIdentifier: "VaultSelectionCell") as! VaultSelectionCell
        if self.S1_SelectedSegment == 0 {
            var array = self.BetsVaultLoad.vault
            array?.removeAll(where: { (vault) -> Bool in
                vault.coin.uppercased() == "USD".uppercased()
            })
            let data = array![indexPath.row] as BetsVaultVault
            cell.IMGCoin.downloadedFrom(link: data.imageUrl, contentMode: .scaleAspectFit, radious: (cell.IMGCoin.frame.height / 2))
            cell.CoinName.text = data.coin
            cell.CoinDetail.text = String.init(format: "%f", (data.liveBalance.nativeField)!).CoinPriceThumbRules(Coin: data.coin.uppercased())
            
            if self.S1_CoinsVault == nil {
                cell.CoinName.font = UIFont.systemFont(ofSize: 15)
                cell.CoinDetail.font = UIFont.systemFont(ofSize: 15)
            }
            else {
                if self.S1_CoinsVault.coin == data.coin {
                    cell.CoinName.font = UIFont.boldSystemFont(ofSize: 15)
                    cell.CoinDetail.font = UIFont.boldSystemFont(ofSize: 15)
                }
                else {
                    cell.CoinName.font = UIFont.systemFont(ofSize: 15)
                    cell.CoinDetail.font = UIFont.systemFont(ofSize: 15)
                }
            }
        }
        else {
            let data = self.BetsVaultLoad.vault.filter { (coins) -> Bool in
                coins.coin.uppercased() == "USD".uppercased()
            }.first
            cell.IMGCoin.downloadedFrom(link: data?.imageUrl, contentMode: .scaleAspectFit, radious: (cell.IMGCoin.frame.height / 2))
            cell.CoinName.text = data?.coin.uppercased()
            cell.CoinDetail.text = String.init(format: "%f", (data!.liveBalance.nativeField)!).CoinPriceThumbRules(Coin: data!.coin.uppercased())
            
            if self.S1_CoinsVault == nil {
                cell.CoinName.font = UIFont.systemFont(ofSize: 15)
                cell.CoinDetail.font = UIFont.systemFont(ofSize: 15)
            }
            else {
                if self.S1_CoinsVault.coin == data?.coin {
                    cell.CoinName.font = UIFont.boldSystemFont(ofSize: 15)
                    cell.CoinDetail.font = UIFont.boldSystemFont(ofSize: 15)
                }
                else {
                    cell.CoinName.font = UIFont.systemFont(ofSize: 15)
                    cell.CoinDetail.font = UIFont.systemFont(ofSize: 15)
                }
            }
        }
        return cell
    }
    
    // MARK:- API Calling
    func GetVaultCoinlist() {
        background {
            let param = VaultCoinlistParamDict.init(email: HeaderSetter().email)
            NetworkingRequests.shared.requestsGET(API_GetCoinVaultList, Parameters: param.description, Headers: [:], onSuccess: { (responseObject) in
                main {
                    let root = CoinVaultRootClass.init(fromDictionary: responseObject as [String: Any])
                    if root.status {
                        self.Sectionmanage(CoinsVaults: root.coins)
                    }
                    else {
                        CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: root.message, dismissDelay: 3, completion: {
                            self.GetVaultCoinlist()
                        })
                    }
                }
            }) { (Status, code) in
                main {
                    CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Status, dismissDelay: 3, completion: {
                        
                    })
                }
            }
        }
    }
    
    func GetFundVault() {
        background {
            let balanceobj = UserInfoData.shared.GetObjectdata(key: Vault_Balancelist) as? BetsVaultPayload
            if balanceobj == nil {
                let param = FundVaultParamDict.init(arcade_userid: HeaderSetter().ArcadeUserid)
                let header = CommanHeader.init(email: HeaderSetter().email, token: HeaderSetter().token)
                NetworkingRequests.shared.requestPOST(API_BetsVaultBalance, Parameters: param.description, Headers: header.description, onSuccess: { (responseObject, status) in
                    main {
                        if status {
                            let betsvault: BetsVaultRootClass = BetsVaultRootClass.init(fromDictionary: responseObject as NSDictionary)
                            UserInfoData.shared.SaveObjectdata(data: betsvault as AnyObject, forkey: Vault_Balancelist)
                            self.BetsVaultLoad = betsvault.payload
                            self.S1_TBL.delegate = self
                            self.S1_TBL.dataSource = self
                            self.S1_TBL.reloadData()
                        }
                        else {
                            let Message = responseObject["payload"] as AnyObject
                            CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Message as! String, dismissDelay: 3, completion: {
                                self.GetFundVault()
                            })
                        }
                    }
                }) { (Message, Code) in
                    main {
                        CRNotifications.showNotification(type: CRNotifications.error, title: "ERROR!", message: Message, dismissDelay: 3, completion: {
                            
                        })
                    }
                }
            }
            else {
                main {
                    self.BetsVaultLoad = balanceobj
                    self.S1_TBL.delegate = self
                    self.S1_TBL.dataSource = self
                    self.S1_TBL.reloadData()
                }
            }
        }
    }
    
    // MARK:- IBAction Methods
    @IBAction func TappedDF_MainDismissBTN(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func S1_TappedBTN(_ sender: UIButton) {
        if sender == self.S1_SegmentsBTN[0] {
            self.S1_SelectedSegment = 0
            self.S1_SegmentsBTN[0].titleLabel?.font = Font().MontserratBoldFont(font: 17)
            self.S1_SegmentsBTN[1].titleLabel?.font = Font().MontserratLightFont(font: 17)
            self.S1_CoinsVault = nil
            self.S1_TBL.reloadData()
        }
        else if sender == self.S1_SegmentsBTN[1] {
            self.S1_SelectedSegment = 1
            self.S1_SegmentsBTN[1].titleLabel?.font = Font().MontserratBoldFont(font: 17)
            self.S1_SegmentsBTN[0].titleLabel?.font = Font().MontserratLightFont(font: 17)
            self.S1_CoinsVault = nil
            self.S1_TBL.reloadData()
        }
        else {
            let step2 = FD_Step2VC.init(nibName: "FD_Step2VC", bundle: nil)
            step2.FromCome = self.FromCome
            step2.VaultType = self.S1_SelectedSegment == 0 ? "Crypto" : "Forex"
            step2.S1_CoinsVault = self.S1_CoinsVault
            step2.SectionCoin = self.SectionCoin
            step2.BetsVaultLoad = self.BetsVaultLoad
            self.navigationController?.pushViewController(step2, animated: false)
        }
        if self.S1_CoinsVault == nil {
            self.S1_ProcessBTN.setTitle("Procced To Withdraw ___ Bets Vault", for: .normal)
            self.S1_ProcessBTN.isUserInteractionEnabled = false
            self.S1_ProcessBTN.alpha = 0.5
        }
        else {
            var title = "Proceed To Withdraw Your COIN Bets Vault"
            self.S1_ProcessBTN.setTitle(title.replaceLocalized(fromvalue: ["COIN"], tovalue: [self.S1_CoinsVault.coin]), for: .normal)
            self.S1_ProcessBTN.isUserInteractionEnabled = true
            self.S1_ProcessBTN.alpha = 1
        }
    }
    
}

//MARK:- Tableview Delegate
extension WithDrawVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.S1_SelectedSegment == 0 {
            var array = self.BetsVaultLoad.vault
            array?.removeAll(where: { (vault) -> Bool in
                vault.coin.uppercased() == "USD".uppercased()
            })
            return array!.count
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.S1_TableviewCell(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.S1_SelectedSegment == 0 {
            var array = self.BetsVaultLoad.vault
            array?.removeAll(where: { (vault) -> Bool in
                vault.coin.uppercased() == "USD".uppercased()
            })
            self.S1_CoinsVault = array![indexPath.row]
        }
        else {
            let data = self.BetsVaultLoad.vault.filter { (coins) -> Bool in
                coins.coin.uppercased() == "USD".uppercased()
            }.first
            self.S1_CoinsVault = data
        }
        var title = "Proceed To Withdraw Your COIN Bets Vault"
        self.S1_ProcessBTN.setTitle(title.replaceLocalized(fromvalue: ["COIN"], tovalue: [self.S1_CoinsVault.coin]), for: .normal)
        self.S1_ProcessBTN.isUserInteractionEnabled = true
        self.S1_ProcessBTN.alpha = 1
        self.S1_TBL.reloadData()
    }
    
}
