//
//  Deposit-FundVC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-05-30.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import CRNotifications
import Lottie

class Deposit_FundVC: BaseVC {
    
    // MARK:- IBOutlets
    @IBOutlet weak var DF_MainView: CustomView!
    @IBOutlet var DF_bgLBL: [UILabel]!
    
    @IBOutlet weak var DF_MainView_Height: NSLayoutConstraint!
    @IBOutlet weak var DF_NautchLBL: CustomLBL!
    @IBOutlet weak var DF_LogoIMG: UIImageView!
    @IBOutlet weak var DF_MainTitleLBL: UILabel!
    @IBOutlet weak var DF_MainDismissBTN: UIButton!
    
    @IBOutlet var AllBottomview: [UIView]!
    
    // TODO:- Step 1 Betvault Selection
    @IBOutlet weak var S1_ShowView: UIView!
    @IBOutlet weak var S1_SegmentView: UIView!
    @IBOutlet var S1_SegmentsBTN: [UIButton]!
    @IBOutlet weak var S1_TBL_TOP: NSLayoutConstraint!
    @IBOutlet weak var S1_TBL: UITableView!
    @IBOutlet weak var S1_ProcessBTN: UIButton!
    
    // TODO:- Step 2 Funding Methods
    @IBOutlet weak var S2_ShowView: UIView!
    @IBOutlet weak var S2_TBL: UITableView!
    @IBOutlet weak var S2_ProcessBTN: UIButton!
    
    // TODO:- Step 3 QRcode Methods
    @IBOutlet weak var S3_QRView: UIView!
    @IBOutlet weak var S3_QRimage: UIImageView!
    @IBOutlet weak var S3_CodeView: UIView!
    @IBOutlet weak var S3_QRCodelbl: UILabel!
    @IBOutlet weak var S3_QRCopyBTN: UIButton!
    @IBOutlet weak var S3_ProcessBTN: UIButton!
    
    // TODO:- Step 4 DepositViews
    @IBOutlet weak var S4_DepositView: UIView!
    @IBOutlet weak var S4_TBL: UITableView!
    @IBOutlet weak var S4_ProcessBTN: UIButton!
    
    // TODO:- Step 5 CoinSelectionView
    @IBOutlet weak var S5_CoinSectionview: UIView!
    @IBOutlet weak var S5_SegmentView: UIView!
    @IBOutlet var S5_SegmentBTN: [UIButton]!
    @IBOutlet weak var S5_TBL_TOP: NSLayoutConstraint!
    @IBOutlet weak var S5_TBL: UITableView!
    @IBOutlet weak var S5_ProcessBTN: UIButton!
    
    // TODO:- Step 6 Amount Enter View
    @IBOutlet weak var S6_AmountView: UIView!
    @IBOutlet weak var S6_Amount1LBL: UILabel!
    @IBOutlet weak var S6_25BTN: CustomBTN!
    @IBOutlet weak var S6_50BTN: CustomBTN!
    @IBOutlet weak var S6_Symbol1LBL: UILabel!
    @IBOutlet weak var S6_SepratorLBL: UILabel!
    @IBOutlet weak var S6_Amount2LBL: UILabel!
    @IBOutlet weak var S6_Symbol2LBL: UILabel!
    @IBOutlet weak var S6_DigitView: UIView!
    @IBOutlet var S6_DigitsBTN: [UIButton]!
    @IBOutlet weak var S6_ProcessBTN: UIButton!
    
    // TODO:- Step 7 Animation View
    @IBOutlet weak var S7_Animationview: UIView!
    @IBOutlet weak var S7_Animation: UIView!
    @IBOutlet weak var S7_AnimationDetails: UILabel!
    
    // TODO:- Step 8 Congrest View
    @IBOutlet weak var S8_CongratulationView: UIView!
    @IBOutlet weak var S8_CongretTitleLBL: UILabel!
    @IBOutlet weak var S8_CongretCoinView: CustomView!
    @IBOutlet weak var S8_CongretCoinIMG: UIImageView!
    @IBOutlet weak var S8_CongretCoinName: UILabel!
    @IBOutlet weak var S8_CongretCoinSeprator: UILabel!
    @IBOutlet weak var S8_CongretCoinDetail: UILabel!
    @IBOutlet weak var S8_Collection: UICollectionView!
    @IBOutlet weak var S8_CloseBTN: UIButton!
    
    // MARK:- Variable Define
    var FromCome: String!
    var FinalCoinAddress: String = ""
    
    let email: String = UserInfoData.shared.GetUserEmail()!
    let token: String = UserInfoData.shared.GetUserToken()!
    
    func DismissCallBack(action:(() -> Void)? = nil) {
        struct __ { static var action :(() -> Void)? }
        if action != nil { __.action = action }
        else { __.action?() }
    }
    
    // TODO:- Step 1 Variables
    var S1_SelectedSegment: Int = 0
    var S1_CoinsVault: CoinVaultCoin!
    var CoinsVault : [CoinVaultCoin]!
    var BetVault: [BetsVaultVault]!
    var BetCoins: [BetsVaultCoin]!
    var SectionCoin = [CoinSectionArray]()
    var BetsVaultLoad: BetsVaultPayload!
    
    // TODO:- Setp 2 Variables
    var S2_SelectedPayIndex: Int!
    var S2_StaticPayOption: [String] = ["VaultDeposit", "snapay"]
    var isEnableforAmount: Bool = false
    
    // TODO:- Setp 3 Variables
    
    // TODO:- Step 4 Variables
    var CS_SelectedSegment: Int = 0
    var DP_Process_Tag:Int = 100 // tag 100 = processed, tag 200 = Deposit , tag 300 = Deposti Api call
    
    // TODO:- Step 6 Variables
    var Topupamount1: String! = "0.00"
    var Topupamount2: String! = "0.00"
    var PinArray: NSMutableArray!
    var isFirstAmount: Bool = true
    
    // TODO:- Other Variables
    
    var typedValue: Int = 0
    var Selected_Vaultcoins : CoinVaultCoin!
    var Selected_betVault: BetsVaultVault!
    
    // MARK:- View Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.DF_MainView.backgroundColor = .white
        self.DF_MainView.BGColor = .white
        self.DF_LogoIMG.tintColor = AppDarkColor
        self.DF_MainTitleLBL.theme_textColor = GlobalPicker.textColor
        
        for BottomView in self.AllBottomview {
            BottomView.backgroundColor = AppDarkColor
        }
        self.GetFundVault()
        self.GetVaultCoinlist()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.SetupStep_1()
    }
    
    // MARK:- user Define Methods
    
    func cornerColor(color: UIColor = AppDarkColor) {
        for lbl in self.DF_bgLBL {
            lbl.backgroundColor = color
        }
    }
    
    func getBetVault() -> String? {
        return self.S1_CoinsVault == nil ? "" : self.S1_CoinsVault.coinSymbol.uppercased()
    }
    
    func GetSelected_betVault_Coin() -> String? {
        return self.Selected_betVault == nil ? "" : self.Selected_betVault.coin.uppercased()
    }
    
    // MARK:- API Calling
    func GetVaultCoinlist() {
        background {
            NetworkingRequests.shared.requestsGET(API_GetCoinVaultList, Parameters: ["email" : self.email], Headers: [:], onSuccess: { (responseObject) in
                main {
                    let root = CoinVaultRootClass.init(fromDictionary: responseObject as [String: Any])
                    if root.status {
                        self.CoinsVault = root.coins
                        self.Sectionmanage()
                    }
                    else {
                        CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: root.message, dismissDelay: 3, completion: {
                            self.GetVaultCoinlist()
                        })
                    }
                }
            }) { (Status, code) in
                main {
                    CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Status, dismissDelay: 3, completion: {
                        
                    })
                }
            }
        }
    }
    
    func GetFundVault() {
        background {
            let balanceobj = UserInfoData.shared.GetObjectdata(key: Vault_Balancelist) as? BetsVaultPayload
            if balanceobj == nil {
                let param = BetVaultParamDict.init(email: self.email)
                let header = CommanHeader.init(email: self.email, token: self.token)
                NetworkingRequests.shared.requestPOST(API_BetsVaultBalance, Parameters: param.description, Headers: header.description, onSuccess: { (responseObject, status) in
                    main {
                        if status {
                            let betsvault: BetsVaultRootClass = BetsVaultRootClass.init(fromDictionary: responseObject)
                            UserInfoData.shared.SaveObjectdata(data: betsvault as AnyObject, forkey: Vault_Balancelist)
                            self.BetsVaultLoad = betsvault.payload
                            self.BetVault = betsvault.payload.vault
                            self.BetCoins = betsvault.payload.coins
                        }
                        else {
                            let Message = responseObject["payload"] as AnyObject
                            CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Message as! String, dismissDelay: 3, completion: {
                                self.GetFundVault()
                            })
                        }
                    }
                }) { (Message, Code) in
                    main {
                        CRNotifications.showNotification(type: CRNotifications.error, title: "ERROR!", message: Message, dismissDelay: 3, completion: {
                            
                        })
                    }
                }
            }
            else {
                main {
                    self.BetVault = balanceobj!.vault
                }
            }
        }
    }
    
    func ConvertForex(Buy: String, From: String, Value: String) {
        CommanApiCall().ForexConversion(Buy: Buy, From: From) { (keyvalue1, keyvalue2) in
            if self.isFirstAmount {
                if let n = NumberFormatter().number(from: Value) {
                    let cgvalue = Double(truncating: n)
                    let convert = cgvalue * Double(keyvalue1)
                    self.S6_Amount2LBL.text! = String.init(format: "%f", convert).ConvertIntoUSDFormat()!
                    self.Topupamount2 = String.init(format: "%f", convert).ConvertIntoUSDFormat()
                }
            }
            else {
                if let n = NumberFormatter().number(from: Value) {
                    let cgvalue = Double(truncating: n)
                    let convert = cgvalue / Double(keyvalue2)
                    self.S6_Amount1LBL.text! = String.init(format: "%f", convert).ConvertIntoUSDFormat()!
                    self.Topupamount2 = String.init(format: "%f", cgvalue).ConvertIntoUSDFormat()
                }
            }
        }
    }
    
    func GetCoinAddressCode(Coin: String, CodeSuccess: ((_ status: Bool, _ Address: String)->Void)?) {
        background {
            let param = VaultAddressParamDict.init(email: self.email, coin: Coin)
            let header = CommanHeader.init(email: self.email, token: self.token)
            NetworkingRequests.shared.requestPOST(API_GetVault_Address, Parameters: param.description, Headers: header.description, onSuccess: { (responseObject, status) in
                main {
                    guard let body = responseObject as? [String: Any] else {
                        return
                    }
                    if status {
                        if (body["status"] != nil) {
                            if body.keys.contains("address") {
                                self.FinalCoinAddress = body["address"] as! String
                                CodeSuccess!(status, self.FinalCoinAddress)
                            }
                            else if body.keys.contains("payload") {
                                self.FinalCoinAddress = body["payload"] as! String
                                CodeSuccess!(status, self.FinalCoinAddress)
                            }
                            else {
                                CodeSuccess!(status, "")
                            }
                        }
                        else {
                            CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: body["payload"] as! String, dismissDelay: 3, completion: {
                                
                            })
                            CodeSuccess!(status, "")
                        }
                    }
                    else {
                        CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: body["payload"] as! String, dismissDelay: 3, completion: {
                            
                        })
                        CodeSuccess!(status, "")
                    }
                }
            }) { (Status, code) in
                main {
                    CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Status, dismissDelay: 3, completion: {
                        
                    })
                    CodeSuccess!(false, "")
                }
            }
        }
    }
    
    func TopupAmount() {
        self.SetupAnimation_7()
        background {
            var amount: Double = 0.0
            if let n = NumberFormatter().number(from: self.Topupamount2 as String) {
                amount = Double(truncating: n)
            }
            let param = BetVaultTopupParam.init(email: self.email, source_coin: self.Selected_Vaultcoins.coinSymbol, dest_coin: self.GetSelected_betVault_Coin()!, amount: amount)
            let header = CommanHeader.init(email: self.email, token: self.token)
            NetworkingRequests.shared.requestPOST(API_TopupVault, Parameters: param.description, Headers: header.description, onSuccess: { (responseObject, status) in
                main {
                    if status {
                        self.SetupCongrestView_8()
                    }
                    else {
                        let Message = responseObject["payload"] as AnyObject
                        CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Message as! String, dismissDelay: 3, completion: {
                            self.SetupDPWD_4()
                        })
                    }
                }
            }) { (Message, Code) in
                main {
                    CRNotifications.showNotification(type: CRNotifications.error, title: "ERROR!", message: Message, dismissDelay: 3, completion: {
                        self.SetupDPWD_4()
                    })
                }
            }
        }
    }
    
    func WithdrawAmount() {
        self.SetupAnimation_7()
        background {
            var amount: Double = 0.0
            if let n = NumberFormatter().number(from: self.Topupamount2 as String) {
                amount = Double(truncating: n)
            }
            let param = BetVaultTopupParam.init(email: self.email, source_coin: self.Selected_Vaultcoins.coinSymbol, dest_coin: self.GetSelected_betVault_Coin()!, amount: amount)
            let header = CommanHeader.init(email: self.email, token: self.token)
            NetworkingRequests.shared.requestPOST(API_WithdrawVault, Parameters: param.description, Headers: header.description, onSuccess: { (responseObject, status) in
                main {
                    if status {
                        self.SetupCongrestView_8()
                    }
                    else {
                        let Message = responseObject["payload"] as AnyObject
                        CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Message as! String, dismissDelay: 3, completion: {
                            self.SetupDPWD_4()
                        })
                    }
                }
            }) { (Message, Code) in
                main {
                    CRNotifications.showNotification(type: CRNotifications.error, title: "ERROR!", message: Message, dismissDelay: 3, completion: {
                        self.SetupDPWD_4()
                    })
                }
            }
        }
    }
    
    // MARK:- IBAction Methods
    @IBAction func TappedDF_MainDismissBTN(_ sender: Any) {
        if self.DF_MainDismissBTN.tag == 0 {
            self.dismiss(animated: true) {
                self.DismissCallBack()
            }
        }
        else if self.DF_MainDismissBTN.tag == 1 {
            self.SetupStep_1()
        }
        else if self.DF_MainDismissBTN.tag == 2 {
            self.SetupStep_2()
        }
        else if self.DF_MainDismissBTN.tag == 3 {
            if self.S2_SelectedPayIndex == 0 {
                self.SetupStep_3()
            }
            else if self.S2_SelectedPayIndex == 1 {
                self.SetupStep_2()
            }
            else {
                
            }
        }
        else if self.DF_MainDismissBTN.tag == 4 {
            if self.FromCome.uppercased() == "FUND".uppercased() {
                self.isEnableforAmount = false
                self.SetupStep_2()
            }
            else {
                self.isEnableforAmount = false
                self.SetupStep_2()
            }
        }
        else if self.DF_MainDismissBTN.tag == 5 {
            self.isEnableforAmount = true
            self.SetupDPWD_4()
            self.DF_MainDismissBTN.tag = 4
        }
    }
    
}

//MARK:- Step 1 Related Functions
extension Deposit_FundVC {
    
    func Sectionmanage() {
        
        // fiat Section Create
        let fiat = CoinSectionArray.init(fromDictionary: [:])
        fiat.sectionkey = "Fiat"
        self.SectionCoin.append(fiat)
        
        // crypto Section Create
        let crypto = CoinSectionArray.init(fromDictionary: [:])
        crypto.sectionkey = "Crypto"
        self.SectionCoin.append(crypto)
        
        for items in self.CoinsVault {
            if items.assetType.uppercased() == "crypto".uppercased() {
                crypto.sectionArray.append(items)
            }
            else {
                fiat.sectionArray.append(items)
            }
        }
        if self.SectionCoin.count != 0 {
            self.S1_TBL.delegate = self
            self.S1_TBL.dataSource = self
            self.S1_TBL.reloadData()
        }
    }
    
    func SetupStep_1() {
        
        self.DF_MainDismissBTN.tag = 0
        
        self.cornerColor()
        
        self.S1_ShowView.isHidden = false
        self.S2_ShowView.isHidden = true
        self.S3_QRView.isHidden = true
        self.S4_DepositView.isHidden = true
        self.S5_CoinSectionview.isHidden = true
        self.S6_AmountView.isHidden = true
        self.S7_Animationview.isHidden = true
        self.S8_CongratulationView.isHidden = true
        
        self.DF_MainView_Height.constant = 130 + (Screen_height / 2)
        
        self.DF_MainTitleLBL.text = "Select The Bets Vault That You Want To Fund"
        
        if self.S1_SelectedSegment == 0 {
            self.S1_SegmentsBTN[0].titleLabel?.font = Font().BoldFont(font: 17)
            self.S1_SegmentsBTN[1].titleLabel?.font = Font().LightFont(font: 17)
        }
        else {
            self.S1_SelectedSegment = 1
            self.S1_SegmentsBTN[1].titleLabel?.font = Font().BoldFont(font: 17)
            self.S1_SegmentsBTN[0].titleLabel?.font = Font().LightFont(font: 17)
        }
        
        self.S1_SegmentView.clipsToBounds = true
        self.S1_SegmentView.backgroundColor = UIColor.colorWithHexString(hexStr: "E4E9F2")
        self.S1_SegmentView.layer.borderWidth = 1.0
        self.S1_SegmentView.layer.borderColor = UIColor.colorWithHexString(hexStr: "E4E9F2").cgColor
        self.S1_SegmentView.layer.cornerRadius = 15
        
        self.S1_SegmentsBTN[0].clipsToBounds = true
        self.S1_SegmentsBTN[1].clipsToBounds = true
        
        self.DF_bgLBL[0].backgroundColor = AppDarkColor
        self.DF_bgLBL[1].backgroundColor = AppDarkColor
        
        self.S1_ShowView.backgroundColor = .white
        self.S1_TBL.backgroundColor = .white
        
        self.S1_ProcessBTN.setTitle("Procced To Fund ___ Bets Vault", for: .normal)
        self.S1_ProcessBTN.isUserInteractionEnabled = false
        self.S1_ProcessBTN.alpha = 0.5
        self.S1_ProcessBTN.tintColor = .clear
        self.S1_ProcessBTN.backgroundColor = .clear
        self.S1_ProcessBTN.setTitleColor(.white, for: .normal)
        
        self.S1_TBL.register(UINib.init(nibName: "VaultSelectionCell", bundle: nil), forCellReuseIdentifier: "VaultSelectionCell")
    }
    
    @IBAction func S1_TappedBTN(_ sender: UIButton) {
        if sender == self.S1_SegmentsBTN[0] {
            self.S1_SelectedSegment = 0
            self.S1_SegmentsBTN[0].titleLabel?.font = Font().BoldFont(font: 17)
            self.S1_SegmentsBTN[1].titleLabel?.font = Font().LightFont(font: 17)
            self.DF_MainTitleLBL.text = "Select The Bets Vault You Want To Deposit"
            self.S1_TBL.reloadData()
        }
        else if sender == self.S1_SegmentsBTN[1] {
            self.S1_SelectedSegment = 1
            self.S1_SegmentsBTN[1].titleLabel?.font = Font().BoldFont(font: 17)
            self.S1_SegmentsBTN[0].titleLabel?.font = Font().LightFont(font: 17)
            self.DF_MainTitleLBL.text = "Select The Bets Vault You Want To Withdraw"
            self.S1_TBL.reloadData()
        }
        else {
            //            if self.S1_BetVault != nil && self.S1_CoinsVault != nil {
            self.SetupStep_2()
            //            }
            //            else {
            //                if self.S1_CoinsVault == nil {
            //                    self.S1_TappedBTN(self.S1_SegmentsBTN[0])
            //                }
            //                else {
            //                    self.S1_TappedBTN(self.S1_SegmentsBTN[1])
            //                }
            //            }
        }
    }
    
}

//MARK:- Step 2 Related Functions
extension Deposit_FundVC {
    
    func SetupStep_2() {
        self.DF_MainDismissBTN.tag = 1
        
        self.cornerColor(color: UIColor.white)
        
        self.DF_MainView_Height.constant = (Screen_height / 2) + 50
        
        self.S2_ShowView.isHidden = false
        self.S1_ShowView.isHidden = true
        self.S3_QRView.isHidden = true
        self.S4_DepositView.isHidden = true
        self.S5_CoinSectionview.isHidden = true
        self.S6_AmountView.isHidden = true
        self.S7_Animationview.isHidden = true
        self.S8_CongratulationView.isHidden = true
        
        self.DF_MainTitleLBL.text = "Selecting Funding Method"
        
        self.S2_ShowView.backgroundColor = .white
        self.S2_TBL.backgroundColor = .white
        
        self.S2_ProcessBTN.setTitle("Proceed To Fund With _____", for: .normal)
        self.S2_ProcessBTN.isUserInteractionEnabled = false
        self.S2_ProcessBTN.alpha = 0.5
        self.S2_ProcessBTN.tintColor = .clear
        self.S2_ProcessBTN.backgroundColor = .clear
        self.S2_ProcessBTN.setTitleColor(.white, for: .normal)
        
        self.S2_TBL.register(UINib.init(nibName: "FundingCell", bundle: nil), forCellReuseIdentifier: "FundingCell")
        
        self.S2_TBL.delegate = self
        self.S2_TBL.dataSource = self
        self.S2_TBL.reloadData()
    }
    
    @IBAction func S2_TappedBTN(_ sender: UIButton) {
        if self.FinalCoinAddress.count != 0 {
            if self.S2_SelectedPayIndex == 1 {
                self.isEnableforAmount = false
                self.DP_Process_Tag = 100
                self.SetupCoinSelection_5()
            }
            else if self.S2_SelectedPayIndex == 0 {
                self.SetupStep_3()
            }
            else {
                
            }
        }
    }
    
}

//MARK:- Step 3 Related Functions
extension Deposit_FundVC {
    
    func SetupStep_3() {
        self.DF_MainDismissBTN.tag = 2
        
        self.DF_bgLBL[0].backgroundColor = AppDarkColor
        self.DF_bgLBL[1].backgroundColor = AppDarkColor
        
        self.S3_QRView.isHidden = false
        self.S1_ShowView.isHidden = true
        self.S2_ShowView.isHidden = true
        self.S4_DepositView.isHidden = true
        self.S5_CoinSectionview.isHidden = true
        self.S6_AmountView.isHidden = true
        self.S7_Animationview.isHidden = true
        self.S8_CongratulationView.isHidden = true
        
        self.DF_MainTitleLBL.text = "Here Is Your Bitcoin Address"
        
        self.GenerateQRcode(QRValue: self.FinalCoinAddress)
        
        self.S3_CodeView.clipsToBounds = true
        self.S3_CodeView.layer.borderColor = AppDarkColor.withAlphaComponent(0.2).cgColor
        self.S3_CodeView.layer.borderWidth = 1.0
        self.S3_CodeView.layer.cornerRadius = 3
        
        self.S3_QRCopyBTN.clipsToBounds = true
        self.S3_QRCopyBTN.layer.borderColor = AppDarkColor.withAlphaComponent(0.2).cgColor
        self.S3_QRCopyBTN.layer.borderWidth = 1.0
        self.S3_QRCopyBTN.layer.cornerRadius = 3
        
        self.S3_QRView.backgroundColor = .white
        
        self.S3_ProcessBTN.setTitle("Share", for: .normal)
        self.S3_ProcessBTN.tintColor = .clear
        self.S3_ProcessBTN.backgroundColor = .clear
        self.S3_ProcessBTN.setTitleColor(.white, for: .normal)
        self.S3_ProcessBTN.setTitleColor(.white, for: .selected)
    }
    
    func GenerateQRcode(QRValue: String) {
        self.FinalCoinAddress = QRValue
        self.S3_QRCodelbl.text = QRValue
        // Get data from the string
        let data = QRValue.data(using: String.Encoding.ascii)
        // Get a QR CIFilter
        guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else { return }
        // Input the data
        qrFilter.setValue(data, forKey: "inputMessage")
        // Get the output image
        guard let qrImage = qrFilter.outputImage else { return }
        // Scale the image
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let scaledQrImage = qrImage.transformed(by: transform)
        // Do some processing to get the UIImage
        let context = CIContext()
        guard let cgImage = context.createCGImage(scaledQrImage, from: scaledQrImage.extent) else { return }
        self.S3_QRimage.image = UIImage(cgImage: cgImage)
    }
    
    func ShareQRCode(QRValue: String) {
        //        let secondActivityItem : NSURL = NSURL(string: "http//:urlyouwant")!
        // If you want to put an image
        let image : UIImage = self.S3_QRimage.image!
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [QRValue, image], applicationActivities: nil)
        
        // This lines is for the popover you need to show in iPad
        activityViewController.popoverPresentationController?.sourceView = self.S3_ProcessBTN
        
        // This line remove the arrow of the popover to show in iPad
        activityViewController.popoverPresentationController?.permittedArrowDirections = .any
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.postToFacebook
            ,UIActivity.ActivityType.postToTwitter
            ,UIActivity.ActivityType.postToWeibo
            ,UIActivity.ActivityType.message
            ,UIActivity.ActivityType.mail
            ,UIActivity.ActivityType.print
            ,UIActivity.ActivityType.copyToPasteboard
            ,UIActivity.ActivityType.assignToContact
            ,UIActivity.ActivityType.saveToCameraRoll
            ,UIActivity.ActivityType.addToReadingList
            ,UIActivity.ActivityType.postToFlickr
            ,UIActivity.ActivityType.postToVimeo
            ,UIActivity.ActivityType.postToTencentWeibo
            ,UIActivity.ActivityType.airDrop
            ,UIActivity.ActivityType.openInIBooks
            ,UIActivity.ActivityType.markupAsPDF
        ]
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func S3_TappedBTN(_ sender: UIButton) {
        if sender == self.S3_QRCopyBTN {
            if self.FinalCoinAddress.count > 0 {
                let pasteboard = UIPasteboard.general
                pasteboard.string = ""
                pasteboard.string = self.FinalCoinAddress
                if let string = pasteboard.string {
                    print(string)
                    self.S3_QRCodelbl.text = "Copied To Clipboard"
                }
            }
        }
        else {
            if self.S3_ProcessBTN.isSelected {
                self.S3_ProcessBTN.isSelected = false
                self.dismiss(animated: true) {
                    self.DismissCallBack()
                }
            }
            else {
                self.DF_MainDismissBTN.tag = 0
                self.ShareQRCode(QRValue: self.FinalCoinAddress)
                self.S3_ProcessBTN.setTitleColor(.white, for: .selected)
                self.S3_ProcessBTN.backgroundColor = .clear
                self.S3_ProcessBTN.setTitle("Close", for: .normal)
                self.S3_ProcessBTN.isSelected = true
            }
        }
    }
    
}

//MARK:- Step 4 Related Functions
extension Deposit_FundVC {
    
    func SetupDPWD_4() {
        self.DF_MainDismissBTN.tag = 3
        
        self.DF_bgLBL[0].backgroundColor = AppDarkColor
        self.DF_bgLBL[1].backgroundColor = AppDarkColor
        
        self.S4_DepositView.isHidden = false
        self.S1_ShowView.isHidden = true
        self.S2_ShowView.isHidden = true
        self.S3_QRView.isHidden = true
        self.S5_CoinSectionview.isHidden = true
        self.S6_AmountView.isHidden = true
        self.S7_Animationview.isHidden = true
        self.S8_CongratulationView.isHidden = true
        
        if self.FromCome.uppercased() == "FUND".uppercased() {
            self.DF_MainView_Height.constant = 130 + (2 * 120) + 100
            self.DF_MainTitleLBL.text = "Select Vault For Deposit"
            self.S5_TBL_TOP.constant = 53
        }
        else {
            //            self.MainView_Height.constant = 130 + 110 + 100
            self.DF_MainView_Height.constant = 130 + (2 * 120) + 100
            self.DF_MainTitleLBL.text = "Select Vault For Widthdraw"
            self.S5_TBL_TOP.constant = 53
        }
        
        self.S4_DepositView.backgroundColor = .white
        self.S4_TBL.backgroundColor = .white
        
        self.S4_ProcessBTN.setTitle("Proceed", for: .normal)
        self.S4_ProcessBTN.tintColor = .clear
        self.S4_ProcessBTN.backgroundColor = .clear
        self.S4_ProcessBTN.setTitleColor(.white, for: .normal)
        self.S4_ProcessBTN.tag = self.DP_Process_Tag
        
        if self.DP_Process_Tag == 100 {
            self.S4_TBL.register(UINib.init(nibName: "VaultCoinCell", bundle: nil), forCellReuseIdentifier: "VaultCoinCell")
            self.S4_TBL.delegate = self
            self.S4_TBL.dataSource = self
            self.S4_TBL.reloadData()
        }
        else if self.DP_Process_Tag == 200 {
            if self.isEnableforAmount {
                self.S4_ProcessBTN.setTitleColor(.white, for: .normal)
                self.isEnableforAmount = true
                if self.FromCome.uppercased() == "FUND".uppercased() {
                    self.DF_MainTitleLBL.text = "Enter Amount For Deposit"
                    self.S4_ProcessBTN.setTitle("Deposit", for: .normal)
                }
                else {
                    self.DF_MainTitleLBL.text = "Enter Amount For Widthdraw"
                    self.S4_ProcessBTN.setTitle("Widthdraw", for: .normal)
                }
                self.DP_Process_Tag = 200
                self.S4_ProcessBTN.tag = self.DP_Process_Tag
                self.S4_TBL.reloadData()
            }
        }
        else if self.DP_Process_Tag == 300 {
            if self.FromCome.uppercased() == "FUND".uppercased() {
                self.DF_MainView_Height.constant = 130 + (2 * 120) + 110
                self.DF_MainTitleLBL.text = "Enter Amount For Deposit"
                var amount: Double = 0.00
                if let n = NumberFormatter().number(from: self.Topupamount2 as String) {
                    amount = Double(truncating: n)
                }
                let title = String.init(format: "Deposit %@ ", self.Selected_Vaultcoins.coinSymbol) + String.init(format: "%f", amount).CoinPriceThumbRules(Coin: self.GetSelected_betVault_Coin()!)!
                self.S7_AnimationDetails.text = String.init(format: "Depositing %@ ", self.Selected_Vaultcoins.coinSymbol) + String.init(format: "%f", amount).CoinPriceThumbRules(Coin: self.GetSelected_betVault_Coin()!)!
                
                self.S8_CongretCoinIMG.downloadedFrom(link: self.Selected_Vaultcoins.coinImage, contentMode: .scaleAspectFit, radious: (self.S8_CongretCoinIMG.frame.height / 2))
                self.S8_CongretCoinName.text = self.Selected_Vaultcoins.coinName
                self.S8_CongretCoinDetail.text = String.init(format: "%f", amount).CoinPriceThumbRules(Coin: self.Selected_Vaultcoins.symbol)
                
                self.S8_CongretTitleLBL.text = String.init(format: "Congratulations Your Have Depositd %@ ", self.Selected_Vaultcoins.coinSymbol) + String.init(format: "%f", amount).CoinPriceThumbRules(Coin: self.GetSelected_betVault_Coin()!)! + "Into Your Bets Vault"
                
                self.S4_ProcessBTN.setTitle(title, for: .normal)
            }
            else {
                self.DF_MainView_Height.constant = 130 + (2 * 120) + 110
                self.DF_MainTitleLBL.text = "Enter Amount For Widthdraw"
                
                var amount: Double = 0.00
                if let n = NumberFormatter().number(from: self.Topupamount2 as String) {
                    amount = Double(truncating: n)
                }
                
                let title = String.init(format: "Widthdraw ") + String.init(format: "%f", amount).CoinPriceThumbRules(Coin: self.GetSelected_betVault_Coin()!)!
                self.S7_AnimationDetails.text = String.init(format: "Widthdrawing ") + String.init(format: "%f", amount).CoinPriceThumbRules(Coin: self.GetSelected_betVault_Coin()!)!
                
                self.S8_CongretCoinIMG.downloadedFrom(link: self.Selected_betVault.imageUrl, contentMode: .scaleAspectFit, radious: (self.S8_CongretCoinIMG.frame.height / 2))
                self.S8_CongretCoinName.text = self.GetSelected_betVault_Coin()!
                
                self.S8_CongretCoinDetail.text = String.init(format: "%f", amount).CoinPriceThumbRules(Coin: self.GetSelected_betVault_Coin()!)
                
                self.S8_CongretTitleLBL.text = String.init(format: "Congratulations Your Have Widthdraw  ") + String.init(format: "%f", amount).CoinPriceThumbRules(Coin: self.GetSelected_betVault_Coin()!)! + " From Your Bets Vault"
                
                self.S4_ProcessBTN.setTitle(title, for: .normal)
            }
            self.S4_TBL.reloadData()
        }
        
    }
    
    @IBAction func S4_TappedBTN(_ sender: Any) {
        if self.DP_Process_Tag == 100 {
            self.isEnableforAmount = true
            self.DP_Process_Tag = 200
            self.SetupDPWD_4()
        }
        else if self.DP_Process_Tag == 200 {
            self.DP_Process_Tag = 300
            self.SetupAmountView_6()
        }
        else if self.DP_Process_Tag == 300 {
            if self.FromCome.uppercased() == "FUND".uppercased() {
                self.TopupAmount()
            }
            else {
                self.WithdrawAmount()
            }
        }
    }
    
}

//MARK:- Step 5 Related Functions
extension Deposit_FundVC {
    
    func SetupCoinSelection_5(index: Int = 0) {
        if self.Selected_Vaultcoins == nil && self.Selected_betVault == nil {
            self.DF_MainDismissBTN.tag = 4
        }
        else {
            self.DF_MainDismissBTN.tag = 3
        }
        
        self.DF_MainView_Height.constant = 130 + (Screen_height / 2)
        self.DF_bgLBL[0].backgroundColor = AppDarkColor
        self.DF_bgLBL[1].backgroundColor = AppDarkColor
        
        self.S5_CoinSectionview.isHidden = false
        self.S1_ShowView.isHidden = true
        self.S2_ShowView.isHidden = true
        self.S3_QRView.isHidden = true
        self.S4_DepositView.isHidden = true
        self.S6_AmountView.isHidden = true
        self.S7_Animationview.isHidden = true
        self.S8_CongratulationView.isHidden = true
        
        if index == 0 {
            self.CS_SelectedSegment = 0
            self.S5_SegmentBTN[0].titleLabel?.font = Font().BoldFont(font: 17)
            self.S5_SegmentBTN[1].titleLabel?.font = Font().LightFont(font: 17)
            self.DF_MainTitleLBL.text = "Select Liquid Vault"
        }
        else {
            self.CS_SelectedSegment = 1
            self.S5_SegmentBTN[1].titleLabel?.font = Font().BoldFont(font: 17)
            self.S5_SegmentBTN[0].titleLabel?.font = Font().LightFont(font: 17)
            self.DF_MainTitleLBL.text = "Select Bets Vault"
        }
        
        self.CS_SelectedSegment = index
        
        self.S5_SegmentView.clipsToBounds = true
        self.S5_SegmentView.backgroundColor = UIColor.colorWithHexString(hexStr: "E4E9F2")
        self.S5_SegmentView.layer.borderWidth = 1.0
        self.S5_SegmentView.layer.borderColor = UIColor.colorWithHexString(hexStr: "E4E9F2").cgColor
        self.S5_SegmentView.layer.cornerRadius = 15
        
        self.S5_SegmentBTN[0].clipsToBounds = true
        
        self.S5_SegmentBTN[1].clipsToBounds = true
        
        self.S5_CoinSectionview.backgroundColor = .white
        self.S5_TBL.backgroundColor = .white
        
        self.S5_ProcessBTN.setTitle("Select the Vault", for: .normal)
        self.S5_ProcessBTN.tintColor = .clear
        self.S5_ProcessBTN.backgroundColor = .clear
        self.S5_ProcessBTN.setTitleColor(.white, for: .normal)
        
        self.S5_TBL.register(UINib.init(nibName: "VaultSelectionCell", bundle: nil), forCellReuseIdentifier: "VaultSelectionCell")
        
        self.S5_TBL.delegate = self
        self.S5_TBL.dataSource = self
        self.S5_TBL.reloadData()
    }
    
    @IBAction func S5_TappedBTN(_ sender: Any) {
        if self.Selected_Vaultcoins != nil && self.Selected_betVault != nil {
            self.SetupDPWD_4()
        }
        else if self.Selected_Vaultcoins == nil {
            self.SetupCoinSelection_5(index: 0)
        }
        else {
            self.SetupCoinSelection_5(index: 1)
        }
    }
    
    @IBAction func S5_SegmentBTN(_ sender: UIButton) {
        if sender.tag == 0 {
            self.CS_SelectedSegment = 0
            self.S5_SegmentBTN[0].titleLabel?.font = Font().BoldFont(font: 17)
            self.S5_SegmentBTN[1].titleLabel?.font = Font().LightFont(font: 17)
            self.DF_MainTitleLBL.text = "Select Liquid Vault"
        }
        else {
            self.CS_SelectedSegment = 1
            self.S5_SegmentBTN[1].titleLabel?.font = Font().BoldFont(font: 17)
            self.S5_SegmentBTN[0].titleLabel?.font = Font().LightFont(font: 17)
            self.DF_MainTitleLBL.text = "Select Bets Vault"
        }
        self.S5_TBL.reloadData()
    }
    
}

//MARK:- Step 6 Related Functions
extension Deposit_FundVC {
    
    func SetupAmountView_6() {
        self.DF_MainDismissBTN.tag = 5
        
        self.DF_bgLBL[0].backgroundColor = AppDarkColor
        self.DF_bgLBL[1].backgroundColor = AppDarkColor
        
        self.PinArray = NSMutableArray.init()
        
        self.DF_MainView_Height.constant = Screen_height - 150
        
        self.S6_AmountView.isHidden = false
        self.S1_ShowView.isHidden = true
        self.S2_ShowView.isHidden = true
        self.S3_QRView.isHidden = true
        self.S4_DepositView.isHidden = true
        self.S5_CoinSectionview.isHidden = true
        self.S7_Animationview.isHidden = true
        self.S8_CongratulationView.isHidden = true
        
        if self.FromCome.uppercased() == "FUND".uppercased() {
            self.DF_MainTitleLBL.text = "Enter Amount For Deposit"
            self.isFirstAmount = true
            self.S6_Amount1LBL.font = Font().BoldFont(font: 17)
            self.S6_Amount2LBL.font = Font().LightFont(font: 17)
        }
        else {
            self.DF_MainTitleLBL.text = "Enter Amount For Widthdraw"
            self.isFirstAmount = false
            self.S6_Amount2LBL.font = Font().BoldFont(font: 17)
            self.S6_Amount1LBL.font = Font().LightFont(font: 17)
        }
        
        if let n = NumberFormatter().number(from: self.Topupamount1 as String) {
            let f = Double(truncating: n)
            self.S6_Amount1LBL.text = String.init(format: "%f", f).WithoutCoinPriceThumbRules(Coin: self.Selected_Vaultcoins.coinSymbol)
        }
        
        if let n = NumberFormatter().number(from: self.Topupamount2 as String) {
            let f = Double(truncating: n)
            self.S6_Amount2LBL.text = String.init(format: "%f", f).WithoutCoinPriceThumbRules(Coin: self.GetSelected_betVault_Coin()!)
        }
        
        self.S6_25BTN.setTitle("25%", for: .normal)
        self.S6_50BTN.setTitle("50%", for: .normal)
        self.S6_Symbol1LBL.text = self.Selected_Vaultcoins.coinSymbol
        self.S6_SepratorLBL.backgroundColor = AppDarkColor
        self.S6_Symbol2LBL.text = self.GetSelected_betVault_Coin()
        
        for item in self.S6_DigitsBTN {
            item.isUserInteractionEnabled = true
            item.isEnabled = true
            if item.tag == 100 {
                let longpress = UILongPressGestureRecognizer.init(target: self, action: #selector(Longbackpress))
                item.addGestureRecognizer(longpress)
            }
        }
        
        self.S6_ProcessBTN.setTitle("Complete", for: .normal)
        self.S6_ProcessBTN.tintColor = .clear
        self.S6_ProcessBTN.backgroundColor = .clear
        self.S6_ProcessBTN.setTitleColor(.white, for: .normal)
        self.S6_ProcessBTN.isEnabled = false
    }
    
    @objc func Longbackpress() {
        self.DF_MainDismissBTN.tag = 0
        self.PinArray.removeAllObjects()
        self.S6_Amount1LBL.text = "0.00"
        if let n = NumberFormatter().number(from: self.S6_Amount1LBL.text! as String) {
            let f = Double(truncating: n)
            self.S6_Amount1LBL.text = String.init(format: "%f", f).WithoutCoinPriceThumbRules(Coin: self.Selected_Vaultcoins.coinSymbol)
        }
        
        self.S6_Amount2LBL.text = "0.00"
        if let n = NumberFormatter().number(from: self.S6_Amount2LBL.text! as String) {
            let f = Double(truncating: n)
            self.S6_Amount2LBL.text = String.init(format: "%f", f).WithoutCoinPriceThumbRules(Coin: self.GetSelected_betVault_Coin()!)
        }
    }
    
    func EnterPin(digit: String) {
        self.PinArray.add(digit)
        self.EnteredAmounts()
        if self.PinArray.count != 0 {
            self.S6_ProcessBTN.isEnabled = true
        }
        else {
            self.S6_ProcessBTN.isEnabled = false
        }
    }
    
    func EnteredAmounts() {
        for (index, item) in self.PinArray.enumerated() {
            if self.isFirstAmount {
                if index == 0 {
                    self.S6_Amount1LBL.text = ""
                    self.S6_Amount1LBL.text = String.init(format: "%@", item as! CVarArg)
                }
                else {
                    self.S6_Amount1LBL.text = String.init(format: "%@%@", self.S6_Amount1LBL.text!, item as! CVarArg)
                }
            }
            else {
                if index == 0 {
                    self.S6_Amount2LBL.text = ""
                    self.S6_Amount2LBL.text = String.init(format: "%@", item as! CVarArg)
                }
                else {
                    self.S6_Amount2LBL.text = String.init(format: "%@%@", self.S6_Amount2LBL.text!, item as! CVarArg)
                }
            }
        }
        if self.isFirstAmount {
            if !(self.S6_Amount1LBL.text?.contains("."))! {
                if let n = NumberFormatter().number(from: self.S6_Amount1LBL.text! as String) {
                    let f = Double(truncating: n)
                    self.S6_Amount1LBL.text = String.init(format: "%f", f).WithoutCoinPriceThumbRules(Coin: self.Selected_Vaultcoins.coinSymbol)
                }
            }
            self.Topupamount1 = self.S6_Amount1LBL.text
            self.ConvertForex(Buy: self.GetSelected_betVault_Coin()!, From: self.Selected_Vaultcoins.coinSymbol, Value: self.S6_Amount1LBL.text!)
        }
        else {
            if !(self.S6_Amount2LBL.text?.contains("."))! {
                if let n = NumberFormatter().number(from: self.S6_Amount2LBL.text! as String) {
                    let f = Double(truncating: n)
                    self.S6_Amount2LBL.text = String.init(format: "%f", f).WithoutCoinPriceThumbRules(Coin: self.GetSelected_betVault_Coin()!)
                }
            }
            self.Topupamount2 = self.S6_Amount2LBL.text
            self.ConvertForex(Buy: self.Selected_Vaultcoins.coinSymbol, From: self.GetSelected_betVault_Coin()!, Value: self.S6_Amount2LBL.text!)
        }
    }
    
    func backspace() {
        self.PinArray.removeLastObject()
        if self.PinArray.count == 0 {
            self.S6_Amount1LBL.text = "0.00"
            if let n = NumberFormatter().number(from: self.S6_Amount1LBL.text! as String) {
                let f = Double(truncating: n)
                self.S6_Amount1LBL.text = String.init(format: "%f", f).WithoutCoinPriceThumbRules(Coin: self.Selected_Vaultcoins.coinSymbol)
            }
            
            self.S6_Amount2LBL.text = "0.00"
            if let n = NumberFormatter().number(from: self.S6_Amount2LBL.text! as String) {
                let f = Double(truncating: n)
                self.S6_Amount2LBL.text = String.init(format: "%f", f).WithoutCoinPriceThumbRules(Coin: self.GetSelected_betVault_Coin()!)
            }
        }
        else {
            self.EnteredAmounts()
        }
    }
    
    @IBAction func S6_TappedBTN(_ sender: UIButton) {
        self.DP_Process_Tag = 300
        self.S4_ProcessBTN.tag = self.DP_Process_Tag
        
        self.S4_ProcessBTN.setTitleColor(.white, for: .normal)
        self.isEnableforAmount = true
        self.SetupDPWD_4()
    }
    
    @IBAction func S6_DigitsBTN(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 2:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 3:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 4:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 5:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 6:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 7:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 8:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 9:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 100:
            self.backspace()
            break
            
        case 0:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 200:
            if !self.PinArray.contains(".") {
                if self.PinArray.count == 0 {
                    self.EnterPin(digit: String.init(format: "0"))
                    self.EnterPin(digit: String.init(format: "."))
                }
                else {
                    self.EnterPin(digit: String.init(format: "."))
                }
            }
            break
            
        default:
            break
        }
    }
    
    @IBAction func S6_Active1LBL(_ sender: Any) {
        if self.FromCome.uppercased() == "FUND".uppercased() {
            self.isFirstAmount = true
            self.S6_Amount1LBL.font = Font().BoldFont(font: 17)
            self.S6_Amount2LBL.font = Font().LightFont(font: 17)
            self.Longbackpress()
        }
        else {
            
        }
    }
    
    @IBAction func S6_Active2LBL(_ sender: Any) {
        if self.FromCome.uppercased() == "FUND".uppercased() {
            self.isFirstAmount = false
            self.S6_Amount2LBL.font = Font().BoldFont(font: 17)
            self.S6_Amount1LBL.font = Font().LightFont(font: 17)
            self.Longbackpress()
        }
        else {
            
        }
    }
    
    @IBAction func S6_25BTN(_ sender: Any) {
        if self.FromCome.uppercased() == "FUND".uppercased() {
            let amount: Double = (self.Selected_Vaultcoins.coinValue * 25) / 100
            self.Topupamount1 = String.init(format: "%f", amount).ConvertIntoUSDFormat()
            self.S6_Amount1LBL.text = self.Topupamount1
            self.ConvertForex(Buy: self.GetSelected_betVault_Coin()!, From: self.Selected_Vaultcoins.coinSymbol, Value: self.S6_Amount1LBL.text!)
        }
        else {
            
        }
    }
    
    @IBAction func S6_50BTN(_ sender: Any) {
        if self.FromCome.uppercased() == "FUND".uppercased() {
            let amount = (self.Selected_Vaultcoins.coinValue * 50) / 100
            self.Topupamount1 = String.init(format: "%f", amount).ConvertIntoUSDFormat()
            self.S6_Amount1LBL.text = self.Topupamount1
            self.ConvertForex(Buy: self.GetSelected_betVault_Coin()!, From: self.Selected_Vaultcoins.coinSymbol, Value: self.S6_Amount1LBL.text!)
        }
        else {
            
        }
    }
    
}

//MARK:- Step 7 Related Functions
extension Deposit_FundVC {
    
    func SetupAnimation_7() {
        self.DF_MainDismissBTN.tag = -2
        
        self.DF_bgLBL[0].backgroundColor = AppDarkColor
        self.DF_bgLBL[1].backgroundColor = AppDarkColor
        
        self.DF_MainTitleLBL.text = ""
        
        self.S7_Animationview.isHidden = false
        self.S1_ShowView.isHidden = true
        self.S2_ShowView.isHidden = true
        self.S3_QRView.isHidden = true
        self.S4_DepositView.isHidden = true
        self.S5_CoinSectionview.isHidden = true
        self.S6_AmountView.isHidden = true
        self.S8_CongratulationView.isHidden = true
        
        let lottie = AnimationView(name: "wallet-coin")
        lottie.contentMode = .scaleAspectFit
        lottie.clipsToBounds = true
        lottie.loopMode = .loop
        self.S7_Animation.addSubview(lottie)
        self.S7_Animation.backgroundColor = .clear
        lottie.translatesAutoresizingMaskIntoConstraints = false
        lottie.centerXAnchor.constraint(equalTo: self.S7_Animation.centerXAnchor, constant: 0).isActive = true
        lottie.centerYAnchor.constraint(equalTo: self.S7_Animation.centerYAnchor, constant: 0).isActive = true
        lottie.leadingAnchor.constraint(equalTo: self.S7_Animation.leadingAnchor, constant: 0).isActive = true
        lottie.trailingAnchor.constraint(equalTo: self.S7_Animation.trailingAnchor, constant: 0).isActive = true
        lottie.heightAnchor.constraint(equalToConstant: self.S7_Animation.frame.size.height).isActive = true
        lottie.widthAnchor.constraint(equalToConstant: self.S7_Animation.frame.size.width).isActive = true
        lottie.play()
    }
    
}

//MARK:- Step 8 Related Functions
extension Deposit_FundVC {
    
    func SetupCongrestView_8() {
        self.DF_MainDismissBTN.tag = 0
        
        self.DF_bgLBL[0].backgroundColor = AppDarkColor
        self.DF_bgLBL[1].backgroundColor = AppDarkColor
        
        self.DF_MainTitleLBL.text = ""
        
        self.S8_CongratulationView.isHidden = false
        self.S1_ShowView.isHidden = true
        self.S2_ShowView.isHidden = true
        self.S3_QRView.isHidden = true
        self.S4_DepositView.isHidden = true
        self.S5_CoinSectionview.isHidden = true
        self.S6_AmountView.isHidden = true
        self.S7_Animationview.isHidden = true
        
        self.S8_CongretCoinName.theme_textColor = GlobalPicker.textColor
        self.S8_CongretCoinDetail.theme_textColor = GlobalPicker.notesColor
        
        self.S8_Collection.register(UINib.init(nibName: "FDOptionCell", bundle: nil), forCellWithReuseIdentifier: "FDOptionCell")
        self.S8_Collection.translatesAutoresizingMaskIntoConstraints = false
        self.S8_Collection.delegate = self
        self.S8_Collection.dataSource = self
        
        self.S8_CloseBTN.setTitle("Close", for: .normal)
        self.S8_CloseBTN.tintColor = .clear
        self.S8_CloseBTN.backgroundColor = .clear
        self.S8_CloseBTN.setTitleColor(.white, for: .normal)
    }
    
    @IBAction func S8_TappedBTN(_ sender: Any) {
        self.dismiss(animated: true) {
            self.DismissCallBack()
        }
    }
    
}

//MARK:- TableView Configs
extension Deposit_FundVC {
    
    // TODO:- S1_Table Config Methods
    func S1_TableviewCell(indexPath: IndexPath) -> VaultSelectionCell {
        let cell: VaultSelectionCell = self.S1_TBL.dequeueReusableCell(withIdentifier: "VaultSelectionCell") as! VaultSelectionCell
        if self.S1_SelectedSegment == 0 {
            let dataobj = self.SectionCoin.filter { (sections) -> Bool in
                sections.sectionkey.uppercased() == "Crypto".uppercased()
            }.first
            let data = dataobj?.sectionArray[indexPath.row]
            cell.IMGCoin.downloadedFrom(link: data!.coinImage, contentMode: .scaleAspectFit, radious: (cell.IMGCoin.frame.height / 2))
            cell.CoinName.text = data!.coinName
            cell.CoinDetail.text = String.init(format: "%f", (data?.coinValue)!).CoinPriceThumbRules(Coin: data!.coinSymbol.uppercased())
            
            if self.S1_CoinsVault == nil {
                cell.CoinName.font = UIFont.systemFont(ofSize: 15)
                cell.CoinDetail.font = UIFont.systemFont(ofSize: 15)
            }
            else {
                if self.S1_CoinsVault.coinName == data!.coinName {
                    cell.CoinName.font = UIFont.boldSystemFont(ofSize: 15)
                    cell.CoinDetail.font = UIFont.boldSystemFont(ofSize: 15)
                }
                else {
                    cell.CoinName.font = UIFont.systemFont(ofSize: 15)
                    cell.CoinDetail.font = UIFont.systemFont(ofSize: 15)
                }
            }
        }
        else {
            let dataobj = self.SectionCoin.filter { (sections) -> Bool in
                sections.sectionkey.uppercased() == "fiat".uppercased()
            }.first
            let data = dataobj?.sectionArray[indexPath.row]
            cell.IMGCoin.downloadedFrom(link: data?.coinImage, contentMode: .scaleAspectFit, radious: (cell.IMGCoin.frame.height / 2))
            cell.CoinName.text = data?.coinSymbol.uppercased()
            cell.CoinDetail.text = String.init(format: "%f", (data?.coinValue)!).CoinPriceThumbRules(Coin: data!.coinSymbol.uppercased())
            
            if self.S1_CoinsVault == nil {
                cell.CoinName.font = UIFont.systemFont(ofSize: 15)
                cell.CoinDetail.font = UIFont.systemFont(ofSize: 15)
            }
            else {
                if self.S1_CoinsVault.coinSymbol == data!.coinSymbol {
                    cell.CoinName.font = UIFont.boldSystemFont(ofSize: 15)
                    cell.CoinDetail.font = UIFont.boldSystemFont(ofSize: 15)
                }
                else {
                    cell.CoinName.font = UIFont.systemFont(ofSize: 15)
                    cell.CoinDetail.font = UIFont.systemFont(ofSize: 15)
                }
            }
        }
        return cell
    }
    
    //    TODO:- S2 Tableview Config Methods
    func S2_TableviewCell(indexPath: IndexPath) -> FundingCell {
        let cell: FundingCell = self.S2_TBL.dequeueReusableCell(withIdentifier: "FundingCell") as! FundingCell
        cell.Icon.isSkeletonable = true
        cell.Name.isSkeletonable = true
        cell.MainIMG.isSkeletonable = true
        if indexPath.row == 0 {
            cell.Icon.downloadedFrom(link: self.S1_CoinsVault.coinImage ?? "", contentMode: .scaleAspectFit, radious: (cell.Icon.frame.height / 2))
            cell.Name.text = self.S1_CoinsVault.coinSymbol.uppercased()
            cell.Icon.isHidden = false
            cell.Name.isHidden = false
            cell.MainIMG.isHidden = true
            if self.S1_CoinsVault.coinSymbol.uppercased() == "USD" || self.S1_CoinsVault.coinSymbol.uppercased() == "BTC" {
                cell.isEnableCell(status: true)
            }
            else {
                cell.isEnableCell(status: false)
            }
        }
        else if indexPath.row == 1 {
            cell.enableMode = .enabled
            cell.MainIMG.image = UIImage.init(named: "VaultDeposit")
            cell.Icon.isHidden = true
            cell.Name.isHidden = true
            cell.MainIMG.isHidden = false
            cell.isEnableCell(status: true)
        }
        else {
            cell.enableMode = .disabled
            cell.MainIMG.image = UIImage.init(named: "snapay")
            cell.Icon.isHidden = true
            cell.Name.isHidden = true
            cell.MainIMG.isHidden = false
            cell.isEnableCell(status: false)
        }
        return cell
    }
    
    //    TODO:- S4 Tableview Config Methods
    func UpdateLiquidValut(index: Int, cell: VaultCoinCell) {
        if self.isEnableforAmount {
            cell.Seprator.isHidden = false
            cell.CoinSTitle.text = String.init(format: "Your Liquid %@ Vault Will Be Debited", self.Selected_Vaultcoins.coinName)
            cell.IMGCoin.downloadedFrom(link: self.Selected_Vaultcoins.coinImage, contentMode: .scaleAspectFit, radious: (cell.IMGCoin.frame.height / 2))
            cell.CoinName.text = self.Selected_Vaultcoins.coinName
            if let n = NumberFormatter().number(from: self.Topupamount1 as String) {
                let f = Double(truncating: n)
                cell.CoinDetail.text = String.init(format: "%f", f).CoinPriceThumbRules(Coin: self.Selected_Vaultcoins.symbol)
            }
        }
        else {
            cell.Seprator.isHidden = true
            cell.CoinSTitle.text = "From Liquid Vault"
            cell.IMGCoin.downloadedFrom(link: self.Selected_Vaultcoins.coinImage, contentMode: .scaleAspectFit, radious: (cell.IMGCoin.frame.height / 2))
            cell.CoinName.text = self.Selected_Vaultcoins.coinName
            cell.CoinDetail.text = String.init(format: "%f", self.Selected_Vaultcoins.coinValue).CoinPriceThumbRules(Coin: self.Selected_Vaultcoins.symbol)
        }
    }
    
    func UpdateBetValut(index: Int, cell: VaultCoinCell, indexpath: IndexPath) {
        if self.isEnableforAmount {
            cell.Seprator.isHidden = false
            if self.FromCome.uppercased() == "FUND".uppercased() {
                cell.CoinSTitle.text = String.init(format: "Your Bets %@ Vault Will Be Cedited", self.GetSelected_betVault_Coin()!)
            }
            else {
                cell.CoinSTitle.text = String.init(format: "Your Bets %@ Vault Will Be Debited", self.GetSelected_betVault_Coin()!)
            }
            cell.IMGCoin.downloadedFrom(link: self.Selected_betVault.imageUrl, contentMode: .scaleAspectFit, radious: (cell.IMGCoin.frame.height / 2))
            cell.CoinName.text = self.GetSelected_betVault_Coin()!
            if let n = NumberFormatter().number(from: self.Topupamount2 as String) {
                let f = Double(truncating: n)
                cell.CoinDetail.text = String.init(format: "%f", f).CoinPriceThumbRules(Coin: self.GetSelected_betVault_Coin()!)
            }
        }
        else {
            cell.Seprator.isHidden = true
            cell.CoinSTitle.text = "To Bets Vault"
            cell.IMGCoin.downloadedFrom(link: self.Selected_betVault.imageUrl, contentMode: .scaleAspectFit, radious: (cell.IMGCoin.frame.height / 2))
            cell.CoinName.text = self.GetSelected_betVault_Coin()!
            cell.CoinDetail.text = String.init(format: "%f", self.Selected_betVault.liveBalance.nativeField).CoinPriceThumbRules(Coin: self.GetSelected_betVault_Coin()!)
        }
    }
    
    func SetupTBlHeader() -> UIView {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: Screen_width, height: Screen_height))
        view.theme_backgroundColor = GlobalPicker.backgroundColor
        return view
    }
    
    //    TODO:- S5 Tableview Config Methods
    func ConfigCoinSymbol(symbol: String) -> String {
        let array = symbol.components(separatedBy: ",")
        if array.count == 0 {
            return symbol
        }
        else {
            return (array.first?.removeWhiteSpace())!
        }
    }
    
    func Config_S5Cell(cell: VaultSelectionCell, indexPath: IndexPath) {
        if self.CS_SelectedSegment == 0 {
            let data = self.CoinsVault[indexPath.row]
//            let fiat = self.SectionCoin.filter { (sections) -> Bool in
//                sections.sectionkey.uppercased() == "crypto".uppercased()
//            }.first
//            let data = fiat?.sectionArray[indexPath.row]
            cell.IMGCoin.downloadedFrom(link: data.coinImage, contentMode: .scaleAspectFit, radious: (cell.IMGCoin.frame.height / 2))
            cell.CoinName.text = data.coinName
            
            cell.CoinDetail.text = String.init(format: "%@ %0.2f", self.ConfigCoinSymbol(symbol: data.symbol), data.coinValue ?? 0.0)
            let coin = self.Selected_Vaultcoins == nil ? "" : self.Selected_Vaultcoins.coinName
            if coin == data.coinName {
                cell.CoinName.font = UIFont.boldSystemFont(ofSize: 15)
                cell.CoinDetail.font = UIFont.boldSystemFont(ofSize: 15)
            }
            else {
                cell.CoinName.font = UIFont.systemFont(ofSize: 15)
                cell.CoinDetail.font = UIFont.systemFont(ofSize: 15)
            }
        }
        else {
            let data = self.BetVault[indexPath.row]
//            let fiat = self.SectionCoin.filter { (sections) -> Bool in
//                sections.sectionkey.uppercased() == "fiat".uppercased()
//            }.first
//            let data = fiat?.sectionArray[indexPath.row]
            cell.IMGCoin.downloadedFrom(link: data.imageUrl, contentMode: .scaleAspectFit, radious: (cell.IMGCoin.frame.height / 2))
            cell.CoinName.text = data.coin.uppercased()
            cell.CoinDetail.text = String.init(format: "%@ %0.00", data.coin.uppercased())
            let coin = self.Selected_betVault == nil ? "" : self.Selected_betVault.coin
            if coin == data.coin {
                cell.CoinName.font = UIFont.boldSystemFont(ofSize: 15)
                cell.CoinDetail.font = UIFont.boldSystemFont(ofSize: 15)
            }
            else {
                cell.CoinName.font = UIFont.systemFont(ofSize: 15)
                cell.CoinDetail.font = UIFont.systemFont(ofSize: 15)
            }
        }
    }
    
    
}

//MARK:- Tableview Delegate
extension Deposit_FundVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == self.S4_TBL {
            if self.Selected_Vaultcoins == nil && self.Selected_betVault == nil {
                return 0
            }
            else if self.Selected_Vaultcoins != nil && self.Selected_betVault != nil {
                return 2
            }
            else if self.Selected_Vaultcoins != nil {
                return 1
            }
            return 0
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.S1_TBL {
            if self.S1_SelectedSegment == 0 {
                let data = self.SectionCoin.filter { (sections) -> Bool in
                    sections.sectionkey.uppercased() == "Crypto".uppercased()
                }.first
                return (data?.sectionArray.count)!
            }
            else {
                let data = self.SectionCoin.filter { (sections) -> Bool in
                    sections.sectionkey.uppercased() == "fiat".uppercased()
                }.first
                return (data?.sectionArray.count)!
            }
        }
        else if tableView == self.S2_TBL{
            return 3
        }
        else if tableView == self.S4_TBL {
            return 1
        }
        else {
            if self.CS_SelectedSegment == 0 {
//                let data = self.SectionCoin.filter { (sections) -> Bool in
//                    sections.sectionkey.uppercased() == "Crypto".uppercased()
//                }.first
//                return (data?.sectionArray.count)!
                return self.CoinsVault.count
            }
            else {
//                let data = self.SectionCoin.filter { (sections) -> Bool in
//                    sections.sectionkey.uppercased() == "fiat".uppercased()
//                }.first
//                return (data?.sectionArray.count)!
                return self.BetVault == nil ? 0 : self.BetVault.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.S1_TBL {
            return self.S1_TableviewCell(indexPath: indexPath)
        }
        else if tableView == self.S2_TBL {
            return self.S2_TableviewCell(indexPath: indexPath)
        }
        else if tableView == self.S4_TBL {
            let cell: VaultCoinCell = tableView.dequeueReusableCell(withIdentifier: "VaultCoinCell") as! VaultCoinCell
            if indexPath.section == 0 {
                self.UpdateLiquidValut(index: 0, cell: cell)
            }
            else {
                self.UpdateBetValut(index: 0, cell: cell, indexpath: indexPath)
            }
            return cell
        }
        else {
            let cell: VaultSelectionCell = tableView.dequeueReusableCell(withIdentifier: "VaultSelectionCell") as! VaultSelectionCell
            self.Config_S5Cell(cell: cell, indexPath: indexPath)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == self.S4_TBL {
            return section == 0 ? 0 : 30
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == self.S4_TBL {
            return section == 0 ? nil : self.SetupTBlHeader()
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.S1_TBL {
            if self.S1_SelectedSegment == 0 {
                let data = self.SectionCoin.filter { (sections) -> Bool in
                    sections.sectionkey.uppercased() == "Crypto".uppercased()
                }.first
                self.S1_CoinsVault = data?.sectionArray[indexPath.row]
            }
            else {
                let data = self.SectionCoin.filter { (sections) -> Bool in
                    sections.sectionkey.uppercased() == "fiat".uppercased()
                }.first
                self.S1_CoinsVault = data?.sectionArray[indexPath.row]
            }
            var title = "Proceed To Fund Your COIN Bets Vault"
            self.S1_ProcessBTN.setTitle(title.replaceLocalized(fromvalue: ["COIN"], tovalue: [self.S1_CoinsVault.coinName]), for: .normal)
            self.S1_ProcessBTN.isUserInteractionEnabled = true
            self.S1_ProcessBTN.alpha = 1
            self.S1_TBL.reloadData()
        }
        else if tableView == self.S2_TBL {
            self.S2_SelectedPayIndex = indexPath.row
            var coin: String = ""
            if indexPath.row == 0 {
                coin = self.S1_CoinsVault.coinSymbol
                self.S2_ProcessBTN.setTitle("Proceed To Fund With " + coin + " Address", for: .normal)
            }
            else if indexPath.row == 1 {
                coin = "Vault"
                self.S2_ProcessBTN.setTitle("Proceed To Fund With " + coin, for: .normal)
            }
            else {
                coin = "Snapay"
                self.S2_ProcessBTN.setTitle("Proceed To Fund With " + coin, for: .normal)
            }
            self.S2_TBL.isUserInteractionEnabled = false
            self.S2_ProcessBTN.isUserInteractionEnabled = true
            self.S2_ProcessBTN.alpha = 1
            self.GetCoinAddressCode(Coin: coin) { (status, address) in
                if status {
                    self.FinalCoinAddress = address
                }
                else {
                    self.FinalCoinAddress = ""
                }
                self.S2_TBL.isUserInteractionEnabled = true
            }
        }
        else if tableView == self.S4_TBL {
            if self.isEnableforAmount {
                self.SetupAmountView_6()
            }
            else {
                self.SetupCoinSelection_5(index: indexPath.section)
            }
        }
        else {
            if self.CS_SelectedSegment == 0 {
                self.Selected_Vaultcoins = self.CoinsVault[indexPath.row]
                let str = String.init(format: "Select %@ Liquid Vault", self.Selected_Vaultcoins.coinName)
                self.S5_ProcessBTN.setTitle(str, for: .normal)
            }
            else {
                self.Selected_betVault = self.BetVault[indexPath.row]
                let str = String.init(format: "Select %@ Bets Vault", self.GetSelected_betVault_Coin()!)
                self.S5_ProcessBTN.setTitle(str, for: .normal)
            }
            self.S5_TBL.reloadData()
        }
    }
    
}

extension Deposit_FundVC: UICollectionViewDelegate,UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FDOptionCell",for: indexPath) as? FDOptionCell else { fatalError() }
        
        cell.NameLBL.text = "Explore Lotteries"
        if indexPath.row == 0 {
            cell.CardIMG.image = UIImage.init(named: "Tab1")
        }
        else if indexPath.row == 1 {
            cell.CardIMG.image = UIImage.init(named: "BetsGraph")
        }
        else {
            cell.CardIMG.image = UIImage.init(named: "Firebets")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            self.dismiss(animated: true) {
                self.DismissCallBack()
            }
        }
        else if indexPath.row == 1 {
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: .ReloadFeed, object: nil, userInfo: ["Tab": 2])
            }
        }
        else {
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 2.5, height: collectionView.frame.height)
    }
    
}
