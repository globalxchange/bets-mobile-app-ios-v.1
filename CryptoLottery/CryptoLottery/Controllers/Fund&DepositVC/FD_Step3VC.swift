//
//  FD_Step3VC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-06-24.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import CRNotifications

class FD_Step3VC: BaseVC {
    
    // MARK:- IBOutlets
    @IBOutlet weak var DF_MainView: CustomView!
    @IBOutlet var DF_bgLBL: [UILabel]!
    
    @IBOutlet weak var DF_MainView_Height: NSLayoutConstraint!
    @IBOutlet weak var DF_NautchLBL: CustomLBL!
    @IBOutlet weak var DF_LogoIMG: UIImageView!
    @IBOutlet weak var DF_MainTitleLBL: UILabel!
    @IBOutlet weak var DF_MainDismissBTN: UIButton!
    
    // TODO:- Step 3 QRcode Methods
    @IBOutlet weak var S3_QRView: UIView!
    @IBOutlet weak var S3_QRimage: UIImageView!
    @IBOutlet weak var S3_CodeView: UIView!
    @IBOutlet weak var S3_Codesview: UIView!
    @IBOutlet weak var S3_QRCodelbl: UILabel!
    @IBOutlet weak var S3_QRCopyBTN: UIButton!
    @IBOutlet var AllBottomview: UIView!
    @IBOutlet weak var S3_ProcessBTN: UIButton!
    
    var FromCome: String!
    var FinalCoinAddress: String = ""
    
    var S1_CoinsVault: BetsVaultVault!
    
    // MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.DF_MainView.backgroundColor = .white
        self.DF_MainView.BGColor = .white
        self.DF_LogoIMG.tintColor = AppDarkColor
        self.DF_MainTitleLBL.theme_textColor = GlobalPicker.textColor
        
        self.AllBottomview.backgroundColor = AppDarkColor
        
        self.cornerColor()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.SetupStep_3()
        
        self.S3_QRimage.isSkeletonable = true
        self.S3_Codesview.isSkeletonable = true
        
        self.S3_QRimage.showGradientSkeleton()
        self.S3_Codesview.showGradientSkeleton()
        
        self.S3_QRCopyBTN.isUserInteractionEnabled = false
        self.S3_QRCopyBTN.alpha = 0.5
        
        self.S3_ProcessBTN.isUserInteractionEnabled = false
        self.S3_ProcessBTN.alpha = 0.5
        
        self.GetCoinAddressCode(Coin: self.S1_CoinsVault.coin) { (status, address) in
            if status {
                self.FinalCoinAddress = address
            }
            else {
                self.FinalCoinAddress = ""
            }
            self.GenerateQRcode(QRValue: self.FinalCoinAddress)
            self.S3_QRCodelbl.text = self.FinalCoinAddress
            
            self.S3_QRimage.hideSkeleton()
            self.S3_Codesview.hideSkeleton()
            
            self.S3_QRCopyBTN.isUserInteractionEnabled = true
            self.S3_QRCopyBTN.alpha = 1
            
            self.S3_ProcessBTN.isUserInteractionEnabled = true
            self.S3_ProcessBTN.alpha = 1
        }
        
    }

    // MARK:- API Calling
    func GetCoinAddressCode(Coin: String, CodeSuccess: ((_ status: Bool, _ Address: String)->Void)?) {
        background {
            let param = VaultAddressParamDict.init(arcade_userid: HeaderSetter().ArcadeUserid)
            let header = CommanHeader.init(email: HeaderSetter().email, token: HeaderSetter().token)
            NetworkingRequests.shared.requestPOST(API_GetVault_Address, Parameters: param.description, Headers: header.description, onSuccess: { (responseObject, status) in
                main {
                    guard let body = responseObject as? [String: Any] else {
                        return
                    }
                    if status {
                        if (body["status"] != nil) {
                            if body.keys.contains("address") {
                                let obj = body[Coin.uppercased()] as! [String: Any]
                                self.FinalCoinAddress = obj["address"] as! String
                                CodeSuccess!(status, self.FinalCoinAddress)
                            }
                            else if body.keys.contains("payload") {
                                let obj = body[Coin.uppercased()] as! [String: Any]
                                self.FinalCoinAddress = obj["address"] as! String
                                CodeSuccess!(status, self.FinalCoinAddress)
                            }
                            else {
                                CodeSuccess!(status, "")
                            }
                        }
                        else {
                            CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: body["payload"] as! String, dismissDelay: 3, completion: {
                                
                            })
                            CodeSuccess!(status, "")
                        }
                    }
                    else {
                        CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: body["payload"] as! String, dismissDelay: 3, completion: {
                            
                        })
                        CodeSuccess!(status, "")
                    }
                }
            }) { (Status, code) in
                main {
                    CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Status, dismissDelay: 3, completion: {
                        
                    })
                    CodeSuccess!(false, "")
                }
            }
        }
    }
    
    // MARK:- User Define Methods
    func cornerColor(color: UIColor = AppDarkColor) {
        for lbl in self.DF_bgLBL {
            lbl.backgroundColor = color
        }
    }

    func SetupStep_3() {
        
        var title = "Here Is Your COIN Address"
        self.DF_MainTitleLBL.text = title.replaceLocalized(fromvalue: ["COIN"], tovalue: [self.S1_CoinsVault.coin])
        
        self.S3_QRCodelbl.textColor = AppDarkColor
        
        self.S3_CodeView.clipsToBounds = true
        self.S3_CodeView.layer.borderColor = AppDarkColor.withAlphaComponent(0.2).cgColor
        self.S3_CodeView.layer.borderWidth = 1.0
        self.S3_CodeView.layer.cornerRadius = 3
        
        self.S3_QRCopyBTN.clipsToBounds = true
        self.S3_QRCopyBTN.layer.borderColor = AppDarkColor.withAlphaComponent(0.2).cgColor
        self.S3_QRCopyBTN.layer.borderWidth = 1.0
        self.S3_QRCopyBTN.layer.cornerRadius = 3
        
        self.S3_QRView.backgroundColor = .white
        
        self.S3_ProcessBTN.setTitle("Share", for: .normal)
        self.S3_ProcessBTN.tintColor = .clear
        self.S3_ProcessBTN.backgroundColor = .clear
        self.S3_ProcessBTN.setTitleColor(.white, for: .normal)
        self.S3_ProcessBTN.setTitleColor(.white, for: .selected)
    }
    
    func GenerateQRcode(QRValue: String) {
        self.FinalCoinAddress = QRValue
        self.S3_QRCodelbl.text = QRValue
        // Get data from the string
        let data = QRValue.data(using: String.Encoding.ascii)
        // Get a QR CIFilter
        guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else { return }
        // Input the data
        qrFilter.setValue(data, forKey: "inputMessage")
        // Get the output image
        guard let qrImage = qrFilter.outputImage else { return }
        // Scale the image
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let scaledQrImage = qrImage.transformed(by: transform)
        // Do some processing to get the UIImage
        let context = CIContext()
        guard let cgImage = context.createCGImage(scaledQrImage, from: scaledQrImage.extent) else { return }
        self.S3_QRimage.image = UIImage(cgImage: cgImage)
    }
    
    func ShareQRCode(QRValue: String) {
        //        let secondActivityItem : NSURL = NSURL(string: "http//:urlyouwant")!
        // If you want to put an image
        let image : UIImage = self.S3_QRimage.image!
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [QRValue, image], applicationActivities: nil)
        
        // This lines is for the popover you need to show in iPad
        activityViewController.popoverPresentationController?.sourceView = self.S3_ProcessBTN
        
        // This line remove the arrow of the popover to show in iPad
        activityViewController.popoverPresentationController?.permittedArrowDirections = .any
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.postToFacebook
            ,UIActivity.ActivityType.postToTwitter
            ,UIActivity.ActivityType.postToWeibo
            ,UIActivity.ActivityType.message
            ,UIActivity.ActivityType.mail
            ,UIActivity.ActivityType.print
            ,UIActivity.ActivityType.copyToPasteboard
            ,UIActivity.ActivityType.assignToContact
            ,UIActivity.ActivityType.saveToCameraRoll
            ,UIActivity.ActivityType.addToReadingList
            ,UIActivity.ActivityType.postToFlickr
            ,UIActivity.ActivityType.postToVimeo
            ,UIActivity.ActivityType.postToTencentWeibo
            ,UIActivity.ActivityType.airDrop
            ,UIActivity.ActivityType.openInIBooks
            ,UIActivity.ActivityType.markupAsPDF
        ]
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    // MARK:- IBAction Methods
    
    @IBAction func S3_TappedBTN(_ sender: UIButton) {
        if sender == self.S3_QRCopyBTN {
            if self.FinalCoinAddress.count > 0 {
                let pasteboard = UIPasteboard.general
                pasteboard.string = self.FinalCoinAddress
                if let string = pasteboard.string {
                    print(string)
                    self.S3_QRCodelbl.text = "Copied To Clipboard"
                    self.DF_MainDismissBTN.tag = 0
                    self.S3_ProcessBTN.setTitleColor(.white, for: .selected)
                    self.S3_ProcessBTN.backgroundColor = .clear
                    self.S3_ProcessBTN.setTitle("Close", for: .normal)
                    self.S3_ProcessBTN.isSelected = true
                }
            }
        }
        else {
            if self.S3_ProcessBTN.isSelected {
                App?.SetDashboardVC(index: 2)
            }
            else {
                self.ShareQRCode(QRValue: self.FinalCoinAddress)
            }
        }
    }
    
    @IBAction func TappedDF_MainDismissBTN(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    

}
