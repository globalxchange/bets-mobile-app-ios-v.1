//
//  FD_Step6VC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-06-24.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class FD_Step6VC: BaseVC {
    
    // MARK:- IBOutlets
    @IBOutlet weak var DF_MainView: CustomView!
    @IBOutlet var DF_bgLBL: [UILabel]!
    
    @IBOutlet weak var DF_MainView_Height: NSLayoutConstraint!
    @IBOutlet weak var DF_NautchLBL: CustomLBL!
    @IBOutlet weak var DF_LogoIMG: UIImageView!
    @IBOutlet weak var DF_MainTitleLBL: UILabel!
    @IBOutlet weak var DF_MainDismissBTN: UIButton!
    
    // TODO:- Step 6 Amount Enter View
    @IBOutlet weak var S6_AmountView: UIView!
    @IBOutlet weak var S6_Amount1LBL: UILabel!
    @IBOutlet weak var S6_25BTN: CustomBTN!
    @IBOutlet weak var S6_50BTN: CustomBTN!
    @IBOutlet weak var S6_Symbol1LBL: UILabel!
    @IBOutlet weak var S6_SepratorLBL: UILabel!
    @IBOutlet weak var S6_Amount2LBL: UILabel!
    @IBOutlet weak var S6_Symbol2LBL: UILabel!
    @IBOutlet weak var S6_DigitView: UIView!
    @IBOutlet var S6_DigitsBTN: [UIButton]!
    @IBOutlet var AllBottomview: UIView!
    @IBOutlet weak var S6_ProcessBTN: UIButton!
    
    // TODO:- Step 6 Variables
    var Topupamount1: Double = 0.00
    var Topupamount2: Double = 0.00
    var PinArray: NSMutableArray!
    var isFirstAmount: Bool = true
    
    var FromCome: String!
    var S1_CoinsVault: BetsVaultVault!
    var S4_CoinsVault: CoinVaultCoin!
    var SectionCoin = [CoinSectionArray]()
    var BetsVaultLoad: BetsVaultPayload!
    var S4_VaultType: String!
    var S1_VaultType: String!
    var DP_Process_Tag: Int = 300
    
    // MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.DF_MainView.backgroundColor = .white
        self.DF_MainView.BGColor = .white
        self.DF_LogoIMG.tintColor = AppDarkColor
        self.DF_MainTitleLBL.theme_textColor = GlobalPicker.textColor
        
        self.AllBottomview.backgroundColor = AppDarkColor
        self.SetupAmountView_6()
        self.cornerColor()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK:- API Calling
    func ConvertForex(Buy: String, From: String, Value: String) {
        self.S6_ProcessBTN.isUserInteractionEnabled = false
        self.S6_ProcessBTN.alpha = 0.5
        CommanApiCall().ForexConversion(Buy: Buy, From: From) { (keyvalue1, keyvalue2) in
            if self.isFirstAmount {
                if let n = NumberFormatter().number(from: Value) {
                    let cgvalue = Double(truncating: n)
                    let convert = cgvalue * Double(keyvalue2)
                    self.S6_Amount1LBL.text! = String.init(format: "%f", cgvalue).WithoutCoinPriceThumbRules(Coin: self.S1_CoinsVault.coin)!
                    self.Topupamount1 = cgvalue
                    self.S6_Amount2LBL.text! = String.init(format: "%f", convert).WithoutCoinPriceThumbRules(Coin: self.S4_CoinsVault.coinSymbol)!
                    self.Topupamount2 = convert
                }
            }
            else {
                if let n = NumberFormatter().number(from: Value) {
                    let cgvalue = Double(truncating: n)
                    let convert = cgvalue * Double(keyvalue1)
                    self.S6_Amount2LBL.text! = String.init(format: "%f", cgvalue).WithoutCoinPriceThumbRules(Coin: self.S1_CoinsVault.coin)!
                    self.Topupamount2 = cgvalue
                    self.S6_Amount1LBL.text! = String.init(format: "%f", convert).WithoutCoinPriceThumbRules(Coin: self.S4_CoinsVault.coinSymbol)!
                    self.Topupamount1 = convert
                }
            }
            self.S6_ProcessBTN.isUserInteractionEnabled = true
            self.S6_ProcessBTN.alpha = 1.0
        }
    }
    
    // MARK:- User Define Methods
    func cornerColor(color: UIColor = AppDarkColor) {
        for lbl in self.DF_bgLBL {
            lbl.backgroundColor = color
        }
    }

    func SetupAmountView_6() {
        self.PinArray = NSMutableArray.init()
        
        self.DF_MainView_Height.constant = Screen_height - 100
        
        self.S6_ProcessBTN.isUserInteractionEnabled = false
        self.S6_ProcessBTN.alpha = 0.5
        
        if self.FromCome.uppercased() == "FUND".uppercased() {
            self.DF_MainTitleLBL.text = "Enter Amount For Deposit"
            self.isFirstAmount = true
            self.S6_Amount1LBL.font = Font().MontserratBoldFont(font: 17)
            self.S6_Amount2LBL.font = Font().MontserratLightFont(font: 17)
        }
        else {
            self.DF_MainTitleLBL.text = "Enter Amount For Widthdraw"
            self.isFirstAmount = false
            self.S6_Amount2LBL.font = Font().MontserratBoldFont(font: 17)
            self.S6_Amount1LBL.font = Font().MontserratLightFont(font: 17)
        }
        
        self.S6_Amount1LBL.text = String.init(format: "%f", self.Topupamount1).WithoutCoinPriceThumbRules(Coin: self.S1_CoinsVault.coin)
    
        self.S6_Amount2LBL.text = String.init(format: "%f", self.Topupamount2).WithoutCoinPriceThumbRules(Coin: self.S4_CoinsVault.coinSymbol)
        
        self.S6_25BTN.setTitle("25%", for: .normal)
        self.S6_50BTN.setTitle("50%", for: .normal)
        self.S6_Symbol1LBL.text = self.S1_CoinsVault.coin
        self.S6_SepratorLBL.backgroundColor = AppDarkColor
        self.S6_Symbol2LBL.text = self.S4_CoinsVault.coinSymbol
        
        for item in self.S6_DigitsBTN {
            item.isUserInteractionEnabled = true
            item.isEnabled = true
            if item.tag == 100 {
                let longpress = UILongPressGestureRecognizer.init(target: self, action: #selector(Longbackpress))
                item.addGestureRecognizer(longpress)
            }
        }
        
        self.S6_ProcessBTN.setTitle("Complete", for: .normal)
        self.S6_ProcessBTN.tintColor = .clear
        self.S6_ProcessBTN.backgroundColor = .clear
        self.S6_ProcessBTN.setTitleColor(.white, for: .normal)
        self.S6_ProcessBTN.isEnabled = false
        self.S6_ProcessBTN.isUserInteractionEnabled = false
        self.S6_ProcessBTN.alpha = 0.5
    }
    
    @objc func Longbackpress() {
        self.DF_MainDismissBTN.tag = 0
        self.PinArray.removeAllObjects()
        self.S6_Amount1LBL.text = "0.00"
        if let n = NumberFormatter().number(from: self.S6_Amount1LBL.text! as String) {
            let f = Double(truncating: n)
            self.S6_Amount1LBL.text = String.init(format: "%f", f).WithoutCoinPriceThumbRules(Coin: self.S1_CoinsVault.coin)
        }
        
        self.S6_Amount2LBL.text = "0.00"
        if let n = NumberFormatter().number(from: self.S6_Amount2LBL.text! as String) {
            let f = Double(truncating: n)
            self.S6_Amount2LBL.text = String.init(format: "%f", f).WithoutCoinPriceThumbRules(Coin: self.S4_CoinsVault.coinSymbol)
        }
    }
    
    func EnterPin(digit: String) {
        self.PinArray.add(digit)
        self.EnteredAmounts()
        if self.PinArray.count != 0 {
            self.S6_ProcessBTN.isEnabled = true
            self.S6_ProcessBTN.isUserInteractionEnabled = true
            self.S6_ProcessBTN.alpha = 1
        }
        else {
            self.S6_ProcessBTN.isEnabled = false
            self.S6_ProcessBTN.isUserInteractionEnabled = false
            self.S6_ProcessBTN.alpha = 0.5
        }
    }
    
    func EnteredAmounts() {
        for (index, item) in self.PinArray.enumerated() {
            if self.isFirstAmount {
                if index == 0 {
                    self.S6_Amount1LBL.text = ""
                    self.S6_Amount1LBL.text = String.init(format: "%@", item as! CVarArg)
                }
                else {
                    self.S6_Amount1LBL.text = String.init(format: "%@%@", self.S6_Amount1LBL.text!, item as! CVarArg)
                }
            }
            else {
                if index == 0 {
                    self.S6_Amount2LBL.text = ""
                    self.S6_Amount2LBL.text = String.init(format: "%@", item as! CVarArg)
                }
                else {
                    self.S6_Amount2LBL.text = String.init(format: "%@%@", self.S6_Amount2LBL.text!, item as! CVarArg)
                }
            }
        }
        if self.isFirstAmount {
            if !(self.S6_Amount1LBL.text?.contains("."))! {
                if let n = NumberFormatter().number(from: self.S6_Amount1LBL.text! as String) {
                    let f = Double(truncating: n)
                    self.S6_Amount1LBL.text = String.init(format: "%f", f).WithoutCoinPriceThumbRules(Coin: self.S1_CoinsVault.coin)
                }
            }
            if let n = NumberFormatter().number(from: self.S6_Amount1LBL.text! as String) {
                let f = Double(truncating: n)
                self.Topupamount1 = f
            }
            
            self.ConvertForex(Buy: self.S1_CoinsVault.coin, From: self.S4_CoinsVault.coinSymbol, Value: self.S6_Amount1LBL.text!)
        }
        else {
            if !(self.S6_Amount2LBL.text?.contains("."))! {
                if let n = NumberFormatter().number(from: self.S6_Amount2LBL.text! as String) {
                    let f = Double(truncating: n)
                    self.S6_Amount2LBL.text = String.init(format: "%f", f).WithoutCoinPriceThumbRules(Coin: self.S4_CoinsVault.coinSymbol)
                }
            }
            if let n = NumberFormatter().number(from: self.S6_Amount2LBL.text! as String) {
                let f = Double(truncating: n)
                self.Topupamount2 = f
            }
            self.ConvertForex(Buy: self.S1_CoinsVault.coin, From: self.S4_CoinsVault.coinSymbol, Value: self.S6_Amount2LBL.text!)
        }
    }
    
    func backspace() {
        self.PinArray.removeLastObject()
        if self.PinArray.count == 0 {
            self.S6_Amount1LBL.text = "0.00"
            if let n = NumberFormatter().number(from: self.S6_Amount1LBL.text! as String) {
                let f = Double(truncating: n)
                self.S6_Amount1LBL.text = String.init(format: "%f", f).WithoutCoinPriceThumbRules(Coin: self.S1_CoinsVault.coin)
            }
            
            self.S6_Amount2LBL.text = "0.00"
            if let n = NumberFormatter().number(from: self.S6_Amount2LBL.text! as String) {
                let f = Double(truncating: n)
                self.S6_Amount2LBL.text = String.init(format: "%f", f).WithoutCoinPriceThumbRules(Coin: self.S4_CoinsVault.coinSymbol)
            }
        }
        else {
            self.EnteredAmounts()
        }
    }
    
    // MARK:- IBAction Methods
    @IBAction func S6_TappedBTN(_ sender: UIButton) {
        let step5 = FD_Step5VC.init(nibName: "FD_Step5VC", bundle: nil)
        step5.FromCome = self.FromCome
        step5.S1_CoinsVault = self.S1_CoinsVault
        step5.S4_CoinsVault = self.S4_CoinsVault
        step5.Value1 = self.Topupamount1
        step5.Value2 = self.Topupamount2
        step5.SectionCoin = self.SectionCoin
        step5.BetsVaultLoad = self.BetsVaultLoad
        step5.S4_VaultType = self.S4_VaultType
        step5.S1_VaultType = self.S1_VaultType
        step5.DP_Process_Tag = 300
        self.navigationController?.pushViewController(step5, animated: false)
    }
    
    @IBAction func S6_DigitsBTN(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 2:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 3:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 4:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 5:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 6:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 7:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 8:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 9:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 100:
            self.backspace()
            break
            
        case 0:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 200:
            if !self.PinArray.contains(".") {
                if self.PinArray.count == 0 {
                    self.EnterPin(digit: String.init(format: "0"))
                    self.EnterPin(digit: String.init(format: "."))
                }
                else {
                    self.EnterPin(digit: String.init(format: "."))
                }
            }
            break
            
        default:
            break
        }
    }
    
    @IBAction func S6_Active1LBL(_ sender: Any) {
        if self.FromCome.uppercased() == "FUND".uppercased() {
            self.isFirstAmount = true
            self.S6_Amount1LBL.font = Font().MontserratBoldFont(font: 17)
            self.S6_Amount2LBL.font = Font().MontserratLightFont(font: 17)
            self.Longbackpress()
        }
        else {
            
        }
    }
    
    @IBAction func S6_Active2LBL(_ sender: Any) {
        if self.FromCome.uppercased() == "FUND".uppercased() {
            self.isFirstAmount = false
            self.S6_Amount2LBL.font = Font().MontserratBoldFont(font: 17)
            self.S6_Amount1LBL.font = Font().MontserratLightFont(font: 17)
            self.Longbackpress()
        }
        else {
            
        }
    }
    
    @IBAction func S6_25BTN(_ sender: Any) {
        if self.FromCome.uppercased() == "FUND".uppercased() {
            let amount: Double = (self.S1_CoinsVault.liveBalance.nativeField * 25) / 100
            self.Topupamount1 = amount
            self.S6_Amount1LBL.text = String.init(format: "%f", self.Topupamount1).WithoutCoinPriceThumbRules(Coin: self.S1_CoinsVault.coin)
            self.ConvertForex(Buy: self.S1_CoinsVault.coin, From: self.S4_CoinsVault.coinSymbol, Value: self.S6_Amount1LBL.text!)
        }
        else {
            
        }
    }
    
    @IBAction func S6_50BTN(_ sender: Any) {
        if self.FromCome.uppercased() == "FUND".uppercased() {
            let amount = (self.S1_CoinsVault.liveBalance.nativeField * 50) / 100
            self.Topupamount1 = amount
            self.S6_Amount1LBL.text = String.init(format: "%f", self.Topupamount1).WithoutCoinPriceThumbRules(Coin: self.S1_CoinsVault.coin)
            self.ConvertForex(Buy: self.S1_CoinsVault.coin, From: self.S4_CoinsVault.coinSymbol, Value: self.S6_Amount1LBL.text!)
        }
        else {
            
        }
    }
    
    @IBAction func TappedDF_MainDismissBTN(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    

}
