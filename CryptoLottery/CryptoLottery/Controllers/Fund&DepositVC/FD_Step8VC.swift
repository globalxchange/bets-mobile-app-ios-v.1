//
//  FD_Step8VC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-06-24.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class FD_Step8VC: BaseVC {
    
    // MARK:- IBOutlets
    @IBOutlet weak var DF_MainView: CustomView!
    @IBOutlet var DF_bgLBL: [UILabel]!
    
    @IBOutlet weak var DF_MainView_Height: NSLayoutConstraint!
    @IBOutlet weak var DF_NautchLBL: CustomLBL!
    @IBOutlet weak var DF_LogoIMG: UIImageView!
    @IBOutlet weak var DF_MainTitleLBL: UILabel!
    @IBOutlet weak var DF_MainDismissBTN: UIButton!
    
    // TODO:- Step 8 Congrest View
    @IBOutlet weak var S8_CongratulationView: UIView!
    @IBOutlet weak var S8_CongretTitleLBL: UILabel!
    @IBOutlet weak var S8_CongretCoinView: CustomView!
    @IBOutlet weak var S8_CongretCoinIMG: UIImageView!
    @IBOutlet weak var S8_CongretCoinName: UILabel!
    @IBOutlet weak var S8_CongretCoinSeprator: UILabel!
    @IBOutlet weak var S8_CongretCoinDetail: UILabel!
    @IBOutlet weak var S8_Collection: UICollectionView!
    @IBOutlet var AllBottomview: UIView!
    @IBOutlet weak var S8_CloseBTN: UIButton!
    
    
    var FromCome: String!
    var S1_CoinsVault: BetsVaultVault!
    var S4_CoinsVault: CoinVaultCoin!
    var Topupamount1: Double = 0.0
    var Topupamount2: Double = 0.0
    
    
    // MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.DF_MainView.backgroundColor = .white
        self.DF_MainView.BGColor = .white
        self.DF_LogoIMG.tintColor = AppDarkColor
        self.DF_MainTitleLBL.theme_textColor = GlobalPicker.textColor
        
        self.AllBottomview.backgroundColor = AppDarkColor
        self.SetupCongrestView_8()
        self.cornerColor()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK:- User Define Methods
    func cornerColor(color: UIColor = AppDarkColor) {
        for lbl in self.DF_bgLBL {
            lbl.backgroundColor = color
        }
    }

    func SetupCongrestView_8() {
        
        self.DF_MainTitleLBL.text = ""
        self.DF_MainView_Height.constant = 130 + (3 * 120)
        self.S8_CongretCoinName.theme_textColor = GlobalPicker.textColor
        self.S8_CongretCoinDetail.theme_textColor = GlobalPicker.notesColor
        
        if self.FromCome.uppercased() == "FUND".uppercased() {
            self.S8_CongretCoinIMG.downloadedFrom(link: self.S4_CoinsVault.coinImage, contentMode: .scaleAspectFit, radious: (self.S8_CongretCoinIMG.frame.height / 2))
            self.S8_CongretCoinName.text = self.S4_CoinsVault.coinSymbol
            self.S8_CongretCoinDetail.text = String.init(format: "%f", self.Topupamount2).CoinPriceThumbRules(Coin: self.S4_CoinsVault.coinSymbol)
            
            self.S8_CongretTitleLBL.text = String.init(format: "Congratulations Your Have Depositd ") + String.init(format: "%f", self.Topupamount2).CoinPriceThumbRules(Coin: self.S4_CoinsVault.coinSymbol)! + " Into Your Bets Vault"
        }
        else {
            self.S8_CongretCoinIMG.downloadedFrom(link: self.S4_CoinsVault.coinImage, contentMode: .scaleAspectFit, radious: (self.S8_CongretCoinIMG.frame.height / 2))
            self.S8_CongretCoinName.text = self.S4_CoinsVault.coinSymbol
            
            self.S8_CongretCoinDetail.text = String.init(format: "%f", self.Topupamount2).CoinPriceThumbRules(Coin: self.S4_CoinsVault.coinSymbol)
            
            self.S8_CongretTitleLBL.text = String.init(format: "Congratulations Your Have Widthdraw  ") + String.init(format: "%f", self.Topupamount2).CoinPriceThumbRules(Coin: self.S4_CoinsVault.coinSymbol)! + " From Your Bets Vault"
        }
        
        self.S8_Collection.register(UINib.init(nibName: "FDOptionCell", bundle: nil), forCellWithReuseIdentifier: "FDOptionCell")
        self.S8_Collection.translatesAutoresizingMaskIntoConstraints = false
        self.S8_Collection.delegate = self
        self.S8_Collection.dataSource = self
        
        self.S8_CloseBTN.setTitle("Close", for: .normal)
        self.S8_CloseBTN.tintColor = .clear
        self.S8_CloseBTN.backgroundColor = .clear
        self.S8_CloseBTN.setTitleColor(.white, for: .normal)
    }
    
    // MARK:- IBAction Methods
    @IBAction func S8_TappedBTN(_ sender: Any) {
        App?.SetDashboardVC(index: 2)
        NotificationCenter.default.post(name: .ReloadAccount, object: nil)
    }
    
}

extension FD_Step8VC: UICollectionViewDelegate,UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FDOptionCell",for: indexPath) as? FDOptionCell else { fatalError() }
        
        cell.NameLBL.text = "Explore Lotteries"
        if indexPath.row == 0 {
            cell.CardIMG.image = UIImage.init(named: "Tab1")
        }
        else if indexPath.row == 1 {
            cell.CardIMG.image = UIImage.init(named: "BetsGraph")
        }
        else {
            cell.CardIMG.image = UIImage.init(named: "Firebets")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            App?.SetDashboardVC(index: 2)
            NotificationCenter.default.post(name: .ReloadAccount, object: nil)
        }
        else if indexPath.row == 1 {
            App?.SetDashboardVC(index: 2)
            NotificationCenter.default.post(name: .ReloadAccount, object: nil)
        }
        else {
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 2.5, height: collectionView.frame.height)
    }
    
}
