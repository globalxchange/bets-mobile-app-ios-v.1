//
//  FundingCell.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-05-30.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class FundingCell: UITableViewCell {

    @IBOutlet weak var MainIMG: UIImageView!
    @IBOutlet weak var Icon: UIImageView!
    @IBOutlet weak var Name: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func isEnableCell(status: Bool) {
        self.isUserInteractionEnabled = status
        let view: UIView = self.contentView.subviews.first!
        if status {
            view.alpha = 1.0
        }
        else {
            view.alpha = 0.3
        }
    }
    
}
