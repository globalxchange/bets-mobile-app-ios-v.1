//
//  IGCMenu.m
//  IGCMenu
//
//  Created by Sunil Sharma on 11/02/16.
//  Copyright (c) 2016 Sunil Sharma. All rights reserved.
//

#import "IGCMenu.h"
#import <QuartzCore/QuartzCore.h>
#import "VideoListCell.h"

#define MENU_START_TAG(offset) (6000 + offset)
#define MENU_NAME_LABEL_TAG(offset) (6100 + offset)
#define ANIMATION_DURATION 0.4
#define MENU_BACKGROUND_VIEW_TAG 6200

@implementation IGCMenu{
    NSMutableArray *menuButtonArray;        //array of menu buttons
    NSMutableArray *menuNameLabelArray;     //array of menu name label
    UIView *pMenuButtonSuperView;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        menuButtonArray = [[NSMutableArray alloc] init];
        menuNameLabelArray = [[NSMutableArray alloc] init];
        //Default values
        self.disableBackground = YES;
        self.numberOfMenuItem = 0;
        self.menuRadius = 120;
        self.maxColumn = 3;
        self.backgroundType = BlurEffectDark;
    }
    return self;
}

- (void)createMenuButtons{
    [menuButtonArray removeAllObjects];
    [menuNameLabelArray removeAllObjects];
    
    for (int i = 0; i < self.numberOfMenuItem; i++) {
        
        UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
        menuButton.backgroundColor = [UIColor whiteColor];
        menuButton.tag = MENU_START_TAG(i);
        CGRect newFrame = menuButton.frame;
        CGFloat menuButtonSize;
        if (self.menuHeight) {
            menuButtonSize = self.menuHeight;
        }
        else{
            menuButtonSize = self.menuHeight = 65;
        }
        newFrame.size = CGSizeMake(menuButtonSize, menuButtonSize);
        menuButton.frame = newFrame;
        
        menuButton.center = self.menuButton.center;
        menuButton.layer.cornerRadius = menuButton.frame.size.height / 2;
        menuButton.layer.masksToBounds = YES;
        menuButton.layer.opacity = 0.0;
        [menuButton addTarget:self action:@selector(menuButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [pMenuButtonSuperView insertSubview:menuButton belowSubview:self.menuButton];
        [menuButtonArray addObject:menuButton];
        //Display menu name if present
        if (self.menuItemsNameArray.count > i) {
            UILabel *menuNameLabel = [[UILabel alloc] init];
            menuNameLabel.backgroundColor = [UIColor clearColor];
            menuNameLabel.numberOfLines = 1;
            newFrame = menuNameLabel.frame;
            newFrame.size = CGSizeMake(menuButton.frame.size.width, 20);
            menuNameLabel.frame = newFrame;
            menuNameLabel.center = menuButton.center;
            menuNameLabel.layer.opacity = 0.0;
            menuNameLabel.textAlignment = NSTextAlignmentCenter;
            menuNameLabel.font = [UIFont fontWithName:@"Montserrat-Bold" size:11.0];
            menuNameLabel.text = self.menuItemsNameArray[i];
            [menuNameLabel sizeToFit];
            if (self.backgroundType == BlurEffectExtraLight) {
                menuNameLabel.textColor = [UIColor colorWithRed: 0.00 green: 0.16 blue: 0.32 alpha: 1.00];
            }
            else if (self.backgroundType == BlurEffectLight) {
                menuNameLabel.textColor = [UIColor colorWithRed: 0.00 green: 0.16 blue: 0.32 alpha: 1.00];
            }
            else if (self.backgroundType == BlurEffectDark) {
                menuNameLabel.textColor = [UIColor whiteColor];
            }
            else if (self.backgroundType == Dark) {
                menuNameLabel.textColor = [UIColor whiteColor];
            }
            else {
                menuNameLabel.textColor = [UIColor whiteColor];
            }
            [pMenuButtonSuperView insertSubview:menuNameLabel belowSubview:self.menuButton];
            [menuNameLabelArray addObject:menuNameLabel];
        }
        
        // set accessibility label and add the label if present
        if (self.menuItemsAccessibilityLabelsArray.count > i) {
            menuButton.isAccessibilityElement = YES;
            menuButton.accessibilityLabel = self.menuItemsAccessibilityLabelsArray[i];
        }
        
        //Set custom menus button background color if present
        if (self.menuBackgroundColorsArray.count > i) {
            menuButton.backgroundColor =(UIColor *)self.menuBackgroundColorsArray[i];
        }
        
        //Display menu images if present
        if (self.menuImagesNameArray.count > i) {
            [menuButton setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
            menuButton.tintColor = [UIColor whiteColor];
            [menuButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",self.menuImagesNameArray[i]]] forState:UIControlStateNormal];
        }
        
        if (i == self.menuItemsNameArray.count - 1) {
            [self SetMainTitle];
        }
    }
}

-(void)dismiss {
    [self hideGridMenu];
    [self hideCircularMenu];
    if ([self.delegate respondsToSelector:@selector(DissmissingMenu)]) {
        if (self.delegate) {
            [self.delegate DissmissingMenu];
        }
    }
}

- (void)menuSuperViewBackground{
    if (pMenuButtonSuperView == nil) {
        pMenuButtonSuperView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        pMenuButtonSuperView.tag = MENU_BACKGROUND_VIEW_TAG;
        
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
//        [pMenuButtonSuperView addGestureRecognizer:tap];
    }
    if (!self.menuSuperView) {
        self.menuSuperView = [self.menuButton superview];
    }
    [self.menuSuperView bringSubviewToFront:self.menuButton];
    [self.menuSuperView insertSubview:pMenuButtonSuperView belowSubview:self.menuButton];
    
    if (self.disableBackground){
        pMenuButtonSuperView.userInteractionEnabled = YES;
    }
    else{
        pMenuButtonSuperView.userInteractionEnabled = NO;
    }
    [self setBackgroundEffect];
}

-(void)setBackgroundEffect{
    
    switch (self.backgroundType) {
        case Dark:
            pMenuButtonSuperView.layer.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8].CGColor;
            break;
        case BlurEffectDark:
            [self setBlurredView:UIBlurEffectStyleDark];
            break;
        case BlurEffectLight:
            [self setBlurredView:UIBlurEffectStyleLight];
            break;
        case BlurEffectExtraLight:
            [self setBlurredView:UIBlurEffectStyleExtraLight];
            break;
        case None:
            pMenuButtonSuperView.layer.backgroundColor = [UIColor clearColor].CGColor;
            break;
        default:
            pMenuButtonSuperView.layer.backgroundColor = [UIColor clearColor].CGColor;
            break;
    }
}

-(void)setBlurredView:(UIBlurEffectStyle) blurEffectStyle{
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:blurEffectStyle];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = pMenuButtonSuperView.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [pMenuButtonSuperView addSubview:blurEffectView];
    }
    else {
        pMenuButtonSuperView.backgroundColor = [UIColor clearColor];
    }
}

-(CGFloat)gettopconstraint {
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        switch ((int)[[UIScreen mainScreen] nativeBounds].size.height) {
            case 1136:
                NSLog(@"iPhone 5 or 5S or 5C");
                return 30;
                
            case 1334:
                NSLog(@"iPhone 6/6S/7/8");
                return 30;
                
            case 1920:
                NSLog(@"iPhone 6+/6S+/7+/8+");
                return 30;
                
            case 2208:
                NSLog(@"iPhone 6+/6S+/7+/8+");
                return 30;
                
            case 2436:
                NSLog(@"iPhone X/XS/11 Pro");
                return 70;
                
            case 2688:
                NSLog(@"iPhone XS Max/11 Pro Max");
                return 70;
                
            case 1792:
                NSLog(@"iPhone XR/ 11 ");
                return 70;
                
            default:
                return 30;
                break;
        }
    }
    return 50;
}

-(void)SetMainTitle {

    CGRect lastFrame;
    UIButton *charts;
    for (id item in [pMenuButtonSuperView subviews])
    {
        if ([item isKindOfClass:[UIButton class]]) {
            UIButton *btn = item;
            if (btn.tag == 6002) {
                charts = btn;
                lastFrame = btn.frame;
            }
        }
    }
//    CGFloat usedHeight = [[UIScreen mainScreen] bounds].size.height - lastFrame.origin.y;
//    CGFloat remainHeight = [[UIScreen mainScreen] bounds].size.height - 350;
    
    self.VideoTBL = [[UITableView alloc] initWithFrame:CGRectMake(0, [self gettopconstraint] , [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height - (85 * 3.8) - [self gettopconstraint])];
    self.VideoTBL.backgroundColor = [UIColor clearColor];
    [self.VideoTBL registerNib:[UINib nibWithNibName:@"VideoListCell" bundle:nil] forCellReuseIdentifier:@"VideoListCell"];
    self.VideoTBL.alpha = 0.0;
    self.VideoTBL.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (self.VideoArray.count != 0) {
        self.VideoTBL.delegate = self;
        self.VideoTBL.dataSource = self;
        [self.VideoTBL reloadData];
        [self.VideoTBL setHidden:false];
    }
    else {
        [self.VideoTBL setHidden:true];
    }
    [pMenuButtonSuperView addSubview:self.VideoTBL];
}

- (void)showCircularMenu{
    
    [self menuSuperViewBackground];
    if (menuButtonArray.count <= 0) {
        [self createMenuButtons];
    }
    //menuButton.center = CGPointMake(homeButtonCenter.x - radius * cos(angle * i), homeButtonCenter.y - radius * sin(angle * i));
    
    for (int  i = 1; i < menuButtonArray.count * 2; i=i+2) {
        
        CGFloat angle = M_PI / (menuButtonArray.count * 2);
        [UIView animateWithDuration:ANIMATION_DURATION delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self->pMenuButtonSuperView.layer.opacity = 1.0;
            UIButton * menuButton = (UIButton *)[self->menuButtonArray objectAtIndex:i/2];
            menuButton.layer.opacity = 1.0;
            menuButton.center = CGPointMake(self.menuButton.center.x - self.menuRadius * cos(angle * i), self.menuButton.center.y - self.menuRadius * sin(angle * i));
            if (self->menuNameLabelArray.count > (i/2)) {
                UILabel *menuNameLabel = (UILabel *)[self->menuNameLabelArray objectAtIndex:i/2];
                menuNameLabel.layer.opacity = 1.0;
                menuNameLabel.center = CGPointMake(menuButton.center.x, menuButton.frame.origin.y + menuButton.frame.size.height  + (menuNameLabel.frame.size.height / 2) + 5);
            }
            self.VideoTBL.alpha = 1.0;
        }completion:nil];
    }
}

- (void)hideCircularMenu{
    [UIView animateWithDuration:ANIMATION_DURATION delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        for (int i = 0; i < self->menuButtonArray.count; i++) {
            UIButton *menuButton = (UIButton *)[self->menuButtonArray objectAtIndex:i];
            menuButton.layer.opacity = 0.0;
            menuButton.center = self.menuButton.center;
            if (self->menuNameLabelArray.count > i) {
                UILabel *menuNameLabel = (UILabel *)[self->menuNameLabelArray objectAtIndex:i];
                menuNameLabel.layer.opacity = 0.0;
                menuNameLabel.center = self.menuButton.center;
                self->pMenuButtonSuperView.layer.opacity = 0.0;
            }
        }
    } completion:^(BOOL finished) {
        [self->pMenuButtonSuperView removeFromSuperview];
        self->pMenuButtonSuperView = nil;
        for (int i = 0; i < self->menuButtonArray.count; i++) {
            UIButton *menuButton = (UIButton *)[self->menuButtonArray objectAtIndex:i];
            [menuButton removeFromSuperview];
            if (self->menuNameLabelArray.count > i) {
                UILabel *menuNameLabel = (UILabel *)[self->menuNameLabelArray objectAtIndex:i];
                [menuNameLabel removeFromSuperview];
            }
        }
        [self->menuNameLabelArray removeAllObjects];
        [self->menuButtonArray removeAllObjects];
    }];
}

-(void)showGridMenu{
    [self menuSuperViewBackground];
    if (menuButtonArray.count <= 0) {
        [self createMenuButtons];
    }
    int maxRow = ceilf(menuButtonArray.count /(float)self.maxColumn);
    __block CGFloat topMenuCenterY = self.menuButton.frame.origin.y - 10;
    CGFloat eachMenuVerticalSpace = 0;
    CGFloat eachMenuWidth = 0;
    if (menuButtonArray.count) {
        UIButton *menuButton = (UIButton *)menuButtonArray[0];
        eachMenuVerticalSpace = menuButton.frame.size.height + 20;
        eachMenuWidth = menuButton.frame.size.width;
        if (menuNameLabelArray.count) {
            UILabel *nameLabel = (UILabel *)menuNameLabelArray[0];
            eachMenuVerticalSpace = eachMenuVerticalSpace + nameLabel.frame.size.height;
        }
        topMenuCenterY = topMenuCenterY - (eachMenuVerticalSpace * maxRow) + menuButton.frame.size.height/2;
    }
    else{
        eachMenuVerticalSpace = 100.0;
        topMenuCenterY = topMenuCenterY - (eachMenuVerticalSpace * maxRow) + eachMenuVerticalSpace/3;
    }
    
    __block CGFloat distanceBetweenMenu = ((pMenuButtonSuperView.frame.size.width - (self.maxColumn*eachMenuWidth))/(self.maxColumn +1));
    
    [UIView animateWithDuration:ANIMATION_DURATION delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self->pMenuButtonSuperView.layer.opacity = 1.0;
        
        int menuIndex = 0;
        //for each row
        for(int  i = 1; i <= maxRow; i++,topMenuCenterY += eachMenuVerticalSpace) {
            
            int remainingMenuButton = self.maxColumn;
            //CGFloat menuCenterX = distanceBetweenMenu;
            
            CGFloat menuCenterX;
            //for each column
            for (int j = 1; j <= remainingMenuButton; j++) {
                UIButton *menuButton = (UIButton *)[self->menuButtonArray objectAtIndex:menuIndex];
                menuButton.layer.opacity = 1.0;
                
                menuCenterX = (distanceBetweenMenu *j) + (2*j - 1)*(menuButton.frame.size.width/2);
                if (i == maxRow) {
                    remainingMenuButton = self->menuButtonArray.count % self.maxColumn;
                    if (remainingMenuButton == 0) {
                        remainingMenuButton = self.maxColumn;
                    }
                    menuCenterX = menuCenterX + ((self.maxColumn - remainingMenuButton)*(distanceBetweenMenu/2)) + (self.maxColumn - remainingMenuButton)*menuButton.frame.size.width/2;
                }
                menuButton.center = CGPointMake(menuCenterX, topMenuCenterY);
                
                if (self->menuNameLabelArray.count > menuIndex) {
                    UILabel *menuNameLabel = (UILabel *)[self->menuNameLabelArray objectAtIndex:menuIndex];
                    menuNameLabel.layer.opacity = 1.0;
                    menuNameLabel.center = CGPointMake(menuButton.center.x, menuButton.frame.origin.y + menuButton.frame.size.height  + (menuNameLabel.frame.size.height / 2) + 5);
                }
                
                menuIndex++;
            }
        }
        self.VideoTBL.alpha = 1.0;
    }completion:nil];
}

-(void)hideGridMenu{
    [self hideCircularMenu];
}

- (void)menuButtonClicked:(UIButton *)sender{
    if ([self.delegate respondsToSelector:@selector(igcMenuSelected:atIndex:)]) {
        int index;
        NSInteger buttonTag =  sender.tag;
        for (index = 0; index < menuButtonArray.count; index++) {
            UIButton *menuButton = (UIButton *)[menuButtonArray objectAtIndex:index];
            if (menuButton.tag == buttonTag) {
                NSString *menuName;
                if (self.menuItemsNameArray.count > index) {
                    menuName = self.menuItemsNameArray[index];
                }
                if (self.delegate) {
                    [self.delegate igcMenuSelected:menuName atIndex:index];
                }
                break;
            }
        }
    }
}


//MARK:- Tableview Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.VideoArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 100;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 100)];
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = view.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [view addSubview:blurEffectView];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [[UIScreen mainScreen] bounds].size.width - 30, 100)];
    title.text = @"WATCH THE NEXT LIVE #JACKPOT";
    title.font = [UIFont fontWithName:@"Montserrat-Bold" size:25.0];
    title.numberOfLines = 2;
    title.textColor = [UIColor colorWithRed: 0.00 green: 0.16 blue: 0.32 alpha: 1.00];
    title.textAlignment = NSTextAlignmentCenter;
    title.font = [UIFont systemFontOfSize:25.0];
    [view addSubview:title];
    
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"VideoListCell";
    
    VideoListCell *cell = (VideoListCell *)[tableView  dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"VideoListCell" owner:self options:nil] objectAtIndex:0];
    }
    cell.title.text = @"You May Be The One winning $2,345.00 USD";
    cell.title.font = [UIFont fontWithName:@"Montserrat-Regular" size:17.0];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate respondsToSelector:@selector(igcVideostreamingSelected:)]) {
        if (self.delegate) {
            [self.delegate igcVideostreamingSelected:indexPath.row];
        }
    }
}

@end
