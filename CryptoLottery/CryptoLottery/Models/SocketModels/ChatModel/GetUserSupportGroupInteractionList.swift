//
//  GetUserSupportGroupInteractionList.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-07-10.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import Foundation

struct GetUserSupportGroupInteractionList{

    var groupName : String!
    var msgTimestamp : Double!
    var sender : String!
    var threadId : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        groupName = dictionary["group_name"] as? String
        msgTimestamp = dictionary["msg_timestamp"] as? Double
        sender = dictionary["sender"] as? String
        threadId = dictionary["thread_id"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if groupName != nil{
            dictionary["group_name"] = groupName
        }
        if msgTimestamp != nil{
            dictionary["msg_timestamp"] = msgTimestamp
        }
        if sender != nil{
            dictionary["sender"] = sender
        }
        if threadId != nil{
            dictionary["thread_id"] = threadId
        }
        return dictionary
    }

}
