//
//    CoinVaultPrice.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class CoinVaultPrice : NSObject, NSCoding{

    var aED : Double!
    var aRS : Double!
    var aUD : Double!
    var cAD : Double!
    var cNY : Double!
    var cOP : Double!
    var eUR : Double!
    var gBP : Double!
    var iNR : Double!
    var jPY : Double!
    var mXN : Double!
    var uSD : Double!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        aED = dictionary["AED"] as? Double
        aRS = dictionary["ARS"] as? Double
        aUD = dictionary["AUD"] as? Double
        cAD = dictionary["CAD"] as? Double
        cNY = dictionary["CNY"] as? Double
        cOP = dictionary["COP"] as? Double
        eUR = dictionary["EUR"] as? Double
        gBP = dictionary["GBP"] as? Double
        iNR = dictionary["INR"] as? Double
        jPY = dictionary["JPY"] as? Double
        mXN = dictionary["MXN"] as? Double
        uSD = dictionary["USD"] as? Double
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if aED != nil{
            dictionary["AED"] = aED
        }
        if aRS != nil{
            dictionary["ARS"] = aRS
        }
        if aUD != nil{
            dictionary["AUD"] = aUD
        }
        if cAD != nil{
            dictionary["CAD"] = cAD
        }
        if cNY != nil{
            dictionary["CNY"] = cNY
        }
        if cOP != nil{
            dictionary["COP"] = cOP
        }
        if eUR != nil{
            dictionary["EUR"] = eUR
        }
        if gBP != nil{
            dictionary["GBP"] = gBP
        }
        if iNR != nil{
            dictionary["INR"] = iNR
        }
        if jPY != nil{
            dictionary["JPY"] = jPY
        }
        if mXN != nil{
            dictionary["MXN"] = mXN
        }
        if uSD != nil{
            dictionary["USD"] = uSD
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         aED = aDecoder.decodeObject(forKey: "AED") as? Double
         aRS = aDecoder.decodeObject(forKey: "ARS") as? Double
         aUD = aDecoder.decodeObject(forKey: "AUD") as? Double
         cAD = aDecoder.decodeObject(forKey: "CAD") as? Double
         cNY = aDecoder.decodeObject(forKey: "CNY") as? Double
         cOP = aDecoder.decodeObject(forKey: "COP") as? Double
         eUR = aDecoder.decodeObject(forKey: "EUR") as? Double
         gBP = aDecoder.decodeObject(forKey: "GBP") as? Double
         iNR = aDecoder.decodeObject(forKey: "INR") as? Double
         jPY = aDecoder.decodeObject(forKey: "JPY") as? Double
         mXN = aDecoder.decodeObject(forKey: "MXN") as? Double
         uSD = aDecoder.decodeObject(forKey: "USD") as? Double

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if aED != nil{
            aCoder.encode(aED, forKey: "AED")
        }
        if aRS != nil{
            aCoder.encode(aRS, forKey: "ARS")
        }
        if aUD != nil{
            aCoder.encode(aUD, forKey: "AUD")
        }
        if cAD != nil{
            aCoder.encode(cAD, forKey: "CAD")
        }
        if cNY != nil{
            aCoder.encode(cNY, forKey: "CNY")
        }
        if cOP != nil{
            aCoder.encode(cOP, forKey: "COP")
        }
        if eUR != nil{
            aCoder.encode(eUR, forKey: "EUR")
        }
        if gBP != nil{
            aCoder.encode(gBP, forKey: "GBP")
        }
        if iNR != nil{
            aCoder.encode(iNR, forKey: "INR")
        }
        if jPY != nil{
            aCoder.encode(jPY, forKey: "JPY")
        }
        if mXN != nil{
            aCoder.encode(mXN, forKey: "MXN")
        }
        if uSD != nil{
            aCoder.encode(uSD, forKey: "USD")
        }

    }

}
