//
//	BetsVaultCoin.swift
//
//	Create by Hiren on 29/7/2020
//	Copyright © 2020. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class BetsVaultCoin : NSObject, NSCoding{

	var coin : String!
	var investments : BetsVaultInvestment!
	var tickets : Double!
	var totalDepositAmount : BetsVaultLiveInvestment!
	var totalWithdrawAmount : BetsVaultLiveInvestment!
	var totalWonAmount : BetsVaultTotalInvestment!
	var winnings : Double!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		coin = dictionary["coin"] as? String
		if let investmentsData = dictionary["investments"] as? NSDictionary{
			investments = BetsVaultInvestment(fromDictionary: investmentsData)
		}
		tickets = dictionary["tickets"] as? Double
		if let totalDepositAmountData = dictionary["total_deposit_amount"] as? NSDictionary{
			totalDepositAmount = BetsVaultLiveInvestment(fromDictionary: totalDepositAmountData)
		}
		if let totalWithdrawAmountData = dictionary["total_withdraw_amount"] as? NSDictionary{
			totalWithdrawAmount = BetsVaultLiveInvestment(fromDictionary: totalWithdrawAmountData)
		}
		if let totalWonAmountData = dictionary["total_won_amount"] as? NSDictionary{
			totalWonAmount = BetsVaultTotalInvestment(fromDictionary: totalWonAmountData)
		}
		winnings = dictionary["winnings"] as? Double
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if coin != nil{
			dictionary["coin"] = coin
		}
		if investments != nil{
			dictionary["investments"] = investments.toDictionary()
		}
		if tickets != nil{
			dictionary["tickets"] = tickets
		}
		if totalDepositAmount != nil{
			dictionary["total_deposit_amount"] = totalDepositAmount.toDictionary()
		}
		if totalWithdrawAmount != nil{
			dictionary["total_withdraw_amount"] = totalWithdrawAmount.toDictionary()
		}
		if totalWonAmount != nil{
			dictionary["total_won_amount"] = totalWonAmount.toDictionary()
		}
		if winnings != nil{
			dictionary["winnings"] = winnings
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
        coin = aDecoder.decodeObject(forKey: "coin") as? String
         investments = aDecoder.decodeObject(forKey: "investments") as? BetsVaultInvestment
         tickets = aDecoder.decodeObject(forKey: "tickets") as? Double
         totalDepositAmount = aDecoder.decodeObject(forKey: "total_deposit_amount") as? BetsVaultLiveInvestment
         totalWithdrawAmount = aDecoder.decodeObject(forKey: "total_withdraw_amount") as? BetsVaultLiveInvestment
         totalWonAmount = aDecoder.decodeObject(forKey: "total_won_amount") as? BetsVaultTotalInvestment
         winnings = aDecoder.decodeObject(forKey: "winnings") as? Double

	}

    /**
    * NSCoding required method.
    * Encodes mode properties Doubleo the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if coin != nil{
            aCoder.encode(coin, forKey: "coin")
		}
		if investments != nil{
			aCoder.encode(investments, forKey: "investments")
		}
		if tickets != nil{
			aCoder.encode(tickets, forKey: "tickets")
		}
		if totalDepositAmount != nil{
			aCoder.encode(totalDepositAmount, forKey: "total_deposit_amount")
		}
		if totalWithdrawAmount != nil{
			aCoder.encode(totalWithdrawAmount, forKey: "total_withdraw_amount")
		}
		if totalWonAmount != nil{
			aCoder.encode(totalWonAmount, forKey: "total_won_amount")
		}
		if winnings != nil{
			aCoder.encode(winnings, forKey: "winnings")
		}

	}
    
    func encode(with aCoder: NSCoder) {
        if coin != nil{
            aCoder.encode(coin, forKey: "coin")
        }
        if investments != nil{
            aCoder.encode(investments, forKey: "investments")
        }
        if tickets != nil{
            aCoder.encode(tickets, forKey: "tickets")
        }
        if totalDepositAmount != nil{
            aCoder.encode(totalDepositAmount, forKey: "total_deposit_amount")
        }
        if totalWithdrawAmount != nil{
            aCoder.encode(totalWithdrawAmount, forKey: "total_withdraw_amount")
        }
        if totalWonAmount != nil{
            aCoder.encode(totalWonAmount, forKey: "total_won_amount")
        }
        if winnings != nil{
            aCoder.encode(winnings, forKey: "winnings")
        }
    }

}
