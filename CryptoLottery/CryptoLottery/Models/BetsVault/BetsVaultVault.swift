//
//	BetsVaultVault.swift
//
//	Create by Hiren on 29/7/2020
//	Copyright © 2020. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class BetsVaultVault : NSObject, NSCoding{

	var coin : String!
    var imageUrl: String!
	var liveBalance : BetsVaultTotalInvestment!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
        coin = (dictionary["coin"] as? String)?.uppercased()
        imageUrl = dictionary["image_url"] as? String
		if let liveBalanceData = dictionary["live_balance"] as? NSDictionary{
			liveBalance = BetsVaultTotalInvestment(fromDictionary: liveBalanceData)
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if coin != nil{
            dictionary["coin"] = coin.uppercased()
		}
        if imageUrl != nil{
            dictionary["image_url"] = imageUrl
        }
		if liveBalance != nil{
			dictionary["live_balance"] = liveBalance.toDictionary()
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
        coin = (aDecoder.decodeObject(forKey: "coin") as? String)!.uppercased()
        imageUrl = aDecoder.decodeObject(forKey: "image_url") as? String
         liveBalance = aDecoder.decodeObject(forKey: "live_balance") as? BetsVaultTotalInvestment

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if coin != nil{
			aCoder.encode(coin.uppercased(), forKey: "coin")
		}
        if imageUrl != nil{
            aCoder.encode(imageUrl, forKey: "image_url")
        }
		if liveBalance != nil{
			aCoder.encode(liveBalance, forKey: "live_balance")
		}

	}
    
    func encode(with aCoder: NSCoder) {
        if coin != nil{
            aCoder.encode(coin.uppercased(), forKey: "coin")
        }
        if imageUrl != nil{
            aCoder.encode(imageUrl, forKey: "image_url")
        }
        if liveBalance != nil{
            aCoder.encode(liveBalance, forKey: "live_balance")
        }
    }

}
