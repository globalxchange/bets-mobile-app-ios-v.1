//
//	BetsVaultInvestment.swift
//
//	Create by Hiren on 29/7/2020
//	Copyright © 2020. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class BetsVaultInvestment : NSObject, NSCoding{

	var liveInvestment : BetsVaultLiveInvestment!
	var totalInvestment : BetsVaultTotalInvestment!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		if let liveInvestmentData = dictionary["live_investment"] as? NSDictionary{
			liveInvestment = BetsVaultLiveInvestment(fromDictionary: liveInvestmentData)
		}
		if let totalInvestmentData = dictionary["total_investment"] as? NSDictionary{
			totalInvestment = BetsVaultTotalInvestment(fromDictionary: totalInvestmentData)
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if liveInvestment != nil{
			dictionary["live_investment"] = liveInvestment.toDictionary()
		}
		if totalInvestment != nil{
			dictionary["total_investment"] = totalInvestment.toDictionary()
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
        liveInvestment = aDecoder.decodeObject(forKey: "live_investment") as? BetsVaultLiveInvestment
         totalInvestment = aDecoder.decodeObject(forKey: "total_investment") as? BetsVaultTotalInvestment

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if liveInvestment != nil{
			aCoder.encode(liveInvestment, forKey: "live_investment")
		}
		if totalInvestment != nil{
			aCoder.encode(totalInvestment, forKey: "total_investment")
		}

	}
    
    func encode(with aCoder: NSCoder) {
        if liveInvestment != nil{
            aCoder.encode(liveInvestment, forKey: "live_investment")
        }
        if totalInvestment != nil{
            aCoder.encode(totalInvestment, forKey: "total_investment")
        }
    }

}
