//
//	LotteryClosedLottery.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class LotteryClosedLottery : NSObject, NSCoding{

	var coin : String!
	var endTimestamp : Double!
	var featured : Bool!
	var freezeTimestamp : Double!
	var id : String!
	var interestRate : Double!
	var lottery : String!
	var lotteryBalance : Double!
	var masterFeatured : Bool!
	var startTimestamp : Double!
	var ticketPrice : Double!
	var ticketsSold : Double!
	var winner : String!
	var winnerDeclared : Bool!
	var winningTicket : Double!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		coin = dictionary["coin"] as? String
		endTimestamp = dictionary["end_timestamp"] as? Double
		featured = dictionary["featured"] as? Bool
		freezeTimestamp = dictionary["freeze_timestamp"] as? Double
		id = dictionary["id"] as? String
		interestRate = dictionary["interest_rate"] as? Double
		lottery = dictionary["lottery"] as? String
		lotteryBalance = dictionary["lottery_balance"] as? Double
		masterFeatured = dictionary["master_featured"] as? Bool
		startTimestamp = dictionary["start_timestamp"] as? Double
		ticketPrice = dictionary["ticket_price"] as? Double
		ticketsSold = dictionary["tickets_sold"] as? Double
		winner = dictionary["winner"] as? String
		winnerDeclared = dictionary["winner_declared"] as? Bool
		winningTicket = dictionary["winning_ticket"] as? Double
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if coin != nil{
			dictionary["coin"] = coin
		}
		if endTimestamp != nil{
			dictionary["end_timestamp"] = endTimestamp
		}
		if featured != nil{
			dictionary["featured"] = featured
		}
		if freezeTimestamp != nil{
			dictionary["freeze_timestamp"] = freezeTimestamp
		}
		if id != nil{
			dictionary["id"] = id
		}
		if interestRate != nil{
			dictionary["interest_rate"] = interestRate
		}
		if lottery != nil{
			dictionary["lottery"] = lottery
		}
		if lotteryBalance != nil{
			dictionary["lottery_balance"] = lotteryBalance
		}
		if masterFeatured != nil{
			dictionary["master_featured"] = masterFeatured
		}
		if startTimestamp != nil{
			dictionary["start_timestamp"] = startTimestamp
		}
		if ticketPrice != nil{
			dictionary["ticket_price"] = ticketPrice
		}
		if ticketsSold != nil{
			dictionary["tickets_sold"] = ticketsSold
		}
		if winner != nil{
			dictionary["winner"] = winner
		}
		if winnerDeclared != nil{
			dictionary["winner_declared"] = winnerDeclared
		}
		if winningTicket != nil{
			dictionary["winning_ticket"] = winningTicket
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         coin = aDecoder.decodeObject(forKey: "coin") as? String
         endTimestamp = aDecoder.decodeObject(forKey: "end_timestamp") as? Double
         featured = aDecoder.decodeObject(forKey: "featured") as? Bool
         freezeTimestamp = aDecoder.decodeObject(forKey: "freeze_timestamp") as? Double
         id = aDecoder.decodeObject(forKey: "id") as? String
         interestRate = aDecoder.decodeObject(forKey: "interest_rate") as? Double
         lottery = aDecoder.decodeObject(forKey: "lottery") as? String
         lotteryBalance = aDecoder.decodeObject(forKey: "lottery_balance") as? Double
         masterFeatured = aDecoder.decodeObject(forKey: "master_featured") as? Bool
         startTimestamp = aDecoder.decodeObject(forKey: "start_timestamp") as? Double
         ticketPrice = aDecoder.decodeObject(forKey: "ticket_price") as? Double
         ticketsSold = aDecoder.decodeObject(forKey: "tickets_sold") as? Double
         winner = aDecoder.decodeObject(forKey: "winner") as? String
         winnerDeclared = aDecoder.decodeObject(forKey: "winner_declared") as? Bool
         winningTicket = aDecoder.decodeObject(forKey: "winning_ticket") as? Double

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if coin != nil{
			aCoder.encode(coin, forKey: "coin")
		}
		if endTimestamp != nil{
			aCoder.encode(endTimestamp, forKey: "end_timestamp")
		}
		if featured != nil{
			aCoder.encode(featured, forKey: "featured")
		}
		if freezeTimestamp != nil{
			aCoder.encode(freezeTimestamp, forKey: "freeze_timestamp")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if interestRate != nil{
			aCoder.encode(interestRate, forKey: "interest_rate")
		}
		if lottery != nil{
			aCoder.encode(lottery, forKey: "lottery")
		}
		if lotteryBalance != nil{
			aCoder.encode(lotteryBalance, forKey: "lottery_balance")
		}
		if masterFeatured != nil{
			aCoder.encode(masterFeatured, forKey: "master_featured")
		}
		if startTimestamp != nil{
			aCoder.encode(startTimestamp, forKey: "start_timestamp")
		}
		if ticketPrice != nil{
			aCoder.encode(ticketPrice, forKey: "ticket_price")
		}
		if ticketsSold != nil{
			aCoder.encode(ticketsSold, forKey: "tickets_sold")
		}
		if winner != nil{
			aCoder.encode(winner, forKey: "winner")
		}
		if winnerDeclared != nil{
			aCoder.encode(winnerDeclared, forKey: "winner_declared")
		}
		if winningTicket != nil{
			aCoder.encode(winningTicket, forKey: "winning_ticket")
		}

	}

}
