//
//    UserBioArcadeId.swift
//
//    Create by Hiren on 30/7/2020
//    Copyright © 2020. All rights reserved.
//    Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class UserBioArcadeId : NSObject, NSCoding {
    
    var arcadeId : String!
    var arcadeUserid : String!
    var registered : Bool!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: NSDictionary){
        arcadeId = dictionary["arcade_id"] as? String
        arcadeUserid = dictionary["arcade_userid"] as? String
        registered = dictionary["registered"] as? Bool
    }

    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if arcadeId != nil{
            dictionary["arcade_id"] = arcadeId
        }
        if arcadeUserid != nil{
            dictionary["arcade_userid"] = arcadeUserid
        }
        if registered != nil{
            dictionary["registered"] = registered
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        arcadeId = aDecoder.decodeObject(forKey: "arcade_id") as? String
         arcadeUserid = aDecoder.decodeObject(forKey: "arcade_userid") as? String
         registered = aDecoder.decodeObject(forKey: "registered") as? Bool

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
    {
        if arcadeId != nil{
            aCoder.encode(arcadeId, forKey: "arcade_id")
        }
        if arcadeUserid != nil{
            aCoder.encode(arcadeUserid, forKey: "arcade_userid")
        }
        if registered != nil{
            aCoder.encode(registered, forKey: "registered")
        }

    }
    
    func encode(with aCoder: NSCoder) {
        if arcadeId != nil{
            aCoder.encode(arcadeId, forKey: "arcade_id")
        }
        if arcadeUserid != nil{
            aCoder.encode(arcadeUserid, forKey: "arcade_userid")
        }
        if registered != nil{
            aCoder.encode(registered, forKey: "registered")
        }
    }

}
