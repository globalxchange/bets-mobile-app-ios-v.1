//
//    VaultTransaction_Payload.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class VaultTransaction_Payload : NSObject, NSCoding{

    var coin : String!
    var depositCount : Double!
    var reclaimCount : Double!
    var revokeCount : Double!
    var ticketBuyCount : Double!
    var totalTransactionCount : Double!
    var transactionList : [VaultTransaction_TransactionList]!
    var withdrawCount : Double!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        coin = dictionary["coin"] as? String
        depositCount = dictionary["deposit_count"] as? Double
        reclaimCount = dictionary["reclaim_count"] as? Double
        revokeCount = dictionary["revoke_count"] as? Double
        ticketBuyCount = dictionary["ticket_buy_count"] as? Double
        totalTransactionCount = dictionary["total_transaction_count"] as? Double
        transactionList = [VaultTransaction_TransactionList]()
        if let transactionListArray = dictionary["transaction_list"] as? [[String:Any]]{
            for dic in transactionListArray{
                let value = VaultTransaction_TransactionList(fromDictionary: dic)
                transactionList.append(value)
            }
        }
        withdrawCount = dictionary["withdraw_count"] as? Double
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if coin != nil{
            dictionary["coin"] = coin
        }
        if depositCount != nil{
            dictionary["deposit_count"] = depositCount
        }
        if reclaimCount != nil{
            dictionary["reclaim_count"] = reclaimCount
        }
        if revokeCount != nil{
            dictionary["revoke_count"] = revokeCount
        }
        if ticketBuyCount != nil{
            dictionary["ticket_buy_count"] = ticketBuyCount
        }
        if totalTransactionCount != nil{
            dictionary["total_transaction_count"] = totalTransactionCount
        }
        if transactionList != nil{
            var dictionaryElements = [[String:Any]]()
            for transactionListElement in transactionList {
                dictionaryElements.append(transactionListElement.toDictionary())
            }
            dictionary["transaction_list"] = dictionaryElements
        }
        if withdrawCount != nil{
            dictionary["withdraw_count"] = withdrawCount
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         coin = aDecoder.decodeObject(forKey: "coin") as? String
         depositCount = aDecoder.decodeObject(forKey: "deposit_count") as? Double
         reclaimCount = aDecoder.decodeObject(forKey: "reclaim_count") as? Double
         revokeCount = aDecoder.decodeObject(forKey: "revoke_count") as? Double
         ticketBuyCount = aDecoder.decodeObject(forKey: "ticket_buy_count") as? Double
         totalTransactionCount = aDecoder.decodeObject(forKey: "total_transaction_count") as? Double
         transactionList = aDecoder.decodeObject(forKey :"transaction_list") as? [VaultTransaction_TransactionList]
         withdrawCount = aDecoder.decodeObject(forKey: "withdraw_count") as? Double

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if coin != nil{
            aCoder.encode(coin, forKey: "coin")
        }
        if depositCount != nil{
            aCoder.encode(depositCount, forKey: "deposit_count")
        }
        if reclaimCount != nil{
            aCoder.encode(reclaimCount, forKey: "reclaim_count")
        }
        if revokeCount != nil{
            aCoder.encode(revokeCount, forKey: "revoke_count")
        }
        if ticketBuyCount != nil{
            aCoder.encode(ticketBuyCount, forKey: "ticket_buy_count")
        }
        if totalTransactionCount != nil{
            aCoder.encode(totalTransactionCount, forKey: "total_transaction_count")
        }
        if transactionList != nil{
            aCoder.encode(transactionList, forKey: "transaction_list")
        }
        if withdrawCount != nil{
            aCoder.encode(withdrawCount, forKey: "withdraw_count")
        }

    }

}
