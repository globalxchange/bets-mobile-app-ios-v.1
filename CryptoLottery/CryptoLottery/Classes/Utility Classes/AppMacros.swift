//
//  AppMacros.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-03-27.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import Foundation

let AppDarkHex                          = "002A51"
let AppDarkColor                        = UIColor(red: 0.00, green: 0.16, blue: 0.32, alpha: 1.00)

let REGEX_USER_NAME_LIMIT               = "^.{3,10}$"
let REGEX_USER_NAME                     = "[A-Za-z0-9]{3,10}"
let REGEX_TITLE_LIMIT                   = "^.{3,20}$"
let REGEX_TITLE                         = "[A-Za-z0-9]{3,20}"
let REGEX_DATE                          = "[0-9]{1,2}+[/]{1,1}+[0-9]{2,4}"
let REGEX_TIME                          = "[0-9]{1,2}+[:]{1,1}+[0-9]{1,2}"
let REGEX_LOCATION                      = "[A-Za-z0-9,-_ ]{1,50}"
let REGEX_EMAIL                         = "[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
let REGEX_PASSWORD_LIMIT                = "^.{6,20}$"
let REGEX_PASSWORD                      = "[A-Za-z0-9]{6,20}"
let REGEX_PHONE_DEFAULT                 = "[0-9]{10,12}"

let ShortVersion                        = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
let DisplayName                         = Bundle.main.infoDictionary?["CFBundleName"] as! String
let BuildName                           = Bundle.main.infoDictionary?["CFBundleVersion"] as! String

// MARK: - -------------------- UIResponder --------------------
// MARK: -

let Screen_width                        = UIScreen.main.bounds.size.width
let Screen_height                       = UIScreen.main.bounds.size.height
let Language                            = NSLocale.preferredLanguages.first
let isIphoneXR                          = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 828, height: 1792)) ?? false
let isIphoneXSMAX                       = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 1242, height: 2688)) ?? false
let isXseries                           = isIphoneX || isIphoneXR || isIphoneXSMAX

public let TopNavHeight : CGFloat       = isXseries ? 84 : 64
let TabbarHeight : CGFloat              = isXseries ? 83 : 49
let StateBarHeight : CGFloat            = isXseries ? 44 : 20
let NavBarHeight : CGFloat              = isXseries ? 64 : 44
let BottomSafeAreaHeight : CGFloat      = isXseries ? 34 : 0
let TopSafeAreaHeight : CGFloat         = isXseries ? 24 : 0
let UnderSafeArea : CGFloat             = isXseries ? 24 : 20

let isIphone5 : Bool                    = (UIScreen.main.bounds.height == 568) //se
let isIphone6 : Bool                    = (UIScreen.main.bounds.height == 667) //6/6s/7/7s/8
let isIphone6P : Bool                   = (UIScreen.main.bounds.height == 736) //6p/6sp/7p/7sp/8p
let isIphoneX : Bool                    = Int((Screen_height / Screen_width) * 100) == 216 ? true : false

let App                                 = UIApplication.shared.delegate as? AppDelegate
public let UserDefault                  = UserDefaults.standard

// MARK: - -------------------- Storyboard --------------------
// MARK: -

func getStroyboard(_ StoryboardWithName: Any) -> UIStoryboard {
    return UIStoryboard(name: StoryboardWithName as! String, bundle: nil)
}
func loadViewController(_ StoryBoardName: Any, _ VCIdentifier: Any) -> UIViewController {
    return getStroyboard(StoryBoardName).instantiateViewController(withIdentifier: VCIdentifier as! String)
}

class HeaderSetter {
    lazy var email: String = {
        return UserInfoData.shared.GetUserEmail()! 
    }()
    lazy var token: String = {
        return UserInfoData.shared.GetUserToken()!
    }()
    lazy var ArcadeUserid: String = {
        guard let inf: UserBioPayload = UserInfoData.shared.GetObjectdata(key: BetsRegister) as? UserBioPayload else {
            return ""
        }
        return inf.arcadeIds == nil ? "" : (inf.arcadeIds.first?.arcadeUserid)!
    }()
    lazy var Arcadeid: String = {
        guard let inf: UserBioPayload = UserInfoData.shared.GetObjectdata(key: BetsRegister) as? UserBioPayload else {
            return ""
        }
        return inf.arcadeIds == nil ? "" : (inf.arcadeIds.first?.arcadeId)!
    }()
    lazy var Username: String = {
        return (UserInfoData.shared.GetUserdata()?.username)!
    }()
}

extension Notification.Name {
    static let ReloadFeed = Notification.Name("ReloadFeeds")
    static let ReloadAccount = Notification.Name("ReloadAccount")
    static let FeedTimer = Notification.Name("FeedTimer")
    static let TabbarUpdate = Notification.Name("TabbarUpdate")
    static let Funding = Notification.Name("Funding")
    static let Withdraw = Notification.Name("Withdraw")
    static let OnHoldView = Notification.Name("OnHoldView")
    static let ChatsView = Notification.Name("ChatsView")
    static let TicketView = Notification.Name("TicketView")
    static let FAQView = Notification.Name("FAQView")
}

//MARK:- Application URL List

// MARK:- Vault Related
let API_BetsVaultBalance            = "https://testbetsapi.globalxchange.io/bets_vault_balances"
let API_TopupVault                  = "https://testbetsapi.globalxchange.io/topup_bets_vault"
let API_WithdrawVault               = "https://testbetsapi.globalxchange.io/withdraw_bets_vault"
let API_Vault_Transactions          = "https://testbetsapi.globalxchange.io/user_vault_transactions"
let API_GetVault_Address            = "https://testbetsapi.globalxchange.io/vault_address"

// MARK:- Lottery Related
let API_Purchase_Tickets            = "https://testbetsapi.globalxchange.io/buy_tickets"
let API_LotteryList                 = "https://testbetsapi.globalxchange.io/lotteries"

// MARK:- Bets Register and Brain file upload
let API_GXRegister                  = "https://testbetsapi.globalxchange.io/register"
let API_UploadBaseURL               = "https://drivetest.globalxchange.io/file/dev-upload-file?"

let API_GetUserData                 = "https://comms.globalxchange.com/user/details/get"
let API_Refreshtoken                = "https://gxauth.apimachine.com/gx/user/refresh/tokens"
let API_Login                       = "https://gxauth.apimachine.com/gx/user/login"
let API_Signup                      = "https://gxauth.apimachine.com/gx/user/signup"
let API_Confirm                     = "https://gxauth.apimachine.com/gx/user/confirm"
let API_UserBIO                     = "https://testbetsapi.globalxchange.io/get_user_bio"

let API_GetCoinVaultList            = "https://comms.globalxchange.com/coin/vault/coins_data"
let API_ConvertForex                = "https://comms.globalxchange.com/forex/convert"

let API_CoinList                    = "https://testbetsapi.globalxchange.io/get_coins_data"

let API_CMCPrices                   = "https://comms.globalxchange.com/coin/getcmcPrices?convert=USD"


//MARK:- Threading with Background and Main

func background(work: @escaping () -> ()) {
    DispatchQueue.global(qos: .userInitiated).async {
        work()
    }
}

func main(work: @escaping () -> ()) {
    DispatchQueue.main.async {
        work()
    }
}
