//
//  SocketConnect.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-07-09.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import Foundation
import SocketIO

public class SocketConnect: NSObject {
    
    static let shared = SocketConnect()
    
    var manager: SocketManager!
    var socket: SocketIOClient!
    var supportlist: GetUserSupportGroupInteractionList!
    var SocketURL = "https://chatsapi.globalxchange.io"
    
    @objc class var swiftSharedInstance: SocketConnect {
        struct Singleton {
            static let instance = SocketConnect()
        }
        return Singleton.instance
    }
    
    // the sharedInstance class method can be reached from ObjC
    @objc class func sharedInstance() -> SocketConnect {
        return SocketConnect.swiftSharedInstance
    }
    
    private override init() {
        let configs = [
            "email" : HeaderSetter().email,
            "token" : HeaderSetter().token,
            "blackoriginnull" : "YdF3UMS6uAPRUZWnP2w6cgYy27z7r4"
        ]
        
        self.manager = SocketManager(socketURL: URL(string:self.SocketURL)!, config: [.log(true), .compress , .reconnects(true), .connectParams(configs)])
        self.socket = manager.defaultSocket
    }
    
    func Connect() {
        let socketConnectionStatus = self.socket.status

        switch socketConnectionStatus {
        case .connected:
           print("socket connected")
        case .connecting:
           print("socket connecting")
        case .disconnected:
           print("socket disconnected")
            self.socket.connect()
        case .notConnected:
           print("socket not connected")
            self.socket.connect()
        }
    }
    
    func disconnect() {
        let socketConnectionStatus = self.socket.status

        switch socketConnectionStatus {
        case .connected:
           print("socket connected")
            self.socket.disconnect()
        case .connecting:
           print("socket connecting")
            self.socket.disconnect()
        case .disconnected:
           print("socket disconnected")
        case .notConnected:
           print("socket not connected")
        }
    }
    
    func get_user_support_group_interaction_list() {
//        self.socket.emit("get_user_support_group_interaction_list", SocketData.self as! SocketData) {
//            print("response")
//        }
        self.socket.on("get_user_support_group_interaction_list") { (response, ACK) in
            print(response)
        }
    }
    
    func connectToServerWithNickname(nickname: String, completionHandler: @escaping (_ userList: [[String: Any]]?) -> Void) {
        socket.emit("connectUser", [nickname as AnyObject])
        
        socket.on("userList") { (dataArray, ack) in
            completionHandler(dataArray.first as? [[String: Any]])
        }
        
        listenForOtherMessages()
    }
    
    func exitChatWithNickname(nickname: String, completionHandler: () -> Void) {
        socket.emit("exitUser", [nickname as AnyObject])
        completionHandler()
    }
    
    func sendMessage(message: String, timestamp: Int64, completionHandler: @escaping (_ messageInfo: ChatsMessage) -> Void) {
        var param = [String: Any]()
        param["message"] = message
        param["thread_id"] = self.supportlist.threadId
        param["sender"] = HeaderSetter().Username
        param["timestamp"] = timestamp
        let jsonData = try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
        if let strignifyJson = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) {
            socket.emitWithAck("new_message", with: [strignifyJson]).timingOut(after: 2) { (responseObject) in
                guard let Success_STR = responseObject.first as? String else {
                    print("\(responseObject) couldn't be converted to NSMutableArray")
                    return
                }
                if Success_STR.uppercased() == "success".uppercased() {
                    let message = ChatsMessage.init(fromDictionary: param)
                    completionHandler(message)
                }
            }
        }
    }
    
    func getChatMessage(completionHandler: @escaping (_ messageInfo: [[String: Any]]) -> Void) {
        var param = [String: Any]()
        param["email"] = HeaderSetter().email
        param["username"] = HeaderSetter().Username
        socket.emitWithAck("get_user_support_group_interaction_list", with: [param]).timingOut(after: 2) { (responseObject) in
            guard let array = responseObject.first as? NSMutableArray else {
                print("\(responseObject) couldn't be converted to NSMutableArray")
                return
            }
            
            guard let dict = array.firstObject as? NSMutableDictionary else {
                print("\(array) couldn't be converted to NSMutableDictionary")
                return
            }
            
            self.supportlist = GetUserSupportGroupInteractionList.init(fromDictionary: dict as! [String : Any])
            self.socket.emitWithAck("get_support_group_chat_history", self.supportlist.groupName, self.supportlist.threadId).timingOut(after: 2) { (responseObjects) in
                guard let message_dict = responseObjects.first as? NSMutableArray else {
                    print("\(responseObject) couldn't be converted to NSMutableArray")
                    return
                }
                print("get_support_group_chat_history list \(message_dict)")
                completionHandler(message_dict as! [[String : Any]])
            }
            self.listenForOtherMessages()
        }
    }
    
    
    private func listenForOtherMessages() {
        socket.on("userConnectUpdate") { (dataArray, socketAck) -> Void in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userWasConnectedNotification"), object: dataArray[0] as! [String: AnyObject])
        }
        
        socket.on("userExitUpdate") { (dataArray, socketAck) -> Void in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userWasDisconnectedNotification"), object: dataArray[0] as! String)
        }
        
        socket.on("userTypingUpdate") { (dataArray, socketAck) -> Void in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userTypingNotification"), object: dataArray[0] as? [String: AnyObject])
        }
    }
    
    
    func sendStartTypingMessage(nickname: String) {
        socket.emit("startType", [nickname as AnyObject])
    }
    
    
    func sendStopTypingMessage(nickname: String) {
        socket.emit("stopType", [nickname as AnyObject])
    }
    
}
