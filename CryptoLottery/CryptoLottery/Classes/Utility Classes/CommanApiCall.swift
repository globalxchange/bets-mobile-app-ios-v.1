//
//  CommanApiCall.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-06-13.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import CRNotifications


class CommanApiCall: NSObject {
    
    public func ForexConversion(Buy: String, From: String, onSuccess: ((_ KeyValue1: Double, _ KeyValue2: Double)->Void)?) {
        
        background {
            let key1 = String.init(format: "%@_%@", Buy.lowercased(), From.lowercased())
            let key2 = String.init(format: "%@_%@", From.lowercased(), Buy.lowercased())
            
            let param = ForexConvertParam.init(buy: Buy, from: From)
            NetworkingRequests.shared.requestsGET(API_ConvertForex, Parameters: param.description, Headers: [:], onSuccess: { (responseObject) in
                main {
                    let status = responseObject["status"] as! Bool
                    if status {
                        let keyvalue1 = responseObject[key2] as! Double
                        let keyvalue2 = responseObject[key1] as! Double
                        onSuccess!(keyvalue1, keyvalue2)
                    }
                    else {
                        let Message = responseObject["message"] as AnyObject
                        CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Message as! String, dismissDelay: 3, completion: {
                            
                        })
                    }
                }
            }) { (Status, code) in
                main {
                    CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Status, dismissDelay: 3, completion: {
                        
                    })
                }
            }
        }
    }
    
    public func CMCprices(onSuccess: ((_ CMC: CMCPrices)->Void)?) {
        background {
            NetworkingRequests.shared.requestsGET(API_CMCPrices, Parameters: [:], Headers: nil, onSuccess: { (responseObject) in
                main {
                    let status = responseObject["status"] as! Bool
                    if status {
                        let CMC_Prices = CMCPrices.init(fromDictionary: responseObject as [String: Any])
                        onSuccess!(CMC_Prices)
                    }
                }
            }) { (Status, code) in
                main {
                    CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Status, dismissDelay: 3, completion: {
                        
                    })
                }
            }
        }
    }
    
}
