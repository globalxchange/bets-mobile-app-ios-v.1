//
//  Extension.swift
//
//  Created by Hiren joshi on 21/10/19.
//  Copyright © 2019 Hiren Joshi. All rights reserved.
//

import UIKit
import Foundation

let ThemeSpace = "  "
fileprivate let imageCache = NSCache<NSString, UIImage>()
/**
 String category class for make string functions easy to use
 */
extension String
{
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    var isBlank:Bool {
        return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).isEmpty
    }
    
    var isZero:Bool {
        if self == "0" || self == "" {
            return true
        }
        else {
            return false
        }
    }
    
    
    /**
     Convert argument string into localisedstring and display to caller controllers
     ### Usage Example: ###
     ````
     "locatized"
     ````
     */
    func localized() -> String
    {
//        return LanguageTool.sharedInstance.getStringForKey(self)//NSLocalizedString(self, tableName: "LiveLocalizable",  comment: "")
        return NSLocalizedString(self, comment: self)
    }
    
    func removeWhiteSpace() -> String {
        let trimmed = self.trimmingCharacters(in: .whitespaces)
        var final = trimmed.replacingOccurrences(of: ThemeSpace, with: "")
        final = final.replacingOccurrences(of: " ", with: "")
        return final
    }
    
    func customStringFormatting() -> String {
        return self.chunk(n: 1).map{ String($0) }.joined(separator: ThemeSpace)
    }
    
    mutating func replaceLocalized(fromvalue: [String], tovalue: [String]) -> String {
        var replacestr: String = ""
        for (index, from) in fromvalue.enumerated() {
            replacestr = self.replacingOccurrences(of: from, with: tovalue[index])
            self = replacestr
        }
        return self
    }
    
    /**
     convert argument string into Date year format and display to caller controllers
     ### Usage Example: ###
     ````
     "string".currentYear()
     ````
     */
    static func currentYear()->String
    {
        let myFormatter = DateFormatter()
        myFormatter.dateFormat = "YYYY"
        return myFormatter.string(from: Date())
    }
    
    /**
     convert argument string into Date Month format and display to caller controllers
     ### Usage Example: ###
     ````
     "string".currentMonth()
     ````
     */
    static func currentMonth()->String
    {
        let myFormatter = DateFormatter()
        myFormatter.dateFormat = "MM"
        return myFormatter.string(from: Date())
    }
    
    /**
     Convert argument string into substring value
     - Parameters inputSTR: main string to get sub string.
     - Parameters substring: sub string to get form main string.
     - return array: return string array with stirng, int and int arguments
     
     ### Usage Example: ###
     ````
     String.FindSubString(inputStr: "main string", subStrings: ["sub string", "sub string"])
     ````
     */
    func FindSubString(inputStr : String, subStrings: Array<String>?) ->Array<(String, Int, Int)> {
        var resultArray : Array<(String, Int, Int)> = []
        for i: Int in 0...(subStrings?.count)!-1 {
            if inputStr.contains((subStrings?[i])!) {
                let range: Range<String.Index> = inputStr.range(of: subStrings![i])!
                let lPos = inputStr.distance(from: inputStr.startIndex, to: range.lowerBound)
                let uPos = inputStr.distance(from: inputStr.startIndex, to: range.upperBound)
                let element = ((subStrings?[i])! as String, lPos, uPos)
                resultArray.append(element)
            }
        }
        for words in resultArray {
            print("FindSubString: \(words)")
        }
        return resultArray
    }
    
    /**
     Convert argument string into range value
     - Parameters value: range value with (string, int, int)
     - return array: return given string range
     
     ### Usage Example: ###
     ````
     String.ConvertRange(value: ("string", 0, 10)))
     ````
     */
    func ConvertRange(value: (String, Int, Int)) -> NSRange {
        var range: NSRange!
        range = NSRange(location: value.1, length: value.2)
        return range
    }
    
    /**
     Convert argument string into attributestring
     - Parameters attribute: pass attrbute string array with (stirng, font, color)
     - Parameters mainstring: Main string with wont to convert into attribute string
     - Parameters rangearray: pass range range array
     - return array: return attribute string with given parameter consider.
     
     ### Usage Example: ###
     ````
     let string1: String = ""
     let string2: String = "Enter The mobile no. Associated with your account \nWe will mobile no you link to reset your password." as String
     let mainstring: String = string1 + string2
     let myMutableString = mainstring.Attributestring(attribute: [(string1, Font().MontserratRegularFont(font: 15.0), UIColor.black), (string2, Font().MontserratRegularFont(font: 15.0), UIColor.lightGray)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [string1, string2])))
     self.Note_lbl.attributedText = myMutableString
     String.Attributestring(attribute: [(string, font, color)], mainstring: string, rangearray:[])
     ````
     */
    func Attributestring(attribute: Array<(String, UIFont, UIColor)>, with mainstring: String, with rangearray:NSArray) -> NSAttributedString {
        let myMutableString = NSMutableAttributedString.init()
        var index: Int = 0
        for obj in attribute {
            let attributes: [NSAttributedString.Key: Any] = [
                .font: obj.1,
                .foregroundColor: obj.2,
            ]
            let rangestring: (String, Int, Int) = rangearray.object(at: index) as! (String, Int, Int)
            let attributestring = NSMutableAttributedString.init(string: rangestring.0, attributes: attributes)
            myMutableString.append(attributestring)
            index += 1
        }
        return myMutableString
    }
    
    /**
     Convert argument string into attributestring
     - Parameters attribute: pass attrbute string array with NSAttributedString.Key
     - Parameters mainstring: Main string with wont to convert into attribute string
     - Parameters rangearray: pass range range array
     - return array: return attribute string with given parameter consider.
     
     ### Usage Example: ###
     ````
     String.EditAttributestring(attribute: [NSAttributedStringKey], mainstring: string, rangearray:[])
     ````
     */
    func EditAttributestring(attribute: Array<([NSAttributedString.Key: Any])>, with mainstring: String, with rangearray:NSArray) -> NSAttributedString {
        let myMutableString = NSMutableAttributedString.init()
        var index: Int = 0
        for obj in attribute {
            let rangestring: (String, Int, Int) = rangearray.object(at: index) as! (String, Int, Int)
            let attributestring = NSMutableAttributedString.init(string: rangestring.0, attributes: obj)
            myMutableString.append(attributestring)
            index += 1
        }
        return myMutableString
    }
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }

    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }

    func sizeOfString(usingFont font: UIFont) -> CGSize {
        let fontAttributes = [NSAttributedString.Key.font: font]
        return self.size(withAttributes: fontAttributes)
    }
    
    func CoinPriceThumbRules(Coin: String) -> String? {
        if Coin.uppercased() == "USD".uppercased() || Coin.uppercased() == "USDT".uppercased() {
            return String.init(format: "$ %@", (self.ConvertIntoUSDFormat())!)
        }
        else {
            return String.init(format: "%@ %@", (self.ConvertOtherCurrencyFormat())!, Coin)
        }
    }
    
    func WithoutCoinPriceThumbRules(Coin: String) -> String? {
        if Coin.uppercased() == "USD".uppercased() || Coin.uppercased() == "USDT".uppercased() {
            return String.init(format: "%@", (self.ConvertIntoUSDFormat())!)
        }
        else {
            return String.init(format: "%@", (self.ConvertOtherCurrencyFormat())!)
        }
    }
    
    func ConvertIntoUSDFormat() -> String? {
        if let n = NumberFormatter().number(from: self) {
            let floatvalue = Double(truncating: n)
            return String.init(format: "%0.2f", floatvalue)
        }
        return "0.00"
    }
    
    func ConvertOtherCurrencyFormat() -> String? {
        if self.contains(".") {
            let array = self.components(separatedBy: ".")
            let value1 = array.first
            let value2 = array.last
            if value1?.count == 0 {
                return String.init(format: "0.%@", (value2!.prefix(4)) as CVarArg)
            }
            else if value1?.count == 1 {
                return String.init(format: "%@.%@", (value1)!, (value2!.prefix(4)) as CVarArg)
            }
            else if value1?.count == 2 {
                return String.init(format: "%@.%@", (value1)!, (value2!.prefix(3)) as CVarArg)
            }
            else if value1?.count == 3 {
                return String.init(format: "%@.%@", (value1)!, (value2!.prefix(2)) as CVarArg)
            }
            else if value1?.count == 4 {
                return String.init(format: "%@.%@", (value1)!, (value2!.prefix(1)) as CVarArg)
            }
            else {
                return value1
            }
        }
        else {
            return self.convertfraction()
        }
    }
    
    func convertfraction() -> String? {
        if let n = NumberFormatter().number(from: self) {
            let floatvalue = Double(truncating: n)
            if self.count == 0 || self.isEmpty {
                return String.init(format: "0.0000")
            }
            else if self.count == 1 {
                return String.init(format: "%0.4f", floatvalue)
            }
            else if self.count == 2 {
                return String.init(format: "%0.3f", floatvalue)
            }
            else if self.count == 3 {
                return String.init(format: "%0.2f", floatvalue)
            }
            else if self.count == 4 {
                return String.init(format: "%0.1f", floatvalue)
            }
            else {
                return String.init(format: "%0.0f", floatvalue)
            }
        }
        return String.init(format: "0.0000")
    }
    
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: font], context: nil)
        return boundingBox.height
    }

}

/**
 Label category class for make button functions easy to use
 */
@objc extension UILabel
{
    /**
     label maskround
     */
    func makeRound() {
        self.clipsToBounds = true
        self.layer.cornerRadius = 5.0
    }
}

@objc extension UITableView {
    func lastIndexpath() -> IndexPath {
        let section = max(numberOfSections - 1, 0)
        let row = max(numberOfRows(inSection: section) - 1, 0)

        return IndexPath(row: row, section: section)
    }
    
    func reloadData(completion:@escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
            { _ in completion() }
    }
    
    func scrollToBottom(animated: Bool = true) {
        let sections = self.numberOfSections
        let rows = self.numberOfRows(inSection: sections - 1)
        if (rows > 0) {
            self.scrollToRow(at: NSIndexPath(row: rows - 1, section: sections - 1) as IndexPath, at: .bottom, animated: true)
        }
    }
    
}

@objc extension UIColor {
    static let primaryColor = UIColor(red:0.00, green:0.57, blue:0.98, alpha:1.0)
    
    // custom color methods
    class func colorWithHex(rgbValue: UInt32) -> UIColor {
        return UIColor( red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                        alpha: CGFloat(1.0))
    }
    
    class func colorWithHexString(hexStr: String) -> UIColor {
        var cString:String = hexStr.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (hexStr.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        if (cString.isEmpty || (cString.count) != 6) {
            return colorWithHex(rgbValue: 0xFF5300);
        } else {
            var rgbValue:UInt32 = 0
            Scanner(string: cString).scanHexInt32(&rgbValue)
            
            return colorWithHex(rgbValue: rgbValue);
        }
    }
    
    func changeImageColor(theImageView: UIImageView, newColor: UIColor) {
        theImageView.image = theImageView.image?.withRenderingMode(.alwaysOriginal)
        theImageView.tintColor = newColor;
    }
    
//    static var customAccent: UIColor {
//        if #available(iOS 13, *) {
//            return UIColor { (traitCollection: UITraitCollection) -> UIColor in
//                if traitCollection.userInterfaceStyle == .dark {
//                    return MaterialUI.orange300
//                } else {
//                    return MaterialUI.orange600
//                }
//            }
//        } else {
//            return MaterialUI.orange600
//        }
//    }
    
}

@objc extension UIStackView {
    func addBackground(color: UIColor) {
        let subView = UIView(frame: bounds)
        subView.backgroundColor = color
        subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(subView, at: 0)
    }
}

@objc extension UITextField {
    @objc func TextFieldCheckisDecimal(isdefault status: Bool, defaultvalue: String) {
        let Value = self.text
        var isNumeric: Bool {
            guard Value!.count > 0 else { return false }
            let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
            return Set(Value!).isSubset(of: nums)
        }
        var isDecimal: Bool {
            guard Value!.count > 0 else { return false }
            let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "."]
            return Set(Value!).isSubset(of: nums)
        }
        if isNumeric {
            if Value == "0" || Value == "00" {
                if status {
                    self.text = String.init(format: "%@", Int(defaultvalue)!)
                }
                else {
                    self.text = ""
                }
            }
            else {
                if Value!.hasPrefix("0") || Value!.hasPrefix("00") {
                    //                    let numberAsInt = Int(Value)
                    let str = Value!.replacingOccurrences(of: "^0+", with: "", options: .regularExpression)
                    self.text = str.count == 0 ? String.init(format: "%@", Int(defaultvalue)!) : str
                }
                else {
                    if status {
                        self.text = String.init(format: "%@", Int(defaultvalue)!)
                    }
                    else {
                        self.text = Value
                    }
                }
            }
        }
        else if isDecimal {
            if Value == "0.0" {
                self.text = ""
            }
            else {
                self.text = Value
            }
        }
        else {
            self.text = Value
        }
    }
    
    func IsContainCharacter(textvalue: String) -> Bool {
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
        if textvalue.rangeOfCharacter(from: characterset) != nil {
            return true
        }
        return false
    }
    
    func IsContainUppercase(textvalue: String) -> Bool {
        let characterset = CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ")
        if textvalue.rangeOfCharacter(from: characterset) != nil {
            return true
        }
        return false
    }
    
    func IsContainDigit(textvalue: String) -> Bool {
        let characterset = CharacterSet(charactersIn: "0123456789")
        if textvalue.rangeOfCharacter(from: characterset) != nil {
            return true
        }
        return false
    }
    
    func IsContainSpecialCharacter(textvalue: String) -> Bool {
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        if textvalue.rangeOfCharacter(from: characterset.inverted) != nil {
            return true
        }
        return false
    }
    
    func isValidEmail(testStr:String?) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: testStr)
    }
    
    func isValidPassword(testStr:String?) -> Bool {
        guard testStr != nil else { return false }
     
        // at least one uppercase,
        // at least one digit
        // at least one special character
        // 8 characters total
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,}")
        return passwordTest.evaluate(with: testStr)
    }
    
}

extension Collection {
    public func chunk(n: IndexDistance) -> [SubSequence] {
        var res: [SubSequence] = []
        var i = startIndex
        var j: Index
        while i != endIndex {
            j = index(i, offsetBy: n, limitedBy: endIndex) ?? endIndex
            res.append(self[i..<j])
            i = j
        }
        return res
    }
    
    var pairs: [SubSequence] {
        var startIndex = self.startIndex
        let count = self.count
        let n = count/2 + count % 2
        return (0..<n).map { _ in
            let endIndex = index(startIndex, offsetBy: 2, limitedBy: self.endIndex) ?? self.endIndex
            defer { startIndex = endIndex }
            return self[startIndex..<endIndex]
        }
    }
    
    func distance(to index: Index) -> Int { distance(from: startIndex, to: index) }
}

extension UIView{
    func animShow(){
        UIView.animate(withDuration: 2, delay: 0, options: [.curveEaseIn],
                       animations: {
                        self.center.y -= self.bounds.height
                        self.layoutIfNeeded()
        }, completion: nil)
        self.isHidden = false
    }
    func animHide(){
        UIView.animate(withDuration: 2, delay: 0, options: [.curveLinear],
                       animations: {
                        self.center.y += self.bounds.height
                        self.layoutIfNeeded()

        },  completion: {(_ completed: Bool) -> Void in
        self.isHidden = true
            })
    }
}

extension NSDictionary {
   var DictToqueryString: String {
      var output: String = ""
      for (key,value) in self {
          output +=  "\(key)=\(value)&"
      }
      output = String(output.dropLast())
      return output
   }
    
    func keyedOrNilValue(key: Key) -> Any? {
        return self[key] ?? "" as Any
    }
}

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: jpegQuality.rawValue)
    }
}

extension UIImageView {
    
    func downloadedFrom(url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        if let cachedImage = imageCache.object(forKey: url.absoluteString as NSString) {
            DispatchQueue.main.async() { () -> Void in
                let imageInset: CGFloat = self.frame.size.height / 10
                self.image = cachedImage.resizableImage(withCapInsets: UIEdgeInsets(top: imageInset, left: imageInset, bottom: imageInset, right: imageInset), resizingMode: .stretch)
            }
        } else {
            URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                imageCache.setObject(image, forKey: url.absoluteString as NSString)
                let imageInset: CGFloat = self.frame.size.height / 10
                self.image = image.resizableImage(withCapInsets: UIEdgeInsets(top: imageInset, left: imageInset, bottom: imageInset, right: imageInset), resizingMode: .stretch)
            }
            }.resume()
        }
    }
    
    func downloadedFrom(link: String?, contentMode mode: UIView.ContentMode = .scaleAspectFit, radious: CGFloat? = nil) {
        if link == nil || link?.count == 0 {
            return
        }
        guard let url = URL(string: link!) else { return }
        self.clipsToBounds = true
        self.layer.cornerRadius = radious!
        if let cachedImage = imageCache.object(forKey: url.absoluteString as NSString) {
            DispatchQueue.main.async() { () -> Void in
                let imageInset: CGFloat = self.frame.size.height / 10
                self.image = cachedImage.resizableImage(withCapInsets: UIEdgeInsets(top: imageInset, left: imageInset, bottom: imageInset, right: imageInset), resizingMode: .stretch)
            }
        } else {
            downloadedFrom(url: url, contentMode: mode)
        }
    }
    
}

/*
 let str = "112312451"

 let final = str.pairs.joined(separator: ":")
 print(final)      // "11:23:12:45:1"

 let final2 = str.inserting(separator: ":", every: 2)
 print(final2)      // "11:23:12:45:1\n"

 var str2 = "112312451"
 str2.insert(separator: ":", every: 2)
 print(str2)   // "11:23:12:45:1\n"

 var str3 = "112312451"
 str3.insert(separator: ":", every: 3)
 print(str3)   // "112:312:451\n"

 var str4 = "112312451"
 str4.insert(separator: ":", every: 4)
 print(str4)   // "1123:1245:1\n"
 */

extension StringProtocol where Self: RangeReplaceableCollection {
    mutating func insert<S: StringProtocol>(separator: S, every n: Int) {
        for index in indices.dropFirst().reversed()
            where distance(to: index).isMultiple(of: n) {
            insert(contentsOf: separator, at: index)
        }
    }
    func inserting<S: StringProtocol>(separator: S, every n: Int) -> Self {
        var string = self
        string.insert(separator: separator, every: n)
        return string
    }
    var firstUppercased: String { prefix(1).uppercased() + dropFirst() }
    var firstCapitalized: String { prefix(1).capitalized + dropFirst() }
}

extension Date {
    static var yesterday: Date { return Date().dayBefore }
    static var tomorrow:  Date { return Date().dayAfter }
    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
    
    func localDate() -> Date {
        let nowUTC = Date()
        let timeZoneOffset = Double(TimeZone.current.secondsFromGMT(for: nowUTC))
        guard let localDate = Calendar.current.date(byAdding: .second, value: Int(timeZoneOffset), to: nowUTC) else {return Date()}
        return localDate
    }
}

func isObjectNotNil(object:AnyObject!) -> Bool
{
    if let _:AnyObject = object
    {
        return true
    }
    return false
}

extension Double {
    
    func secondsToHoursMinutesSeconds () -> (Int?, Int?, Int?, Int?, Int?) {
        let final = Int(self)
        let day = (final / 86400)
        //        let day = final / (1000 * 60 * 60 * 4)
        let hrs = (final / 3600) % 24
        //        let hrs = (final / (1000 * 60 * 60)) % 24
        let mins = (final / 60) % 60
        //        let mins = (final / 1000 / 60) % 60
        let seconds = final % 60
        //        let seconds = (final / 1000) % 60
        let milli = (final * 1) / 1000
        print(String.init(format: "Time is: %02i:%02i:%02i:%02i:%02i", Int(day), Int(hrs), Int(mins), Int(seconds), Int(milli)))
        //        let ms = Int((self.truncatingRemainder(dividingBy: 1)) * 1000)
        return (Int(day), Int(hrs), Int(mins), Int(seconds), Int(milli))
    }
    
}
