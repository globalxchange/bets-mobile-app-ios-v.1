//
//  GlobalPicker.swift
//  Demo
//
//  Created by Gesen on 16/3/1.
//  Copyright © 2016年 Gesen. All rights reserved.
//

enum GlobalPicker {
    static let SubbackgroundColor: ThemeColorPicker = ["#F7F7F7"]
    static let backgroundColor: ThemeColorPicker = ["#ffffff"]
    static let textColor: ThemeColorPicker = ["#002A51"]
    static let subtextColor: ThemeColorPicker = ["#FFFFFF"]
    static let notesColor: ThemeColorPicker = ["#5A6876"]
    
    static let barTextColors = ["#000", "#000"]
    static let barTextColor = ThemeColorPicker.pickerWithColors(barTextColors)
    static let barTintColor: ThemeColorPicker = ["#EB4F38", "#EB4F38"]

}
